<?php 
require_once("../includes/startup.php");

$league = intval($_GET['league']);
if ($logged_in) {
  $interested = db_num_rows(database_select("SELECT * FROM league_maybe WHERE league_id = ?", 'i', [$league]));

  if (db_num_rows(database_select("SELECT * FROM league_manager WHERE league_id = ? AND manager_id = ? AND finished = 0", 'ii', [$league, $user_id])) == 1 && $interested > 2) {
    database_update(
      'league_manager',
      ['finished' => 1, 'start' => database_now()],
      'league_id = ? AND manager_id = ?',
      'ii',
      [$league, $user_id]
    );
  }

  header("Location: /leagues/view.php?league=$league");
} else {
  header("Location: /index.php");
}
?>
