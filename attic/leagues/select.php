<?php
// TODO: $get_games_played and $all_games are used to sort the games list by
// prioritizing games that the current user has played. $all_games does not
// exclude games in $get_games_played, so some games will be shown twice. This
// could probably be merged into a single query, using an appropriate ORDER BY
// clause. This would remove the repetition when generating the markup below
// and prevent discrepancies between both queries.
//
// TODO: We only show the "CREATE" button if the current user is not a manager
// of a league for that game. This is currently done by making an extra query
// inside the game loop that checks for entries in league_manager. This could
// be folded into the $get_games_played and $all_games queries with a LEFT
// JOIN to reduce the number of queries.

$get_games_played = database_get_all(database_select("
  SELECT records.game_id, games.game_name
  FROM records
  JOIN games ON records.game_id = games.game_id
  WHERE user_id = ?
  GROUP BY game_id
  ORDER BY games.game_name
", 's', [$user_id]));

$all_games = database_get_all(database_select("
  SELECT game_id, game_name
  FROM games
  WHERE site_id = 1
  ORDER BY game_name ASC
", '', []));

$get_leagues_in = database_get_all(database_select("
  SELECT users.username, finished, games.game_id, games.game_name, league_manager.league_id
  FROM games
  JOIN league_manager ON league_manager.game_id = games.game_id
  JOIN users ON users.user_id = league_manager.manager_id
  ORDER BY games.game_name
", '', []));

render('leagues/select', 'League Select');
