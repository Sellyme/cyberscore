<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");

$page_title = $t['predictions_create_match'];
$name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $g_game_id");
$rules = db_extract("SELECT rules FROM predictions_games WHERE game_id = $g_game_id");
$t->CacheCountryNames();
require_once("includes/html_top.php");
?>
<div id="pagefull">
  <div id="breadcrumb" class="clearfix">
    <?php require_once("predictions/nav.php");?>
  </div>

  <br />

  <form action="predictionscript.php" method="post">
    <input type="hidden" name="function" value="addmatch">
    <input type="hidden" name="game_id" value="<?php echo $g_game_id;?>">
    * <?php echo $t['predictions_create_match_name'];?> <input type="text" name="game_name"><br /><br />
    * <?php echo $t['predictions_create_match_date'];?> <input type="datetime-local" name="game_datetime"> <small><?php echo $t['predictions_create_match_date_notice'];?></small><br /><br />
    * <?php echo $t['predictions_create_match_teamA'];?> <input type="text" name="teamA"><br /><br />
    * <?php echo $t['predictions_create_match_teamB'];?> <input type="text" name="teamB">
    <br /><br />
    <?php echo $t['predictions_create_match_type'];?>
    <select id="match_type" name="match_type">
      <option value=""><?= h($t['general_na']) ?></option>
      <option value="group"><?= h($t['predictions_create_match_type_group']) ?></option>
      <option value="knockout"><?= h($t['predictions_create_match_knockout']) ?></option>
    </select>
    <small><?php echo $t['predictions_create_match_points_notice'];?></small>
    <br />
    <hr>
    <?php echo $t['predictions_create_match_nation_notice'];?>
    <br /><br />

    <label for="country_id"><?php echo $t['predictions_country_A'];?></label>
    <select id="country_id" name="country_id">
      <option value="1" id="blankcountry" ><?php echo $t->GetCountryName(1);?></option>
      <option value="1">------------------------------</option>
      <?php foreach ($t->GetCountryNames() as $country_id => $country_name) { ?>
        <option value="<?php echo $country_id;?>">
          <?php echo $country_name;?>
        </option>
      <?php } ?>
    </select>
    <br />

    <label for="country_id2"><?php echo $t['predictions_country_B'];?></label>
    <select id="country_id2" name="country_id2">
      <option value="1" id="blankcountry" ><?php echo $t->GetCountryName(1);?></option>
      <option value="1">------------------------------</option>
      <?php foreach($t->GetCountryNames() as $country_id => $country_name) { ?>
        <option value="<?php echo $country_id;?>">
          <?php echo $country_name;?>
        </option>
      <?php } ?>
    </select>
    <br/>

    <input type="submit" name="submit" value="<?php echo $t['general_create'];?>">
  </form>
</div>
<?php require_once("includes/html_bottom.php");?>
