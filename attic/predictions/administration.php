<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");

$page_title = $t['predictions_admin_tools'];
$admin_game_ids = db_query("SELECT game_id FROM predictions_users WHERE user_id = $user_id AND user_status = 'administrator'");
require_once("includes/html_top.php");
?>
	<div id="pagefull">
		<div id="breadcrumb" class="clearfix">
			<?php require_once("predictions/nav.php");?>
		</div>
        <table class="zebra"><th><?php echo $t['games_game_name'];?></th><th><?php echo $t['predictions_create_match'];?></th><th><?php echo $t['predictions_handle_users'];?></th><th><?php echo $t['predictions_delete_game'];?></th>
        <?php while($game = db_get_result($admin_game_ids)){
        $game_name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = '".$game['game_id']."'");
        ?>
        <tr>
        <td><a href="game-<?php echo $game['game_id'];?>"><?php echo $game_name;?></a>  <a title="Edit prediction game information" href="editpredictiongame.php?game_id=<?php echo $game['game_id'];?>"><img src="../skins4/<?php echo $skin_folder?>/images/edit.png" width="11" height="11" alt="Edit game information" /></a></td>
        <td><a href="addmatches.php?game_id=<?php echo $game['game_id'];?>"><?php echo $t['predictions_create_match'];?></a></td>
        <td><a href="handleusers.php?game_id=<?php echo $game['game_id'];?>"><?php echo $t['predictions_handle_users'];?></a></td>
        <td><a href="predictionscript.php?game_id=<?php echo $game['game_id'];?>&function=delete" onclick="return confirm('<?php echo $t['predictions_delete_confirm'];?>')"><img src="../images/delete.png" width="11" height="11" alt="<?php echo $t['predictions_delete_game'];?>" /></a></td>
        </tr><?php
        }
        ?>
        </table>
        <br /><br />
	</div>
<?php 
require_once("includes/html_bottom.php");?>
