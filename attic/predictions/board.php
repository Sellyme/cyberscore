<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");
$page_title = $t['groupstats_leaderboard'] . " - " . db_extract("SELECT game_name FROM predictions_games WHERE game_id = $g_game_id");
$role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $g_game_id AND user_id = $user_id");
$board = db_query("SELECT user_id, SUM(points_awarded) AS score FROM predictions_records WHERE game_id = $g_game_id GROUP BY user_id ORDER BY score DESC, user_id ASC");
$t->CacheCountryNames();
$a = 0;
$num_ties = 0;
require_once("includes/html_top.php");
?>
	<div id="pagefull">
		<div id="breadcrumb" class="clearfix">
			<?php require_once("predictions/nav.php");?>
		</div>
        <h1>
       <table class="zebra">
       <?php while($user = db_get_result($board)){ 
       if($user['score'] != $this_score)    {   $a = $a + 1 + $num_ties; $num_ties = 0;}
       else $num_ties = $num_ties + 1;
       $this_score = $user['score'];

       list($country_id, $user_groups, $username) = db_extract("SELECT country_id, user_groups, username FROM users WHERE user_id = '".$user['user_id']."'");
       $country_code = db_extract("SELECT country_code FROM countries WHERE country_id = $country_id");
        ?>
       <tr><td style="width: 50px; padding-left: 20px; padding-right: 5px;"><?php echo nth($a);?></td>
       <td class="flag" style="width: 50px;"><a href="/scoreboards/medal?country=<?php echo $country_id;?>"><img src="/flags/<?php echo $country_code;?>.png" alt="<?php echo $country_code;?>" title="<?php echo $t->GetCountryName($country_id);?>" /></td>
       <td class="flag" style="width: 40px; text-align: center; padding-right: 15px;"><?php
                        $stars = $cs->GetStarIcons($user_groups, $user['user_id']);
							foreach($stars as $star)
							{
								$star = $t->FormatTranslatorStar($star, $username);
								?>
								<img src="<?php echo $star['src'];?>" title="<?php echo $star['title'];?>" alt="<?php echo $star['alt'];?>" />
							<?php } ?></td>

					<td class="userpic" style="width: 150px;"><a href="../../user/<?php echo $user['user_id'];?>"><img src="/userpics/<?php echo $cs->GetUserPic(array('user_id' => $user['user_id'], 'gender' => 'm'))?>" /></a></td>
					<td class="name" style="width: 50px;"><a href="../../user/<?php echo $user['user_id'];?>">
						<?php echo $username;?></a></td>
                            <td style="padding-left: 150px;"><?php echo $user['score']." "; if($user['score'] == 1) echo $t['predictions_point']; else echo $t['predictions_points'];?>
       </tr>
       <?php 
    }?>

        </table>




	</div>
<?php 
require_once("includes/html_bottom.php");?>
