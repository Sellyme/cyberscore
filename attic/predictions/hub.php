<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");

$page_title = $t['predictions_hub'];
$games = database_get_all(database_select("
  SELECT
    predictions_games.game_id,
    predictions_games.game_name,
    predictions_users.user_id IS NOT NULL AND predictions_users.user_status != 'barred' AS enrolled,
    user_status = 'barred' AS barred
  FROM predictions_games
  LEFT JOIN predictions_users ON (predictions_games.game_id = predictions_users.game_id AND predictions_users.user_id = ?)
  ORDER BY enrolled
", 's', [$user_id]));

$enrolled_games = array_filter($games, function($e){return $e['enrolled'];});
$not_enrolled_games = array_filter($games, function($e){return !$e['enrolled'];});

require_once("includes/html_top.php");
?>
<div id="pagefull">
  <div id="breadcrumb" class="clearfix">
    <?php require_once("predictions/nav.php");?>
  </div>

  <?php if (count($enrolled_games) > 0) { ?>
    <h2><?= $t['predictions_enrolled'] ?></h2>
    <table class="zebra">
      <tr>
        <th><?= $t['games_game_name'] ?></th>
        <th><?= $t['predictions_unenrol'] ?></th>
      </tr>
      <?php foreach ($enrolled_games as $game) { ?>
        <tr>
          <td><a href="/predictions/game-<?= $game['game_id'] ?>"><?= h($game['game_name']) ?></a></td>
          <td>
            <form method='post' action='predictionscript.php'>
              <input type='hidden' name='function' value='unenrol' />
              <input type='hidden' name='game_id' value='<?= $game['game_id'] ?>' />
              <input type='submit' value="<?= $t['predictions_unenrol'] ?>" />
            </form>
          </td>
        </tr>
      <?php } ?>
    </table>
  <?php } ?>

  <?php if (count($not_enrolled_games) > 0) { ?>
    <h2><?= $t['predictions_other'] ?></h2>
    <table class="zebra">
      <tr>
        <th><?= $t['games_game_name'] ?></th>
        <th><?= $t['predictions_enrol'] ?></th>
      </tr>
      <?php foreach ($not_enrolled_games as $other_game) { ?>
        <tr>
          <td><a href="/predictions/game-<?= $other_game['game_id'] ?>"><?= h($other_game['game_name']) ?></a></td>
          <td>
            <?php if (!$other_game['barred']) { ?>
              <form method='post' action='predictionscript.php'>
                <input type='hidden' name='function' value='enrol' />
                <input type='hidden' name='game_id' value='<?= $other_game['game_id'] ?>' />
                <input type='submit' value="<?= $t['predictions_enrol'] ?>" />
              </form>
            <?php } else { ?>
              <?= $t['predictions_barred'] ?>
            <?php } ?>
        </tr>
      <?php } ?>
    </table>
  <?php } ?>

  <form action="predictionscript.php" method="post">
    <h3><?= $t['predictions_create_game'] ?></h3>
    <input type="hidden" name="function" value="creategame">
    <?= $t['games_game_name'] ?> <input type="text" name="game_name">
    <input type="submit" name="submit" value="<?= $t['general_create'] ?>">
  </form>
</div>
<?php require_once("includes/html_bottom.php"); ?>
