<?php
$prediction = database_find_by('predictions_games', ['game_id' => $_GET['game_id']]);
?>

<a href="/predictions"><?= h($t['predictions_hub']) ?></a>
| <a href="/predictions/administration.php"><?= h($t['predictions_admin_tools']) ?></a>

<?php if ($prediction) { ?>
  | <b><a href="/predictions/<?= $prediction['game_id'] ?>"><?= h($prediction['game_name']) ?></a></b>
  | <a href="/predictions/board.php?game_id=<?= $prediction['game_id'] ?>"><?= h($t['groupstats_leaderboard']) ?></a>
  | <a href="/predictions/rules.php?game_id=<?= $prediction['game_id'] ?>"><?= h($t['gamerequests_rules']) ?></a>
<?php } ?>
