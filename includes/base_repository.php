<?php

class BaseRepository {
  protected static $relations = [];

  // If a subclass declares $has_timestamps = true, calling `create()` will
  // automatically add the `created_at` field with value NOW(). If the field
  // doesn't exist in the database, this will make things fail.
  protected static $has_timestamps = false;

  public static function primary_key() {
    return static::$primary_key;
  }

  public static function exists($id) {
    $table = static::$table_name;
    $pkey = static::$primary_key;

    return self::get($id) != NULL;
  }

  public static function find_by($params, $includes = []) {
    $table = static::$table_name;

    $e = database_find_by($table, $params);

    foreach ($includes as $relation) {
      [$repo, $fkey] = static::$relations[$relation];
      if ($e[$fkey]) {
        $e[$relation] = $repo::get($e[$fkey]);
      } else {
        $e[$relation] = NULL;
      }
    }

    return $e;
  }

  public static function where($params, $includes = []) {
    $es = database_filter_by(static::$table_name, $params);

    foreach ($includes as $relation) {
      [$repo, $fkey] = static::$relations[$relation];
      $foreign_ids = array_compact(array_unique(pluck($es, $fkey)));
      $res = index_by($repo::where([$repo::$primary_key => $foreign_ids]), $repo::$primary_key);

      foreach ($es as &$e) {
        if ($e[$fkey]) {
          $e[$relation] = $res[$e[$fkey]];
        } else {
          $e[$relation] = NULL;
        }
      }
      unset($e);
    }

    return $es;
  }

  public static function get($id, $includes = []) {
    $table = static::$table_name;
    $pkey = static::$primary_key;

    return self::find_by([$pkey => $id], $includes);
  }

  public static function all($includes = []) {
    $table = static::$table_name;

    return self::where([], $includes);
  }

  public static function create($params) {
    $table = static::$table_name;

    if (static::$has_timestamps && !array_key_exists('created_at', $params)) {
      $params['created_at'] = database_now();
    }

    return database_insert($table, $params);
  }

  public static function update($id, $params) {
    return database_update_by(static::$table_name, $params, [static::$primary_key => $id]);
  }

  public static function update_all($set, $where) {
    return database_update_by(static::$table_name, $set, $where);
  }

  public static function delete($id) {
    return database_delete_by(static::$table_name, [static::$primary_key => $id]);
  }
}
