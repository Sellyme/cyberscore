<?php

function fetch_chart_awards($chart_id) {
  [$total, $modifiers, $set] = Modifiers::FetchChartModifiers($chart_id);

  return chart_awards($total, $modifiers, $set);
}

function chart_awards($total, $modifiers, $set) {
  $awards = [];

  $awards['csp'] = !$set['unranked'] && $total > 0.0;

  if ($set['arcade']        ||
      $set['speedrun']      ||
      $set['solution']      ||
      $set['unranked']      ||
      $set['challenge'] ||
      $set['collectible']   ||
      $set['incremental']) {
    $awards['medals'] = false;
  } else {
    $awards['medals'] = true;
  }
  $awards['speedrun_points']  = $set['speedrun'];
  $awards['brain_power']      = $set['solution'];
  $awards['arcade_points']    = $set['arcade'];
  $awards['style_points']     = $set['challenge'];
  $awards['cyberstars']       = $set['collectible'];
  $awards['cxp']              = $set['incremental'];

  return $awards;
}
