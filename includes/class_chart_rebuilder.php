<?php

require_once("records.php");

class ChartRebuildClass {
    // Rebuild a single chart immediately (skipping the queue)
    public static function RebuildChart($chart_id) {
      chart_build($chart_id);
      database_delete_by('chartrebuilder', ['chart_id' => $chart_id]);
    }

    // Add one chart to rebuild queue
    public static function QueueChartForRebuild($chart_id) {
      if (database_get(database_select("SELECT chart_id FROM chartrebuilder WHERE chart_id = ?", 's', [$chart_id])) == NULL) {
        database_insert('chartrebuilder', ['chart_id' => $chart_id]);
      }
    }

    // Add multiple charts to rebuild queue
    public static function QueueMultipleChartsForRebuild($chart_ids) {
      foreach ($chart_ids as $chart_id) {
        self::QueueChartForRebuild($chart_id);
      }
    }

    // Add all charts to rebuild queue
    public function QueueAllChartsForRebuild() {
        $chart_ids = pluck(database_get_all(database_select("SELECT level_id FROM levels", '', [])), 'level_id');
        $this->QueueMultipleChartsForRebuild($chart_ids);
    }

    // Add all charts to rebuild queue
    public static function QueueGameChartsForRebuild($game_id) {
      $chart_ids = pluck(database_get_all(database_select("SELECT level_id FROM levels WHERE game_id = ?", 's', [$game_id])), 'level_id');
      self::QueueMultipleChartsForRebuild($chart_ids);
    }

    // Add multiple charts from a specified range of chart IDs to rebuild queue
    public static function QueueChartRangeForRebuild($lowermost_chart, $uppermost_chart) {
      $chart_ids = pluck(database_get_all(database_select("SELECT level_id FROM levels WHERE level_id >= ? AND level_id <= ?", 'ss', [$lowermost_chart, $uppermost_chart])), 'level_id');
      self::QueueMultipleChartsForRebuild($chart_ids);
    }

    // Add all charts for a specified chart-modifier to rebuild queue
    // Out of date as of Jan 2022, please update for the new chart-modifiers.
    public function RebuildChartModifier($chart_modifier) {
      switch ($chart_modifier) {
      case "user_challenge":
        $query = "WHERE chart_flag = 4";
        break;
      case "unranked":
        $query = "WHERE chart_flag = 3";
        break;
      case "region_differences":
        $query = "WHERE csp_modifier IN (1, 3)";
        break;
      case "device_differences":
        $query = "WHERE csp_modifier IN (2, 4)";
        break;
      case "patience":
        $query = "WHERE csp_modifier = 5";
        break;
      case "easy_max": // deprecated
        $query = "WHERE 1 = 0";
        break;
      case "computer_generation":
        $query = "WHERE csp_modifier = 6";
        break;
      case "chart_flooder":
        $query = "JOIN games ON games.game_id = chart_modifiers.game_id WHERE ranked_charts + arcade_charts + speedrun_charts + solution_charts >= 250";
        break;
      case "prem_upgrades":
        $query = "WHERE csp_modifier IN (7, 8)";
        break;
      case "prem_dlc":
        $query = "WHERE csp_modifier = 9";
        break;
      }

      $chart_ids = pluck(database_get_all(database_select("SELECT level_id FROM chart_modifiers $query", '', [])), 'level_id');

      self::QueueMultipleChartsForRebuild($chart_ids);
      return count($chart_ids);
    }
}
?>
