<?php

// YET TO ADD THE ACTUAL GAME REBUILDING CODE FROM RECORD_CLASS.PHP (FULL CACHE CALCULATIONS ETC)
class GameRebuildClass {
  //Rebuild a single game immediately (skipping the queue)
  public static function RebuildGame($game_id) {
    RecordManager::RebuildGameBoard($game_id);
    database_delete_by("cacherebuilder", ['game_id' => $game_id]);
  }

  // Add one game to rebuild queue
  public static function QueueGameForRebuild($game_id) {
    if (database_value("SELECT COUNT(1) FROM cacherebuilder WHERE game_id = ?", [$game_id]) == 0) {
      database_insert('cacherebuilder', ['game_id' => $game_id]);
    }
  }

  // Add multiple games to rebuild queue
  public static function QueueMultipleGamesForRebuild($game_ids) {
    foreach ($game_ids as $game_id) {
      self::QueueGameForRebuild($game_id);
    }
  }

  // Add all games to rebuild queue
  public static function QueueAllGamesForRebuild() {
    $game_ids = pluck(database_fetch_all("SELECT game_id FROM games", []), 'game_id');
    self::QueueMultipleGamesForRebuild($game_ids);
  }

  // Add multiple games from a specified range of game IDs to rebuild queue
  public static function QueueGameRangeForRebuild($lowermost_game, $uppermost_game) {
    $game_ids = pluck(database_fetch_all("SELECT game_id FROM games WHERE ? <= game_id AND game_id <= ?", [$lowermost_game, $uppermost_game]), 'game_id');
    self::QueueMultipleGamesForRebuild($game_ids);
  }

  public static function BuildGameNumSubs($game_id) {
    // TODO: make this a single query to records
    $all         = database_fetch_one("SELECT COUNT(*) AS subs, COUNT(DISTINCT user_id) AS players FROM records WHERE game_id = ?", [$game_id]);
    $unranked    = database_fetch_one("SELECT COUNT(*) AS subs, COUNT(DISTINCT user_id) AS players FROM records WHERE game_id = ? AND csp = 0", [$game_id]);
    $csp         = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_csp WHERE game_id = ?", [$game_id]);
    $medal       = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_medals WHERE game_id = ?", [$game_id]);
    $arcade      = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_arcade WHERE game_id = ?", [$game_id]);
    $speedrun    = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_speedrun WHERE game_id = ?", [$game_id]);
    $solution    = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_solution WHERE game_id = ?", [$game_id]);
    $challenge   = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_userchallenge WHERE game_id = ?", [$game_id]);
    $collectible = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_collectible WHERE game_id = ?", [$game_id]);
    $incremental = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_incremental WHERE game_id = ?", [$game_id]);

    if (!isset($all['subs'])) { $all['subs'] = 0;}
    if (!isset($unranked['subs'])) { $unranked['subs'] = 0;}
    if (!isset($csp['subs'])) { $csp['subs'] = 0;}
    if (!isset($medal['subs'])) { $medal['subs'] = 0;}
    if (!isset($arcade['subs'])) { $arcade['subs'] = 0;}
    if (!isset($speedrun['subs'])) { $speedrun['subs'] = 0;}
    if (!isset($solution['subs'])) { $solution['subs'] = 0;}
    if (!isset($challenge['subs'])) { $challenge['subs'] = 0;}
    if (!isset($collectible['subs'])) { $collectible['subs'] = 0;}
    if (!isset($incremental['subs'])) { $incremental['subs'] = 0;}

    database_update_by('games', [
      'all_subs'            => $all["subs"],
      'standard_subs'       => $medal["subs"],
      'arcade_subs'         => $arcade["subs"],
      'speedrun_subs'       => $speedrun["subs"],
      'solution_subs'       => $solution["subs"],
      'challenge_subs'      => $challenge["subs"],
      'collectible_subs'    => $collectible["subs"],
      'incremental_subs'    => $incremental["subs"],
      'csp_subs'            => $csp["subs"],
      'unranked_subs'       => $unranked["subs"],
      'all_players'         => $all["players"],
      'standard_players'    => $medal["players"],
      'arcade_players'      => $arcade["players"],
      'speedrun_players'    => $speedrun["players"],
      'solution_players'    => $solution["players"],
      'challenge_players'   => $challenge["players"],
      'collectible_players' => $collectible["players"],
      'incremental_players' => $incremental["players"],
      'csp_players'         => $csp["players"],
      'unranked_players'    => $unranked["players"],
    ], ['game_id' => $game_id]);
  }

  public static function BuildGameNumCharts($game_id) {
    $counts = database_fetch_one("
      SELECT
        COUNT(1) AS total,
        COALESCE(SUM(standard), 0) AS standard,
        COALESCE(SUM(speedrun), 0) AS speedrun,
        COALESCE(SUM(solution), 0) AS solution,
        COALESCE(SUM(unranked), 0) AS unranked,
        COALESCE(SUM(challenge), 0) AS challenge,
        COALESCE(SUM(collectible), 0) AS collectible,
        COALESCE(SUM(incremental), 0) AS incremental,
        COALESCE(SUM(arcade), 0) AS arcade
      FROM levels
      LEFT JOIN chart_modifiers2 USING (level_id)
      WHERE levels.game_id = ?
    ", [$game_id]);

    database_update_by('games', [
      'all_charts'         => $counts["total"],
      'standard_charts'    => $counts["standard"],
      'arcade_charts'      => $counts["arcade"],
      'speedrun_charts'    => $counts["speedrun"],
      'challenge_charts'   => $counts["challenge"],
      'solution_charts'    => $counts["solution"],
      'collectible_charts' => $counts["collectible"],
      'incremental_charts' => $counts["incremental"],
      'csp_charts'         => $counts["total"] - $counts["unranked"],
      'unranked_charts'    => $counts["unranked"],
    ], ['game_id' => $game_id]);
  }
}
