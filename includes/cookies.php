<?php

class Cookies {
  // TODO: Sign cookies. This would prevent users tampering with
  // their value. Right now, we're not storing anything that requires
  // cookies being tamper-proof, but if we do, we'll need it.
  //
  // We're currently storing the site language and the flash messages.
  public static function Store($key, $value, $duration = 60*60*24*366) {
    setcookie("cyberscore_$key", json_encode($value), time() + $duration, "/");
  }

  public static function Fetch($key) {
    return json_decode($_COOKIE["cyberscore_$key"] ?? "null", true);
  }

  public static function Clear($key) {
    unset($_COOKIE["cyberscore_$key"]);
    setcookie("cyberscore_$key", "", -1, "/");
  }
}
