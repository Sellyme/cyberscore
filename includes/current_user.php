<?php

function load_current_user_from_token() {
  global $current_user_token;

  $token = $_SERVER['HTTP_X_AUTHORIZATION'] ?? NULL;

  $current_user_token = ApiTokenRepository::find_by(['token' => $token]);
  if ($current_user_token == NULL) {
    return;
  }

  ApiTokenActivityRepository::create([
    'api_token_id' => $current_user_token['api_token_id'],
    'method' => $_SERVER['REQUEST_METHOD'],
    'endpoint' => $_SERVER['REQUEST_URI'],
    'ip' => get_ip(),
  ]);

  load_current_user_from_user_id($current_user_token['user_id']);
}

function load_current_user_from_cookie() {
  global $current_user;

  $id = Session::Fetch('user_id');

  if ($id !== null) {
    load_current_user_from_user_id(intval($id));
  }

  if ($current_user != NULL) {
    database_update_by('users', ['date_lastseen' => database_now()], ['user_id' => $current_user['user_id']]);
  } else {
    reset_current_user();
    Session::Clear('user_id');
  }
}

function load_current_user_from_user_id($id) {
  global $t;
  global $cs;
  global $user_id;
  global $current_user;
  global $skin_folder;

  $current_user = database_fetch_one("
    SELECT *
    FROM users
    LEFT JOIN user_prefs USING(user_id)
    LEFT JOIN user_info USING(user_id)
    LEFT JOIN skins USING(skin_id)
    WHERE users.user_id = ? AND users.user_status > 0
  ", [$id]);

  if ($current_user) {
    $user_id = $id; // TODO: remove after it's no longer used in src/pages/
    $skin_folder = $current_user['folder_name'];
    $cs->LoadUser($current_user);
    $t->LoadUser($current_user);
  }
}

function reset_current_user() {
  global $user_id;
  global $current_user;

  global $skin_folder;
  global $cs;
  global $t;

  $user_id = 0;
  $current_user = NULL;
  $skin_folder = 'default';
  $cs->ResetUser();
  $t->ResetUser();
}

// This function is a temporary hack. It mixes both authentication and
// authorization. It's not ideal, but it's currently the easiest way to prevent
// tokens from being used in non-api endpoints. When all api endpoints are
// migrated to the `/api/` scope, this can be removed.
function load_current_session_from_token($permission_or_permissions) {
  global $current_user;
  global $current_user_token;

  if ($current_user != NULL) {
    return;
  }

  load_current_user_from_token();

  if (!wants_json()) {
    http_response_code(401);
    render_json(['error' => 'API requests with authentication must request JSON (add .json to the endpoint)']);

    global $cs;
    $cs->Exit();
  }

  Authorization::require_all_api_permissions($permission_or_permissions);
}
