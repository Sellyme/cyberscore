<?php

class Flash {
  public static $messages = [
    'error' => [],
    'success' => [],
  ];
  private static $message_count = 0;

  public static function AddError($head, $extra_text = '') {
    self::AddMessage('error', $head, $extra_text);
  }

  public static function AddSuccess($head, $extra_text = '') {
    self::AddMessage('success', $head, $extra_text);
  }

  public static function AddMessage($type, $head, $extra_text = '') {
    self::$messages[$type] []= [
      'title' => $head,
      'body' => $extra_text,
    ];

    self::$message_count++;
  }

  public static function Load() {
    $flash = Cookies::Fetch('flash');
    Cookies::Clear('flash');

    if ($flash !== null) {
      self::$messages = $flash;
      self::$message_count = count(self::$messages['error']) + count(self::$messages['success']);
    }
  }

  public static function Persist() {
    if (self::$message_count > 0) {
      Cookies::Store('flash', self::$messages, 60);
    }
  }

  public static function Clear() {
    self::$message_count = 0;
    self::$messages['error'] = [];
    self::$messages['success'] = [];
  }
}
