<?php

$db = null;

class MySQLDriver {
  public $conf = null;
  public $conn = null;
  public $debug = false;
  public $queries = [];

  public function __construct($url, $debug) {
    $this->conf = parse_url($url);
    $this->debug = $debug;

    $this->connect();
    return true;
  }

  public function reconnect() {
    if (!mysqli_ping($this->conn)) {
      mysqli_close($this->conn);
      $this->conn = null;
      $this->connect();
    }
  }

  public function connect() {
    if ($this->conn !== null) {
      mysqli_close($this->conn);
      $this->conn = null;
    }

    $this->conn = mysqli_connect($this->conf['host'], $this->conf['user'], $this->conf['pass']);
    mysqli_select_db($this->conn, ltrim($this->conf["path"], '/'));
    mysqli_set_charset($this->conn, 'utf8mb4');
  }

  public function escape_string($value) {
    return mysqli_real_escape_string($this->conn, $value);
  }

  public function encode($value) {
    if (is_string($value)) {
      return "'" . $this->escape_string($value) . "'";
    } elseif ($value === null) {
      return "NULL";
    } elseif (is_bool($value)) {
      return $value ? "TRUE" : "FALSE";
    } elseif (is_array($value)) {
      return "(" . implode(',', array_map(fn($v) => $this->encode($v), $value)) . ")";
    } elseif ($value instanceof DatabaseLiteral) {
      return $value->sql;
    } else {
      return $value;
    }
  }

  function transaction($fn) {
    mysqli_begin_transaction($this->conn);
    try {
      $ret = $fn();
      mysqli_commit($this->conn);
      return $ret;
    } catch (mysqli_sql_exception $exception) {
      mysqli_rollback($this->conn);
      throw $exception;
    }
  }

  function query($query, $parameters) {
    $types = str_repeat('s', count($parameters));

    $b = microtime(true);

    $stmt = mysqli_prepare($this->conn, $query);
    if ($stmt === false)  {
      if ($this->debug == true) {
        echo "<pre>prepared_statement_error: ";
        var_dump(mysqli_error($this->conn));
        echo "\n\nbacktrace: ";
        debug_print_backtrace();
        echo "</pre>";
      }

      return null;
    }

    if (count($parameters) > 0) {
      mysqli_stmt_bind_param($stmt, $types, ...$parameters);
    }

    mysqli_stmt_execute($stmt);

    $a = microtime(true);
    $this->queries []= [$a - $b, $query, $parameters];

    return mysqli_stmt_get_result($stmt);
  }

  function insert($table, $columns, $values) {
    $columns = array_map(fn($column) => "`$column`", $columns);
    $values = array_map(fn($v) => $this->encode($v), $values);

    $column_definition = implode(",", $columns);
    $values = implode(", ", $values);
    $this->query("INSERT INTO $table ($column_definition) VALUES ($values)", []);

    return mysqli_insert_id($this->conn);
  }

  function insert_all($table, $columns, $set_of_values) {
    if (empty($set_of_values)) return;

    $columns = array_map(fn($column) => "`$column`", $columns);
    $set_of_values = array_map(
      fn($values) => $this->encode($values),
      $set_of_values
    );

    $column_definition = implode(",", $columns);
    $set_of_values = implode(",", $set_of_values);
    $this->query("INSERT INTO $table ($column_definition) VALUES {$set_of_values}", []);
  }
}

function database_open() {
  global $config;
  global $db;

  $db = new MySQLDriver($config['database']['url'], $config['app']['debug']);
  return $db;
}

function database_reconnect() {
  global $db;

  return $db->reconnect();
}

// Encodes a value ready to be inserted into an SQL query.
function database_encode($value) {
  global $db;
  return $db->encode($value);
}

function database_transaction($fn) {
  global $db;
  return $db->transaction($fn);
}

function db_query($query, $parameters) {
  global $db;
  return $db->query($query, $parameters);
}

function database_multi_query($sql) {
  global $db;

  $result = mysqli_multi_query($db->conn, $sql);

  if ($db->debug && $error = mysqli_error($db->conn)) {
    echo "<pre>database_multi_query error: ";
    var_dump(mysqli_error_list($db->conn));
  }

  return $result;
}

function database_insert($table, $record) {
  global $db;

  $columns = array_keys($record);
  $values = array_map(function($c) use ($record) { return $record[$c]; }, $columns);

  return $db->insert($table, $columns, $values);
}

function database_insert_all($table, $records) {
  if (empty($records)) return;

  global $db;

  $columns = array_keys($records[0]);
  $set_of_values = array_map(
    function($r) use ($columns) {
      return array_map(function($c) use ($r) { return $r[$c]; }, $columns);
    },
    $records
  );

  return $db->insert_all($table, $columns, $set_of_values);
}


function database_where_sql_bind($params) {
  if ($params instanceof DatabaseOr) {
    $conditions = [];
    $values = [];

    foreach ($params->parts as $part) {
      [$c, $v] = database_where_sql_bind($part);
      $conditions []= "($c)";
      array_push($values, ...$v);
    }

    return [implode(' OR ', $conditions), $values];
  }

  $conditions = [];
  $values = [];

  foreach ($params as $key => $value) {
    [$c, $v] = database_condition_bind($key, $value);
    $conditions []= $c;
    array_push($values, ...$v);
  }

  if (count($conditions) == 0) {
    return ["(1=1)", []];
  }

  return [implode(' AND ', $conditions), $values];
}

function database_condition_bind($key, $value) {
  if ($value instanceof DatabaseNot) {
    [$sql, $binds] = database_condition_bind($key, $value->value);
    return ["NOT ($sql)", $binds];
  }

  $key = database_escape_column_name($key);

  if ($value === NULL) {
    return ["$key IS NULL", []];
  }

  if ($value instanceof DatabaseLiteral) {
    $sql = $value->sql;
    return ["$key = $sql", []];
  }

  if ($value instanceof DatabaseOperator) {
    $op = $value->operator;
    $rhs = $value->rhs;
    return ["$key $op $rhs", $value->binds];
  }

  if (is_array($value)) {
    if (empty($value)) {
      return ["FALSE", []];
    } else {
      $compact = array_compact($value);
      if (count($compact) != count($value)) {
        if (empty($compact)) {
          return ["$key IS NULL", []];
        } else {
          $sql = implode(",", array_fill(0, count($compact), '?'));
          return ["($key IN ($sql) OR $key IS NULL)", $compact];
        }
      } else {
        $sql = implode(",", array_fill(0, count($value), '?'));
        return ["$key IN ($sql)", $value];
      }
    }
  }

  return ["$key = ?", [$value]];
}

class DatabaseLiteral {
  public $sql;

  public function __construct($sql) {
    $this->sql = $sql;
  }
}

class DatabaseOperator {
  public $operator = null;
  public $rhs = null;
  public $binds = null;

  public function __construct($operator, $rhs, $binds) {
    $this->operator = $operator;
    $this->rhs = $rhs;
    $this->binds = $binds;
  }
}

class DatabaseNot {
  public $value = null;

  public function __construct($value) {
    $this->value = $value;
  }
}

function database_not($value) {
  return new DatabaseNot($value);
}

class DatabaseOr {
  public $parts = null;
  public function __construct($parts) {
    $this->parts = $parts;
  }
}

function database_or(...$parts) {
  return new DatabaseOr($parts);
}

function database_literal($sql) {
  return new DatabaseLiteral($sql);
}

function database_now() {
  return database_literal('NOW()');
}

function database_escape_column_name($name) {
  if ($name == 'key' || $name == 'group') {
    return "`$name`";
  } else {
    return $name;
  }
}

function database_like($text) {
  return new DatabaseOperator('LIKE', '?', [$text]);
}

function database_regexp($text) {
  return new DatabaseOperator('REGEXP', '?', [$text]);
}

function database_escape_like($text) {
  return str_replace(['\\', '%', '_'], ['\\\\', '\\%', '\\_'], $text);
}

function database_set_sql($fields) {
  return implode(
    ', ',
    array_map(
      function($field, $value) {
        return database_escape_column_name($field) . " = " . database_encode($value);
      },
      array_keys($fields),
      $fields
    )
  );
}

function database_update($table, $fields, $condition, $types, $parameters) {
  if (empty($fields)) return 'noop';

  $fields = database_set_sql($fields);

  return database_select("UPDATE $table SET $fields WHERE $condition", $types, $parameters);
}

function database_update_all($table, $key_name, $keys, $fields) {
  $n = count($keys);

  if ($n > 0) {
    $field_names = array_keys($fields[0]);

    $update = [];
    foreach ($field_names as $field_name) {
      $expr = "CASE $key_name";
      for ($i = 0; $i < $n; $i++) {
        $v = database_encode($fields[$i][$field_name]);
        $expr .= " WHEN {$keys[$i]} THEN $v";
      }

      $expr .= " END";
      $update []= "$field_name = $expr";
    }

    $update = implode(", ", $update);
    $keys = implode(", ", $keys);
    return db_query("UPDATE $table SET $update WHERE $key_name IN ($keys)", []);
  }
}

function database_update_by($table, $updates, $condition) {
  if (empty($updates)) return 'noop';

  $updates = database_set_sql($updates);
  [$where_sql, $where_binds] = database_where_sql_bind($condition);

  return db_query("UPDATE $table SET $updates WHERE $where_sql", $where_binds);
}

function database_delete($table, $condition, $parameters) {
  return db_query("DELETE FROM $table WHERE $condition", $parameters);
}

function database_delete_all($table) {
  return db_query("DELETE FROM $table", []);
}

function database_delete_by($table, $fields) {
  [$sql, $binds] = database_where_sql_bind($fields);
  return db_query("DELETE FROM $table WHERE $sql", $binds);
}

function database_query_count() {
  global $db;

  return count($db->queries);
}

function database_select($query, $types, $parameters = NULL) {
  if ($parameters === NULL) {
    $parameters = $types;
    $types = str_repeat('s', count($parameters));
  }

  return db_query($query, $parameters);
}

function database_value($query, $parameters) {
  $result = database_select($query, $parameters);

  $row = mysqli_fetch_row($result);
  if ($row != NULL) {
    return $row[0];
  } else {
    return NULL;
  }
}

function database_single_value($query, $types, $parameters) {
  return database_value($query, $parameters);
}

function database_find_by($table, $fields) {
  [$sql, $binds] = database_where_sql_bind($fields);
  return database_fetch_one("SELECT * FROM $table WHERE $sql", $binds);
}

function database_filter_by($table, $fields) {
  [$sql, $binds] = database_where_sql_bind($fields);
  return database_fetch_all("SELECT * FROM $table WHERE $sql", $binds);
}

function database_fetch_all($query, $parameters) {
  return database_get_all(db_query($query, $parameters));
}

function database_fetch_one($query, $parameters) {
  return database_get(db_query($query, $parameters));
}

function database_get_all($records) {
  return mysqli_fetch_all($records, MYSQLI_ASSOC);
}

function database_get($records) {
  return mysqli_fetch_assoc($records);
}

function database_queries() {
  global $db;

  $queries = $db->queries;
  sort($queries);
  return $queries;
}
