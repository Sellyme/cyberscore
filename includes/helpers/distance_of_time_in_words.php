<?php

// Copyright © 2011-2019 weLaika
// Licensed under the MIT License
// https://github.com/welaika/wordless

function distance_of_time_in_words($from_time, $to_time, $include_seconds = false) {
  $dm = abs($from_time - $to_time) / 60;
  $ds = abs($from_time - $to_time);

  switch (true) {
  case $dm < 2:
    if ($include_seconds == false) {
      if ($dm < 1) {
        return 'less than a minute';
      } else {
        return '1 minute';
      }
    } else {
      switch (true) {
      case $ds < 5:
        return 'less than 5 seconds';
      case $ds < 10:
        return 'less than 10 seconds';
      case $ds < 20:
        return 'less than 20 seconds';
      case $ds <= 40:
        return 'half a minute';
      default:
        return 'less than a minute';
      }
    }
  case $dm < 45:
    return sprintf("%d minutes", round($dm));
    break;
  case $dm < 90:
    return 'about 1 hour';
  case $dm < 24*60:
    return sprintf("about %d hours", round($dm / 60.0));
  case $dm < 2*24*60:
    return '1 day';
  case $dm < 30*24*60:
    return sprintf("%d days", round($dm / 1440));
  case $dm < 60*24*60:
    return 'about 1 month';
  case $dm < 60*24*365:
    return sprintf("%d months", round($dm / 43200));
  case $dm < 2*60*24*365:
    return 'about 1 year';
  default:
    return sprintf("over %d years", round($dm / 525600));
  }
}
