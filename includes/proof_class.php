<?php

// Proofs can be submitted in two ways, either by uploading the file or
// submitting a link. If the link points directly, proofs can be downloaded to
// our server. If something goes wrong, we store the link instead.
//
// This class contains methods to handle this at several levels. Here's a quick
// rundown of what's happening.
//
// These two types, upload and link, are handled by StoreUploadedProof and
// StoreLinkedProof respectively. They both call StoreProofFile and only care
// about storing the file in /uploads/proofs.
//
// After the proofs are downloaded (or not), they need to be associated with a
// record. This is handled by LinkProof($record_id, $link).
//
// If you want to do both operations at once, you can call ProcessUploadedProof
// or ProcessLinkedProof for uploaded or linked proofs respectively.
//
class ProofManager {
  public static function ProcessUploadedProof($record_id, $proof_file) {
    $result = self::StoreUploadedProof($proof_file);

    if ($result['status'] == 'success') {
      self::LinkProof($record_id, $result['link']);
    }

    return $result;
  }

  public static function ProcessLinkedProof($record_id, $proof_link) {
    $result = self::StoreLinkedProof($proof_link);

    if ($result['status'] == 'success') {
      self::LinkProof($record_id, $result['link']);
    }

    return $result;
  }

  public static function StoreUploadedProof($proof_file) {
    if (!is_uploaded_file($proof_file['tmp_name'])) {
      return ['status' => 'error_noupload'];
    }

    return self::StoreProofFile($proof_file['tmp_name']);
  }

  public static function StoreLinkedProof($proof_link) {
    if (!str_starts_with($proof_link, "https://") && !str_starts_with($proof_link, "http://")) {
      return ['status' => 'error_not_http_link'];
    }

    $filename = tempnam(sys_get_temp_dir(), "cs-upload");

    if (!copy($proof_link, $filename)) {
      unlink($filename);
      return ['status' => 'success', 'link' => $proof_link, 'action' => 'linked'];
    }

    $result = self::StoreProofFile($filename);

    if ($result['status'] == 'error_not_image') {
      unlink($filename);
      return ['status' => 'success', 'link' => $proof_link, 'action' => 'linked'];
    }

    unlink($filename);

    return $result;
  }

  private static function StoreProofFile($filename) {
    if (filesize($filename) > 6144000) {
      return ['status' => 'error_size'];
    }

    $file_info = getimagesize($filename);
    if ($file_info == false || $file_info[2] > 3) {
      return ['status' => 'error_not_image'];
    }

    global $config;
    $root = $config['app']['root'];
    $sha256 = hash('sha256', file_get_contents($filename));

    $path = "$root/public/uploads/proofs/$sha256";

    $success = copy($filename, $path);
    if (!$success) {
      return ['status' => 'error_copy'];
    } else {
      return ['status' => 'success', 'link' => "/uploads/proofs/$sha256", 'action' => 'stored'];
    }
  }

  //---------------------------------------------------------------------------
  public static function LinkProof($record_id, $proof_link) {
    $rec_status = database_single_value("SELECT rec_status FROM records WHERE record_id = ?", 's', [$record_id]);
    if ($rec_status == 0 || $rec_status == 3) {
      $rec_status = 4;
    } else if ($rec_status == 1 || $rec_status == 2) {
      // Record is pending/under investigation -> set to 5 or 6
      $rec_status += 4;
    }

    database_update_by(
      'records',
      ['linked_proof' => $proof_link, 'rec_status' => $rec_status],
      ['record_id' => $record_id]
    );

    RecordManager::AddRecordHistoryEntry($record_id, 'p');
  }

  //------------------------------------------------------------------------------
  public static function ProofDetails($record) {
    global $config;
    $root = $config['app']['root'];

    $linked_proof = $record['linked_proof'];
    $record_id = $record['record_id'];
    $user_id = $record['user_id'];

    $proof_url = $linked_proof;

    if ($linked_proof == '') {
      return NULL;
    } else if (str_starts_with($linked_proof, 'https://s3.eu-west-2.amazonaws.com/cyberscoreproofs/')) {
      $proof_icon = 'picture.png';
      $type = 'picture';
      $proof_url = $linked_proof;
    } else if (str_starts_with($linked_proof, '/uploads/proofs/')) {
      $proof_icon = 'picture.png';
      $type = 'picture';
      $proof_url = $config['app']['base_url'] . $linked_proof;
    } else if (str_starts_with($linked_proof, '/proofs/')) {
      // We're a self-hosted proof. We're either on disk or on S3.
      $proof_icon = 'picture.png';
      $type = 'picture';
      if (!file_exists("$root/public/proofs/$user_id/$record_id.jpg")) {
        $proof_url = "https://s3.eu-west-2.amazonaws.com/cyberscoreproofs/Proofs/$user_id/$record_id.jpg";
      } else {
        $proof_url = $config['app']['base_url'] . $proof_url;
      }
    } else if (substr_count($linked_proof, 'youtube') || substr_count($linked_proof, 'youtu.be')) {
      $proof_icon = 'youtube.gif';
      $type = 'youtube';
    } else if (substr_count($linked_proof, 'twitch')) {
      $proof_icon = 'twitch.png';
      $type = 'twitch';
    } else if (substr_count($linked_proof, 'dailymotion')) {
      $proof_icon = 'dailymotion.gif';
      $type = 'dailymotion';
    } else if (substr_count($linked_proof, 'nico')) {
      $proof_icon = 'nico.png';
      $type = 'nico';
    } else {
      // unsure why we're defaulting to video and not picture
      $proof_icon = 'video.gif';
      $type = 'video';
    }

    return [
      'icon' => "/images/proofs/$proof_icon",
      'type' => $type,
      'url'  => $proof_url,
    ];
  }
}
