<?php

class Scoreboard {
  // Calculates the # of people for a given cache
  static function FetchGlobalScoreboardCount($cache) {
    return database_single_value("SELECT COUNT(*) FROM $cache", '', []);
  }

  // Calculated the Rainbow Points generated for a scoreboard
  static function FetchUltimateBoardScore($position, $modifier) {
    if (!is_numeric($position)) {
      return 0;
    } else {
      return round($modifier / sqrt($position), 2);
    }
  }

  // Scales Rainbow Scoreboard Multipliers according to scoreboard popularity
  static function FetchUltimateBoardMaxMultiplier($cache, $maximum, $multiplier) {
    return pow(self::FetchGlobalScoreboardCount($cache), $maximum) * $multiplier;
  }

  // Set Rainbow Scoreboard multiplier
  static function RainbowMultipliers() {
    // Multipliers, can be changed to fine-tune balance of the rainbow scoreboard board
    // Secondary scoreboards should all match
    // Max multiplier should be very fine-tuned. The lower it is, the less powerful the bigger boards are.
    // Bonus multiplier heavily rewards submitting to every single board and reducing average position. Reducing the multiplier adjusts this factor down.
    $multipliers = [
      'starboard'     => 1200,
      'medal'         => 1000,
      'trophy'        => 1000,
      'arcade'        => 800,
      'speedrun'      => 800,
      'solution'      => 800,
      'challenge'     => 800,
      'incremental'   => 800,
      'collectible'   => 800,
      'proof'         => 800,
      'video_proof'   => 1000,
      'submissions'   => 800,
      'max'           => 0.30,
      'bonus'         => 1.01,
    ];

    return $multipliers;
  }

  // Collect Rainbow Scoreboard maximums
  static function RainbowModifiers() {
    // Fetch the base multipliers
    $multipliers = self::RainbowMultipliers();

    // Adjust the modifiers by the # of users represented on the appropriate scoreboard
    $modifiers = [
      'starboard' => self::FetchUltimateBoardMaxMultiplier('sb_cache', $multipliers['max'], $multipliers['starboard']),
      'medal' => self::FetchUltimateBoardMaxMultiplier('sb_cache_standard', $multipliers['max'], $multipliers['medal']),
      'trophy' => self::FetchUltimateBoardMaxMultiplier('sb_cache_trophy', $multipliers['max'], $multipliers['trophy']),
      'arcade' => self::FetchUltimateBoardMaxMultiplier('sb_cache_arcade', $multipliers['max'], $multipliers['arcade']),
      'speedrun' => self::FetchUltimateBoardMaxMultiplier('sb_cache_speedrun', $multipliers['max'], $multipliers['speedrun']),
      'solution' => self::FetchUltimateBoardMaxMultiplier('sb_cache_solution', $multipliers['max'], $multipliers['solution']),
      'challenge' => self::FetchUltimateBoardMaxMultiplier('sb_cache_challenge', $multipliers['max'], $multipliers['challenge']),
      'incremental' => self::FetchUltimateBoardMaxMultiplier('sb_cache_incremental', $multipliers['max'], $multipliers['incremental']),
      'collectible' => self::FetchUltimateBoardMaxMultiplier('sb_cache_collectible', $multipliers['max'], $multipliers['collectible']),
      'proof' => self::FetchUltimateBoardMaxMultiplier('sb_cache_proof', $multipliers['max'], $multipliers['proof']),
      'video_proof' => self::FetchUltimateBoardMaxMultiplier('sb_cache_video_proof', $multipliers['max'], $multipliers['video_proof']),
      'submissions' => self::FetchUltimateBoardMaxMultiplier('sb_cache_total_subs', $multipliers['max'], $multipliers['submissions']),
      'bonus' => $multipliers['bonus'],
    ];

    return $modifiers;
  }
}
