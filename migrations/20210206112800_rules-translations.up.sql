ALTER TABLE rules ADD COLUMN rule_text_translated JSON NULL;
UPDATE rules SET rule_text_translated = JSON_OBJECT('en', rule_text);
