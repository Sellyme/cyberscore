SET SESSION sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE INDEX translation_db_table_field ON translation_db(table_field, table_id);

ALTER TABLE news MODIFY edit_date DATETIME NULL;
ALTER TABLE news MODIFY edit_user_id INT(11) NULL;
ALTER TABLE news ENGINE = 'InnoDB';
ALTER TABLE languages ENGINE = 'InnoDB';

UPDATE news SET edit_user_id = NULL WHERE edit_user_id = 0;
UPDATE news SET edit_date = NULL WHERE edit_date = '0000-00-00 00:00:00';
