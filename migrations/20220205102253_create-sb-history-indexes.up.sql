CREATE INDEX sb_history_user_id ON sb_history(user_id);
CREATE INDEX sb_history_user_id_history_date ON sb_history(user_id, history_date);
CREATE INDEX sb_history_date ON sb_history(history_date);
