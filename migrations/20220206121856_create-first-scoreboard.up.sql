CREATE TABLE gsb_cache_first(
  user_id MEDIUMINT UNSIGNED NOT NULL,
  game_id SMALLINT UNSIGNED NOT NULL,
  year    INT NOT NULL,
  month   INT NOT NULL,
  points  BIGINT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT,
  FOREIGN KEY (game_id) REFERENCES games(game_id) ON DELETE RESTRICT
);

CREATE TABLE sb_cache_first(
  user_id MEDIUMINT UNSIGNED NOT NULL,
  year    INT NOT NULL,
  month   INT NOT NULL,
  points  BIGINT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT
);
