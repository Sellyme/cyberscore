CREATE TABLE chart_modifiers2(
  level_id MEDIUMINT UNSIGNED PRIMARY KEY,

  standard BOOLEAN NOT NULL DEFAULT FALSE,
  speedrun BOOLEAN NOT NULL DEFAULT FALSE,
  solution BOOLEAN NOT NULL DEFAULT FALSE,
  unranked BOOLEAN NOT NULL DEFAULT FALSE,
  challenge BOOLEAN NOT NULL DEFAULT FALSE,
  collectible BOOLEAN NOT NULL DEFAULT FALSE,
  incremental BOOLEAN NOT NULL DEFAULT FALSE,
  arcade BOOLEAN NOT NULL DEFAULT FALSE,

  chart_flooder DECIMAL(6, 3) NOT NULL DEFAULT 1.0,

  regional_differences BOOLEAN NOT NULL DEFAULT FALSE,
  accessibility_issues_conditionally_accessible BOOLEAN NOT NULL DEFAULT FALSE,
  accessibility_issues_geographical BOOLEAN NOT NULL DEFAULT FALSE,
  accessibility_issues_inaccessible BOOLEAN NOT NULL DEFAULT FALSE,
  computer_generated BOOLEAN NOT NULL DEFAULT FALSE,
  gameplay_differences_device BOOLEAN NOT NULL DEFAULT FALSE,
  gameplay_differences_game_version BOOLEAN NOT NULL DEFAULT FALSE,
  patience BOOLEAN NOT NULL DEFAULT FALSE,
  premium_dlc BOOLEAN NOT NULL DEFAULT FALSE,
  premium_upgrades_and_consumables_conditionally_accessible BOOLEAN NOT NULL DEFAULT FALSE,
  premium_upgrades_and_consumables_earnable BOOLEAN NOT NULL DEFAULT FALSE,
  premium_upgrades_and_consumables_inaccessible BOOLEAN NOT NULL DEFAULT FALSE,
  premium_upgrades_and_consumables_premium_only BOOLEAN NOT NULL DEFAULT FALSE,
  significant_device_differences BOOLEAN NOT NULL DEFAULT FALSE,
  significant_regional_differences BOOLEAN NOT NULL DEFAULT FALSE,

  FOREIGN KEY (level_id) REFERENCES levels(level_id) ON DELETE RESTRICT
);
