ALTER TABLE gsb_cache_trophy MODIFY rpc DOUBLE DEFAULT NULL;
ALTER TABLE gsb_cache_trophy MODIFY players INT DEFAULT NULL;
ALTER TABLE gsb_cache_trophy MODIFY scoreboard_pos INT UNSIGNED DEFAULT NULL;
ALTER TABLE gsb_cache_trophy MODIFY split_index INT UNSIGNED DEFAULT NULL;
ALTER TABLE gsb_cache_trophy MODIFY dlc_index INT UNSIGNED DEFAULT NULL;

ALTER TABLE gsb_cache_trophy ADD COLUMN csp_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN csp_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN standard_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN standard_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN speedrun_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN speedrun_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN solution_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN solution_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN challenge_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN challenge_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN arcade_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN arcade_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN incremental_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN incremental_trophy_points DOUBLE DEFAULT 0.0;

ALTER TABLE gsb_cache_trophy ADD COLUMN collectible_trophy VARCHAR(32) DEFAULT NULL;
ALTER TABLE gsb_cache_trophy ADD COLUMN collectible_trophy_points DOUBLE DEFAULT 0.0;
