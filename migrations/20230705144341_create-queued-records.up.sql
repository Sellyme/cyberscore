CREATE TABLE queued_records(
  queued_record_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

  chart_id MEDIUMINT UNSIGNED NOT NULL,
  user_id  MEDIUMINT UNSIGNED NOT NULL,

  entity_id TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  entity_id2 TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  entity_id3 TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  entity_id4 TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  entity_id5 TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  entity_id6 TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci,

  platform_id SMALLINT UNSIGNED,
  game_patch text CHARACTER SET utf8 COLLATE utf8_unicode_ci,

  submission DOUBLE DEFAULT NULL,
  submission2 DOUBLE DEFAULT NULL,
  extra1 DOUBLE DEFAULT NULL,
  extra2 DOUBLE DEFAULT NULL,
  extra3 DOUBLE DEFAULT NULL,
  comment VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,

  created_at DATETIME NOT NULL,

  FOREIGN KEY (chart_id) REFERENCES levels(level_id) ON DELETE RESTRICT,
  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT,
  FOREIGN KEY (platform_id) REFERENCES platforms(platform_id) ON DELETE RESTRICT
);
