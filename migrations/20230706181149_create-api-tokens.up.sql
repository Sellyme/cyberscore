CREATE TABLE api_tokens(
  api_token_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,

  user_id MEDIUMINT UNSIGNED NOT NULL,
  token VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  description TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  permissions TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  created_at DATETIME NOT NULL,
  deleted_at DATETIME NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT
);

CREATE TABLE api_token_activities(
  api_token_activity_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,

  api_token_id BIGINT UNSIGNED,
  method VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  endpoint TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  ip VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  created_at DATETIME NOT NULL,

  FOREIGN KEY (api_token_id) REFERENCES api_tokens(api_token_id) ON DELETE RESTRICT
);
