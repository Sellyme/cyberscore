ALTER TABLE sb_history_standard RENAME COLUMN platinums TO platinum;
ALTER TABLE sb_history_standard RENAME COLUMN golds     TO gold;
ALTER TABLE sb_history_standard RENAME COLUMN silvers   TO silver;
ALTER TABLE sb_history_standard RENAME COLUMN bronzes   TO bronze;
ALTER TABLE sb_history_speedrun RENAME COLUMN platinums TO platinum;
ALTER TABLE sb_history_speedrun RENAME COLUMN golds     TO gold;
ALTER TABLE sb_history_speedrun RENAME COLUMN silvers   TO silver;
ALTER TABLE sb_history_speedrun RENAME COLUMN bronzes   TO bronze;
ALTER TABLE sb_history_speedrun RENAME COLUMN medal_points TO speedrun_points;
ALTER TABLE sb_history_arcade   RENAME COLUMN tokens    TO arcade_points;
ALTER TABLE sb_history_solution DROP COLUMN bonus_csr;
ALTER TABLE sb_history_solution DROP COLUMN total_csr;
ALTER TABLE sb_history_solution ADD COLUMN brain_power INT NOT NULL AFTER scoreboard_pos;
ALTER TABLE sb_history_challenge DROP COLUMN bonus_csr;
ALTER TABLE sb_history_challenge DROP COLUMN total_csr;
ALTER TABLE sb_history_challenge ADD COLUMN cp DOUBLE NOT NULL AFTER scoreboard_pos;
CREATE TABLE sb_history_collectible (
  sb_history_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id INT UNSIGNED NOT NULL,
  scoreboard_pos INT UNSIGNED NOT NULL,

  cyberstars BIGINT UNSIGNED NOT NULL,

  num_subs INT UNSIGNED NOT NULL,
  history_date DATETIME NOT NULL,
  PRIMARY KEY (sb_history_id)
);
CREATE TABLE sb_history_incremental (
  sb_history_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id INT UNSIGNED NOT NULL,
  scoreboard_pos INT UNSIGNED NOT NULL,

  cxp BIGINT UNSIGNED NOT NULL,
  vs_cxp BIGINT UNSIGNED,
  base_level INT UNSIGNED,

  num_subs INT UNSIGNED NOT NULL,
  history_date DATETIME NOT NULL,
  PRIMARY KEY (sb_history_id)
);
