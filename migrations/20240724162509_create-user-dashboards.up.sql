CREATE TABLE user_dashboards(
  user_dashboard_id bigint unsigned NOT NULL AUTO_INCREMENT,
  user_id mediumint unsigned NOT NULL,
  config text NOT NULL,
  PRIMARY KEY(user_dashboard_id),
  KEY user_id (user_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);
