export function registerScores(result, addScore, game) {
  if (result.pokedex_entry) {
    const dex = `#${result.pokedex_entry.dex.toString().padStart(4, "0")}`;

    addScore(charts_with_name(charts_in_groups(game, `Seen`), dex), result.pokedex_entry.seen);
    addScore(charts_with_name(charts_in_groups(game, `Caught`), dex), result.pokedex_entry.caught);
    addScore(charts_with_name(charts_in_groups(game, `Lucky`), dex), result.pokedex_entry.lucky);
    addScore(charts_with_name(charts_in_groups(game, `Purified`), dex), result.pokedex_entry.purified);

    if (result.pokedex_entry.weight_min != null) addScore(charts_with_name(charts_in_groups(game, `Lightest`), dex), result.pokedex_entry.weight_min / 100.0);
    if (result.pokedex_entry.weight_max != null) addScore(charts_with_name(charts_in_groups(game, `Heaviest`), dex), result.pokedex_entry.weight_max / 100.0);
    if (result.pokedex_entry.height_min != null) addScore(charts_with_name(charts_in_groups(game, `Shortest`), dex), result.pokedex_entry.height_min / 100.0);
    if (result.pokedex_entry.height_max != null) addScore(charts_with_name(charts_in_groups(game, `Tallest`), dex), result.pokedex_entry.height_max / 100.0);
  }
}

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects Seen/Caught/Lucky/Purified from pokédex entries',
    'detects min and max weight and height from pokédex entries',
  ],
  limitations: [
    "only tested with english screenshots",
    "didn't test with tablet screenshots",
  ],
};
