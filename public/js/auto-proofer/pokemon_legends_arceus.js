export function registerScores(result, addScore, game) {
  if (result.info) {
    const dex = `#${result.info.dex.toString().padStart(3, "0")}`;
    const values = [
      result.info.weight_min / 10.0,
      result.info.weight_max / 10.0,
      result.info.height_min,
      result.info.height_max,
    ];

    ['Lightest', 'Heaviest', 'Smallest', 'Largest'].map((groupName) =>
      charts_with_name(charts_in_groups(game, groupName), dex)
    ).forEach((charts, index) => {
      addScore(charts, null, values[index]);
    });
  }

  if (result.research) {
    const dex = `#${result.research.dex.toString().padStart(3, "0")}`;

    {
      const charts = charts_with_name(charts_in_groups(game, "Research Tasks"), dex);
      charts.forEach((chart, index) => {
        addScore([charts[index]], result.research.tasks[index]);
      });
    }

    {
      const charts = charts_in_groups(game, "Max Research Level");
      addScore(charts_with_name(charts, dex), result.research.research_level);
    }
  }

  if (result.lost_and_found) {
    const charts = charts_in_groups(game, "Lost and Found");

    addScore(charts_with_name(charts, "Total MP"),      result.lost_and_found.total_mp);
    addScore(charts_with_name(charts, "Dropped items"), result.lost_and_found.retrieved_dropped_items);
  }

  if (result.pokedex_cover) {
    const charts = charts_in_groups(game, "Pokédex");

    addScore(charts_with_name(charts, "Research Points"), result.pokedex_cover.research_points);
    addScore(charts_with_name(charts, "Total Seen"),      result.pokedex_cover.total_seen);
    addScore(charts_with_name(charts, "Total Caught"),    result.pokedex_cover.total_caught);

    if (result.pokedex_cover.fieldlands_seen !== undefined) {
      addScore(charts_with_name(charts, "Fieldlands Seen"),   result.pokedex_cover.fieldlands_seen);
      addScore(charts_with_name(charts, "Fieldlands Caught"), result.pokedex_cover.fieldlands_caught);
    }

    if (result.pokedex_cover.mirelands_seen !== undefined) {
      addScore(charts_with_name(charts, "Mirelands Seen"),   result.pokedex_cover.mirelands_seen);
      addScore(charts_with_name(charts, "Mirelands Caught"), result.pokedex_cover.mirelands_caught);
    }

    if (result.pokedex_cover.coastlands_seen !== undefined) {
      addScore(charts_with_name(charts, "Coastlands Seen"),   result.pokedex_cover.coastlands_seen);
      addScore(charts_with_name(charts, "Coastlands Caught"), result.pokedex_cover.coastlands_caught);
    }

    if (result.pokedex_cover.highlands_seen !== undefined) {
      addScore(charts_with_name(charts, "Highlands Seen"),   result.pokedex_cover.highlands_seen);
      addScore(charts_with_name(charts, "Highlands Caught"), result.pokedex_cover.highlands_caught);
    }

    if (result.pokedex_cover.icelands_seen !== undefined) {
      addScore(charts_with_name(charts, "Icelands Seen"),   result.pokedex_cover.icelands_seen);
      addScore(charts_with_name(charts, "Icelands Caught"), result.pokedex_cover.icelands_caught);
    }
  }
};

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects weights and heights from the pokedex page',
    'detects individual research values and the research level from the research tasks page',
    'detects seen/caught numbers and research points from the pokédex cover screen',
  ],
  limitations: [
    'the pokedex must be ordered numerically',
    'does not detect alternate forms, you must manually select them',
    'only works with 1280x720px native Switch screenshots',
    'only detects weights in lbs. and heights in ft/in',
    'only tested with English screenshots, other language may not work',
  ],
};
