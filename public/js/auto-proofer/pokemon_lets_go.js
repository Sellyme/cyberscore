export function registerScores(result, addScore, game) {
  if (result.index) {
    const charts = charts_in_groups(game, "Pokédex");

    addScore(charts_with_name(charts, "Seen"), result.index.pokemon_seen);
    addScore(charts_with_name(charts, "Obtained"), result.index.pokemon_obtained);
  }

  if (result.pokedex) {
    const dex = `#${result.pokedex.dex.toString().padStart(3, "0")}`;
    const values = [
      result.pokedex.weight_min,
      result.pokedex.weight_max,
      result.pokedex.height_min,
      result.pokedex.height_max,
    ];

    ['Lightest', 'Heaviest', 'Shortest', 'Tallest'].map((groupName) =>
      result.pokedex.alola ?
        charts_with_name(charts_with_name(charts_in_groups(game, groupName), dex), "Alola") :
        charts_with_name(charts_in_groups(game, groupName), dex)
    ).forEach((charts, index) => {
      let s1 = result.pokedex.metric ? values[index] : null;
      let s2 = !result.pokedex.metric ? values[index] : null;
      addScore(charts, s1, s2);
    });

    addScore(
      charts_with_name(charts_in_groups(game, "Number Caught"), dex),
      result.pokedex.number_caught
    );
  }

  if (result.park) {
    const score_charts = charts_in_groups(game, "Highest Score");
    const time_charts = charts_in_groups(game, "Quickest Time");

    for (let i = 0; i < result.park.dex.length; i++) {
      const dex = `#${result.park.dex[i].toString().padStart(3, "0")}`;

      addScore(charts_with_name(score_charts, dex), result.park.points[i]);
      addScore(charts_with_name(time_charts, dex),  result.park.time[i]);
    }
  }
}

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects pokédex seen, obtained, and total caught from the pokédex index screen',
    'detects weights (lbs or kg), heights (ft+in or m), and number caught from individual pokédex screens',
    'detects Go Park points and time from the pokemon selection screen',
  ],
  limitations: [
    'Go Park detection only works with english pokémon names',
  ],
};
