export function registerScores(result, addScore, game) {
  if (result.profile) {
    const charts = charts_in_groups(game, "Adventure Records");

    addScore(charts_with_name(charts, "Badges Collected"), result.profile.badges_collected);
    addScore(charts_with_name(charts, "Caught"), result.profile.pokedex_caught);
    addScore(charts_with_name(charts, "Paldea Pokédex – Battled"), result.profile.pokedex_seen);
    addScore(charts_with_name(charts, "Shiny Pokémon Battled"), result.profile.shiny_pokemon_battled);
    addScore(charts_with_name(charts, "Recipes Collected"), result.profile.recipes_collected);
  }

  if (result.sandwich) {
    addScore(
      charts_with_name(charts_in_groups(game, "Sandwiches created"), result.sandwich.name).slice(0,1),
      result.sandwich.times_made,
    );
  }
}

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects adventure card scores',
    'detects sandwiches made',
  ],
  limitations: [
  ],
}
