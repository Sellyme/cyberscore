addLoadEvent(function() {	
	var rhsTabs = new YAHOO.ext.TabPanel("profilenav");
	rhsTabs.addTab("personal","Personal Details");
	rhsTabs.addTab("password","Password Security");
	rhsTabs.addTab("location","Location and Language");
	rhsTabs.addTab("contact","Contact Details");
	rhsTabs.addTab("prefs","Site Preferences");
	rhsTabs.addTab("others","Other Options");
	rhsTabs.activate("personal");
});