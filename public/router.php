<?php

set_include_path(__DIR__ . "/..");
require_once('includes/startup.php');
require_once('includes/router.php');
require_once('src/router.php');

$matched_route = route_match(
  $_SERVER['REQUEST_METHOD'],
  $_SERVER['REQUEST_URI']
);

if ($matched_route) {
  if (is_string($matched_route[0]) && file_exists($matched_route[0])) {
    extract_request_params($matched_route[1]);
    foreach ($matched_route[2] as $mw) {
      ($mw)();
    }

    log_event('page_request');

    require_once($matched_route[0]);
    global $cs;
    $cs->Exit();
  } else if ($matched_route[0] instanceof RouteRedirect) {
    foreach ($matched_route[2] as $mw) {
      ($mw)();
    }

    redirect_to($matched_route[0]->target($matched_route[1]));
    global $cs;
    $cs->Exit();
  }
}

http_response_code(404);
render_with("home/404", ['page_title' => "Page not found"]);
