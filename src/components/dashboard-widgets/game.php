<?php

$game = database_find_by('games', ['game_id' => $game_id]);
$game['game_name'] = $t->GetGameName($game_id);
$game['boxart'] = $cs->GetBoxArts($game_id)[0];

display_partial('dashboard-widgets/game', [
  'widget' => $widget,
  'game' => $game,
]);
