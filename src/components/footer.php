<?php
$num_online = database_single_value("
  SELECT COUNT(1) FROM users
  WHERE date_lastseen >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)
", '', []);

$site_lang_id = $t->GetSiteLang();

if (Authorization::has_access('Translator')) {
  $languages = database_get_all(database_select("
    SELECT * FROM languages
    WHERE sub_language = 'n'
    ORDER BY available ASC, language_code ASC
  ", '', []));
} else {
  $languages = database_get_all(database_select("
    SELECT * FROM languages
    WHERE sub_language = 'n' AND available = 'y'
    ORDER BY language_code ASC
  ", '', []));
}

render_component_template('footer', [
  'year' => date('Y'),
  'num_online' => $num_online,
  'languages' => $languages,
  'site_lang_id' => $site_lang_id,
  'execution_time' => round(execution_time(), 3),
  'database_queries' => array_map(fn ($x) => [sprintf("%.5f", $x[0]), trim(preg_replace("/\s+/", " ", $x[1])), json_encode($x[2])], database_queries()),
  'missing_translations' => array_unique($t->missing_translations),
]);
