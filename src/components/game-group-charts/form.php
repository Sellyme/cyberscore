<?php

$chart_types = [
  ['value' => 1, 'label' => $t->ReadChartType(1)],
  ['value' => 2, 'label' => $t->ReadChartType(2)],
  ['value' => 3, 'label' => $t->ReadChartType(3)],
  ['value' => 4, 'label' => $t->ReadChartType(4)],
  ['value' => 5, 'label' => $t->ReadChartType(5)],
];

$chart_flags = [];
foreach (Modifiers::$chart_flags as [$chart_flag, $modifier]) {
  $chart_flags []= [
    'identifier' => $chart_flag,
    'modifier' => $modifier,
    'label' => Modifiers::csp_modifier_label($chart_flag),
  ];
}

$csp_modifiers = [];
foreach (Modifiers::$csp_modifiers as [$identifier, $modifier, $arcade]) {
  $csp_modifiers []= [
    'identifier' => $identifier,
    'modifier' => $modifier,
    'formatted_modifier' => Modifiers::format_csp_modifier($modifier),
    'arcade' => $arcade,
    'category' => Modifiers::CSPModifierCategory($identifier),
    'category_label' => Modifiers::CSPModifierCategoryLabel($identifier),
    'subcategory' => Modifiers::CSPModifierSubcategory($identifier),
  ];
}

$csp_modifier_categories = group_by($csp_modifiers, fn($m) => $m['category']);

$selection ??= [
  'players' => 1,
  'level_rules' => [],
  'standard' => true,
  'speedrun' => false,
  'solution' => false,
  'arcade' => false,
  'challenge' => false,
  'incremental' => false,
  'collectible' => false,
  'unranked' => false,
  'gameplay_differences_device' => false,
  'gameplay_differences_game_version' => false,
  'premium_upgrades_and_consumables_earnable' => false,
  'premium_upgrades_and_consumables_premium_only' => false,
  'premium_upgrades_and_consumables_inaccessible' => false,
  'premium_upgrades_and_consumables_conditionally_accessible' => false,
  'accessibility_issues_inaccessible' => false,
  'accessibility_issues_conditionally_accessible' => false,
  'accessibility_issues_geographical' => false,
  'full_game' => false,
  'part_game' => false,
  'body_of_work' => false,
  'chart_type' => 1,
  'num_decimals' => -1,
  'score_prefix' => '',
  'name_pri' => '',
  'score_suffix' => '',
  'chart_type2' => 0,
  'num_decimals2' => -1,
  'score_prefix2' => '',
  'name_sec' => '',
  'score_suffix2' => '',
  'conversion_factor' => '',
  'name1' => '',
  'name2' => '',
  'name3' => '',
];

render_component_template('game-group-charts/form', [
  'chart_types' => $chart_types,
  'chart_flags' => $chart_flags,
  'csp_modifier_categories' => $csp_modifier_categories,
  'csp_modifiers' => $csp_modifiers,
  'selection' => $selection,
]);
