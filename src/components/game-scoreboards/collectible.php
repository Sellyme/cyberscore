<table class="scoreboard" id="scoreboard_classic">
<tr>
    <th colspan="4"></th>
    <th colspan="3" class="medals" style="min-width: 100px;"><img src="<?= skin_image_url('Dropdown_icons/icon_cyberstar.png') ?>" width="40" height="40" alt="<?= $t['general_cyberstars'] ?>" title="<?= $t['general_cyberstars'] ?>"/></th>

  </tr>
  <?php foreach ($scoreboard['entries'] as $g) { ?>
    <tr<?= $g['tr_class'] ?>>

          <!-- Trophy and/or scoreboard position -->
          <td class="pos">
        <?php if(is_numeric($g['pos_content'])) { echo nth($g['pos_content']); } 
              else echo $g['pos_content']; ?>

      <!-- Trophy Points -->
      <?php if ($g['collectible_trophy_points'] > 0) { ?>
      <br>
      <small>
          <img src="<?= skin_image_url('trophy_points_trophy.png') ?>" width="10" height="10" alt="<?= $t['general_trophy_points'] ?>" title="<?= $t['general_trophy_points'] ?>" /> 
          <?= number_format($g['collectible_trophy_points'], 0) ?>
      </small>
        <?php } ?>
      </td>

      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />

        <!-- Submission & Proof display -->
        <?php render_component_with('game-scoreboards/counters', ['game_id' => $game['game_id'], 'counter' => $g, 'chart_type' => 'collectible']); ?>
      </td>
      <td class="medals">
        <?= h(number_format($g['cyberstars'],0)) ?><br><?= $t['general_cyberstars'] ?>
        </td>
        <td class="medals">
          <small>
          <img src="<?= skin_image_url('Dropdown_icons/icon_cyberstar.png') ?>" width="12" height="12" alt="<?= $t['general_cyberstars'] ?>" title="<?= $t['general_cyberstars'] ?>"/> <?= number_format($g['cyberstars'] - ($g['num_approved_v']) - $g['num_approved'], 0) ?>
              <!-- Proof Counters -->
    <?php if ($g['num_approved'] > 0) { ?>
      <br>
      <!-- Image proof -->
      <img src="<?= skin_image_url('scoreboards/proof.png') ?>" height="12" width="12" alt="<?= $t['viewrecords_records_approved'] ?>" title="<?= $t['viewrecords_records_approved'] ?>">
      <?= h($g['num_approved'])?>

      <!-- Video proof -->
      <?php if ($g['num_approved_v'] > 0) { ?>
        <br>
        <img src="<?= skin_image_url('scoreboards/vproof.png') ?>" height="12" width="12" alt="<?= $t['scoreboards_total_subs_video_approved'] ?>" title="<?= $t['scoreboards_total_subs_video_approved'] ?>">
        <?= h($g['num_approved_v']) ?>
      <?php } 
      } ?>
      </td>
    </tr>
  <?php } ?>
</table>
