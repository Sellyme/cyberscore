<a href="/records?user_id=<?= h($counter['user_id']) ?>&games=<?= h($game_id) ?>&chartstatus=<?= h($chart_type) ?>" title="View records">
  <small>
    <!-- Submissions -->
    <img src=" <?= skin_image_url('scoreboards/subs.png') ?>" height="12" width="12" alt="<?= $t['general_submissions'] ?>" title="<?= $t['general_submissions'] ?>">
    <?= h($counter['num_subs'])?>
</a>

    <!-- Proof Counters -->
    <?php if ($counter['num_approved'] > 0) { ?>
      <!-- Image proof -->
      <a href="/records?user_id=<?= h($counter['user_id']) ?>&games=<?= h($game_id) ?>&chartstatus=<?= h($chart_type) ?>&selectstatus=3" title="View records">
      <img src="<?= skin_image_url('scoreboards/proof.png') ?>" height="12" width="12" alt="<?= $t['viewrecords_records_approved'] ?>" title="<?= $t['viewrecords_records_approved'] ?>">
      <?= h($counter['num_approved'])?></a>

      <!-- Video proof -->
      <?php if ($counter['num_approved_v'] > 0) { ?>
        <a href="/records?user_id=<?= h($counter['user_id']) ?>&games=<?= h($game_id) ?>&chartstatus=<?= h($chart_type) ?>&selectstatus=3&proof_type=videos" title="View records">
        <img src="<?= skin_image_url('scoreboards/vproof.png') ?>" height="12" width="12" alt="<?= $t['scoreboards_total_subs_video_approved'] ?>" title="<?= $t['scoreboards_total_subs_video_approved'] ?>">
        <?= h($counter['num_approved_v']) ?></a>
      <?php } ?>
    <?php } ?>

    <!-- Compare records -->
    <?php if (isset($current_user['user_id']) && $current_user['user_id'] != $counter['user_id']) { ?>
          <a href="/compare.php?user_id1=<?= h($current_user['user_id']) ?>&user_id2=<?= h($counter['user_id']) ?>&game_id=<?= h($game_id) ?>">
            <img src="/images/compare.gif" width="12" height="12" alt="<?= h($t['compare_title']) ?>" title="<?= h($t['compare_title']) ?>"/>
          </a>
        <?php } ?>
  </small>

