<?php
$boards = ['csp', 'standard', 'arcade', 'speedrun', 'solution', 'challenge', 'incremental', 'collectible'];

$totals = database_get(database_select("SELECT
SUM(platinum) AS total_platinum,
SUM(gold) AS total_gold,
SUM(silver) AS total_silver,
SUM(bronze) AS total_bronze,
SUM(trophy_points) AS total_points,
SUM(csp_trophy_points) AS csp,
SUM(standard_trophy_points) AS standard,
SUM(arcade_trophy_points) AS arcade,
SUM(speedrun_trophy_points) AS speedrun,
SUM(solution_trophy_points) AS solution,
SUM(challenge_trophy_points) AS challenge,
SUM(incremental_trophy_points) AS incremental,
SUM(collectible_trophy_points) AS collectible,
SUM(CASE WHEN csp_trophy = 'platinum' THEN 1 END) AS platinum,
SUM(CASE WHEN csp_trophy = 'gold' OR csp_trophy = 'platinum' THEN 1 END) AS csp_gold,
SUM(CASE WHEN csp_trophy = 'silver' THEN 1 END) AS csp_silver,
SUM(CASE WHEN csp_trophy = 'bronze' THEN 1 END) AS csp_bronze,
SUM(CASE WHEN standard_trophy = 'gold' THEN 1 END) AS standard_gold,
SUM(CASE WHEN standard_trophy = 'silver' THEN 1 END) AS standard_silver,
SUM(CASE WHEN standard_trophy = 'bronze' THEN 1 END) AS standard_bronze,
SUM(CASE WHEN arcade_trophy = 'gold' THEN 1 END) AS arcade_gold,
SUM(CASE WHEN arcade_trophy = 'silver' THEN 1 END) AS arcade_silver,
SUM(CASE WHEN arcade_trophy = 'bronze' THEN 1 END) AS arcade_bronze,
SUM(CASE WHEN speedrun_trophy = 'gold' THEN 1 END) AS speedrun_gold,
SUM(CASE WHEN speedrun_trophy = 'silver' THEN 1 END) AS speedrun_silver,
SUM(CASE WHEN speedrun_trophy = 'bronze' THEN 1 END) AS speedrun_bronze,
SUM(CASE WHEN solution_trophy = 'gold' THEN 1 END) AS solution_gold,
SUM(CASE WHEN solution_trophy = 'silver' THEN 1 END) AS solution_silver,
SUM(CASE WHEN solution_trophy = 'bronze' THEN 1 END) AS solution_bronze,
SUM(CASE WHEN challenge_trophy = 'gold' THEN 1 END) AS challenge_gold,
SUM(CASE WHEN challenge_trophy = 'silver' THEN 1 END) AS challenge_silver,
SUM(CASE WHEN challenge_trophy = 'bronze' THEN 1 END) AS challenge_bronze,
SUM(CASE WHEN incremental_trophy = 'gold' THEN 1 END) AS incremental_gold,
SUM(CASE WHEN incremental_trophy = 'silver' THEN 1 END) AS incremental_silver,
SUM(CASE WHEN incremental_trophy = 'bronze' THEN 1 END) AS incremental_bronze,
SUM(CASE WHEN collectible_trophy = 'gold' THEN 1 END) AS collectible_gold,
SUM(CASE WHEN collectible_trophy = 'silver' THEN 1 END) AS collectible_silver,
SUM(CASE WHEN collectible_trophy = 'bronze' THEN 1 END) AS collectible_bronze
FROM gsb_cache_trophy WHERE gsb_cache_trophy.game_id = ?", 's', [$game['game_id']]));
 ?>

<table class="zebraa" style="padding-top: 10px; padding-bottom: 5px; text-align: center;" id="scoreboard_classic">
  <tr style="padding-top: 10px; padding-bottom: 5px; text-align: center;">
    <!-- User -->
    <th style="width:90px"></th>
    <!-- Name -->
    <th style="width:250px;"></th>
    <!-- Trophies -->
    <!-- CSP -->
    <?php if($totals['csp'] != 0) { ?>
    <th class="trophy-csp" style="padding-right: 3px; width: 125px;">
      <img style="max-height: 32px; width: auto;" src="/images/icon_trophy.png" alt="CSP Scoreboard" title="CSP Scoreboard">
    </th>
    <?php } ?>
    <?php if($totals['standard'] != 0) { ?>
    <!-- Medals -->
    <th class="trophy-standard" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_ranked.png" alt="Medal Table" title="Medal Table"></th>
    <?php } ?>
    <?php if($totals['arcade'] != 0) { ?>
    <!-- Arcade -->
    <th class="trophy-arcade" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_arcade.png" alt="Arcade Board" title="Arcade Board"></th>
    <?php } ?>
    <?php if($totals['speedrun'] != 0) { ?>
    <!-- Speedrun -->
    <th class="trophy-speedrun" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_speedrun.png" alt="Speedrun Table" title="Speedrun Table"></th>
    <?php } ?>
    <?php if($totals['solution'] != 0) { ?>
    <!-- Solution -->
    <th class="trophy-solution" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_solution.png" alt="Solution Hub" title="Solution Hub"></th>
    <?php } ?>
    <?php if($totals['challenge'] != 0) { ?>
    <!-- Challenge -->
    <th class="trophy-challenge" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_challenge.png" alt="Challenge Board" title="Challenge Board"></th>
    <?php } ?>
    <?php if($totals['incremental'] != 0) { ?>
    <!-- Incremental -->
    <th class="trophy-incremental" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_incremental.png" alt="Experience Table" title="Experience Table"></th>
    <?php } ?>
    <?php if($totals['collectible'] != 0) { ?>
    <!-- Collectible -->
    <th class="trophy-collectible" style="padding-right: 3px; width: 125px; padding-top: 10px; padding-bottom: 5px; "><img style="max-height: 32px; width: auto;" src="/images/icon_collectible.png" alt="Collector's Cache" title="Collector's Cache"></th>
    <?php } ?>
  </tr>

  <?php foreach($scoreboard['entries'] as $this_user) { ?>
    <tr>
      <td style="width: 58px; text-align: center; padding-left: 20px;"><?= user_avatar_image_tag($this_user) ?></td>
      <td><small><a href="/user/<?= h($this_user['user_id']) ?>"><?= $this_user['display_name'] ?></a></small>
        <br>
        <table style="max-width: 150px; margin: 0 auto; text-align: center;">
          <th style="max-width: 30px; max-height: 10px; text-align: center;">
            <img
              src="/skins4/default/images/trophy_points_trophy.png"
              alt="Trophy Points"
              title="Trophy Points"
              width="16"
              height="16" />
            <br />
            <small><?= number_format($this_user['trophy_points'],0) ?></small>
          </th>
          <th style="max-width: 30px; max-height: 10px; text-align: center;">
            <img
              src="/skins4/default/images/trophy_1.png"
              alt="Platinum"
              title="Platinum"
              width="16"
              height="16" />
            <br />
            <small><?=  number_format($this_user['platinum'],0) ?></small>
          </th>
          <th style="max-width: 30px; max-height: 10px; text-align: center;">
            <img
              src="/skins4/default/images/trophy_2.png"
              alt="Golds"
              title="Golds"
              width="16"
              height="16" />
            <br />
            <small><?=  number_format($this_user['gold'],0) ?></small>
          </th>
          <th style="max-width: 30px; max-height: 10px; text-align: center;">
            <img
              src="/skins4/default/images/trophy_3.png"
              alt="Silvers"
              title="Silvers"
              width="16"
              height="16" />
            <br />
            <small><?=  number_format($this_user['silver'],0) ?></small>
          </th>
          <th style="max-width: 30px;max-height: 10px; text-align: center;">
            <img
              src="/skins4/default/images/trophy_4.png"
              alt="Bronzes"
              title="Bronzes"
              width="16"
              height="16" />
            <br />
            <small><?=  number_format($this_user['bronze'],0) ?></small>
          </th>
        </table>
      </td>

<?php foreach($boards as $board) { ?>
      <?php if($totals[$board] != 0) { ?>
        <?php if($this_user[$board.'_trophy'] || $this_user[$board.'_trophy_points']) { ?>
          <td class="trophy-<?= $board ?>">
            <a href="/game_scoreboard.php?game_id=<?= $this_user['game_id'] ?>&amp;board=<?php if($board == 'standard') { echo 'medal'; } else echo $board; ?>">
              <?php if($this_user[$board.'_trophy']) { ?>
                <?php if($this_user[$board.'_trophy'] == 'platinum') { ?>
                  <img src="<?= skin_image_url('trophy_1.png') ?>" width="18" height="18" style="vertical-align: middle; margin-top: 1px;" alt="Platinum trophy" title="Platinum trophy"/>
                  <?php } else if($this_user[$board.'_trophy'] == 'gold') { ?>
                  <img src="<?= skin_image_url('trophy_2.png') ?>" width="18" height="18" style="vertical-align: middle; margin-top: 1px;" alt="Gold trophy" title="Gold trophy"/>
                  <?php } else if($this_user[$board.'_trophy'] == 'silver') { ?>
                  <img src="<?= skin_image_url('trophy_3.png') ?>" width="18" height="18" style="vertical-align: middle; margin-top: 1px;" alt="Silver trophy" title="Silver trophy"/>
                  <?php } else if($this_user[$board.'_trophy'] == 'bronze') { ?>
                  <img src="<?= skin_image_url('trophy_4.png') ?>" width="18" height="18" style="vertical-align: middle; margin-top: 1px;" alt="Bronze trophy" title="Bronze trophy"/>
                  <?php } else if($this_user[$board.'_trophy'] == 'fourth') { ?>
                  <img src="<?= skin_image_url('trophy_5.png') ?>" width="18" height="18" style="vertical-align: middle; margin-top: 1px;" alt="Fourth-place trophy" title="Fourth-place trophy"/>
                  <?php } else if($this_user[$board.'_trophy'] == 'fifth') { ?>
                  <img src="<?= skin_image_url('trophy_6.png') ?>" width="18" height="18" style="vertical-align: middle; margin-top: 1px;" alt="Fifth-place trophy" title="Fifth-place trophy"/>
                  <?php } else { ?>
                  <small><?= nth($this_user[$board.'_trophy']) ?></small>
                <?php }   
                    } ?>
                <br />

              <?php if($this_user[$board.'_trophy_points']) { ?>
                <img src="<?= skin_image_url('trophy_points_trophy.png') ?>" width="10" height="10" style="vertical-align: middle;" alt="Trophy Points"  title="Trophy Points" />
                <small><b><?= number_format($this_user[$board.'_trophy_points'],0) ?></b></small>
              <?php } ?>
            </a>
          </td>
          <?php } else { ?>
          <td class="trophy-locked">
          </td> <?php } ?>
      <?php } 
        }?>

      <td style="test-align: center;"></td>
    </tr>
<?php } ?>
</table>
