<?= h($t['leaderboards_unranked_group']) ?><br>
<?php echo str_replace(array('[url]', '[/url]'), array('<a href="https://www.soniccenter.org/">', '</a>'), $t['leaderboards_tsc']);
$a = 0;?>

<table class="scoreboard" id="scoreboard_classic">
<tr>
    <th colspan="4"></th>
    <th class="medals">Percent</th>
    <th class="medals">Points</th>
</tr>
  <?php foreach($scoreboard['entries'] as $g) { 
    $a++;
    ?>
    <tr<?= $g['tr_class'] ?>>
      <td class="pos"><?= $g['pos_content'] ?></td>
      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />
        <?php if (isset($g['num_subs'])) { ?>
          <small><?= h($g['num_subs'] == 1 ? $t['general_submission_singular'] : str_replace('[subs]', $g['num_subs'], $t['general_submission_plural'])) ?></small>
        <?php } ?>
      </td>
      <td class="medals">
        <?php if($a == 1) $colour = '#8b8888';
              else if($g['percentage'] >= 80) $colour = '#b61d1d';
              else if($g['percentage'] >= 60) $colour = '#ff8100';
              else if($g['percentage'] >= 40) $colour = '#ceba05';
              else if($g['percentage'] >= 20) $colour = '#199600';
              else if($g['percentage'] >= 0) $colour = '#4499cc'; ?>

      <span style="color: <?= $colour ?> "><b><?= h(number_format($g['percentage'], 1)) ?>%</b></span>
      </td>
      <td class="medals"><?= h(number_format($g['score'], 0)) ?>
      </td>
    </tr>
  <?php } ?>
</table>
