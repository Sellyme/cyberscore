<!-- This ordering is not very efficient but it is deliberate to enable putting the buttons in a specific order. -->
<?php if (Authorization::has_access(['ProofMod', 'GameMod', 'Designer'])) { ?>
  <details class="sidebar-section" data-id="staff-tools" open="" ontoggle="rememberDetailsState(this)">
        <summary class="sidebar-title">Staff Tools</summary><br>
  <div style="display: flex; flex-wrap: wrap; margin-bottom: 0px; padding-left: 0px; gap: 3px; justify-content: center;">
    <?php if (Authorization::has_access('GameMod')) { ?>
      <a id="link-square" style="background: url(/images/button_6.jpg); background-size:cover;" href="/mod_scripts/edit_game.php?id=<?= h($game['game_id']) ?>">
        Edit game info
      </a>
    <?php } ?>
    <?php if (Authorization::has_access(['GameMod', 'ProofMod'])) { ?>
      <a id="link-square" style="background: url(/images/button_7.jpg); background-size:cover;" href="/mod_scripts/edit_game_groups.php?game_id=<?= h($game['game_id']) ?>">
        Edit game charts
      </a>

    <?php } ?>
    <?php if (Authorization::has_access('GameMod')) { ?>
      <a id="link-square" style="background: url(/images/button_11.jpg); background-size:cover;" href="/rules/index.php">
        Rule Editor
      </a>
      <?php } ?>
      <?php if (Authorization::has_access(['GameMod', 'ProofMod'])) { ?>
      <a id="link-square" style="background: url(/images/button_12.jpg); background-size:cover;" href="/proof-rules/index.php">
        Proof <span style="font-size: 0.85em">Guidelines</span>
      </a>
      <?php } ?>

    <?php if (Authorization::has_access('GameMod')) { ?>
      <a id="link-square" style="background: url(/images/button_10.jpg); background-size:cover;" href="/game-series">
        Series <span style="font-size: 0.85em">Management</span>
      </a>
      <a id="link-square" style="background: url(/images/button_13.jpg); background-size:cover;" href="/admin/rebuild_center">
        Rebuild Centre
      </a>
    <?php } ?>
    <?php if (Authorization::has_access(['GameMod', 'Designer'])) { ?>
      <a id="link-square" style="background: url(/images/button_8.png); background-size:cover;" href="/boxarts/new?game_id=<?= h($game['game_id']) ?>">
        Upload Boxart
      </a>
      <a id="link-square" style="background: url(/images/button_9.jpg); background-size:cover;" href="/rankbuttons/new">
        Upload <span style="font-size: 0.85em">Rankbuttons</span>
      </a>
    <?php } ?>
  </div>
    </details>
<?php } ?>


