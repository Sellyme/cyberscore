<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="scoreboardCSR">
      <?= h(round($entry['style_points'], 1)) ?>
      <?= h($entry['style_points'] != 1 ? $t['general_challenge_points_plural'] : $t['general_challenge_points_singular']) ?>
    </td>
  </tr>
<?php } ?>
