<tr>
  <th colspan="4"></th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'platinum']) ?>">
      <img src="<?= skin_image_url('platinummedal.png') ?>" width="40" height="40" alt="platinum" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'gold']) ?>">
      <img src="<?= skin_image_url('goldmedal.png') ?>" width="40" height="40" alt="gold medal" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'silver']) ?>">
      <img src="<?= skin_image_url('silvermedal.png') ?>" width="40" height="40" alt="silver medal" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'bronze']) ?>">
      <img src="<?= skin_image_url('bronzemedal.png') ?>" width="40" height="40" alt="bronze medal" />
    </a>
  </th>
</tr>
<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="medals"><?= h(number_format($entry['platinum'])) ?></td>
      <td class="medals"><?= h(number_format($entry['gold'])) ?></td>
      <td class="medals"><?= h(number_format($entry['silver'])) ?></td>
      <td class="medals"><?= h(number_format($entry['bronze'])) ?></td>
  </tr>
<?php } ?>
