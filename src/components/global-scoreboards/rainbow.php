<tr>
  <th colspan="4"></th>
  <th style="text-align: center;">
    <img src="/images/rainbow_star.png" height="75" alt="Rainbow Power" />
    <br />
    <?= h($t['scoreboards_rp']) ?>
  </th>
  <th></th>

</tr>
<?php
$ymax = 0;
foreach ($entries as $entry) {
  $ymax = max([
    $ymax,
    $entry['solution_hub'],
    $entry['arcade_board'],
    $entry['experience_ranking'],
    $entry['proof_board'],
    $entry['video_proof_board'],
    $entry['collectors_cache'],
    $entry['speedrun_table'],
    $entry['trophy_table'],
    $entry['medal_table'],
    $entry['starboard'],
    $entry['total_submissions_board'],
    $entry['challenge_board'],
  ]);
}

// Implementation of "Nice numbers for graph labels" from Graphics Gems, vol. 1, originally by Paul S. Heckbert, University of California, Berkeley, California.
$exp = floor(log10($ymax));
$f = $ymax / (pow(10, $exp));
if ($f < 1) $nf = 1;
else if ($f < 3) $nf = 3;
else if ($f < 5) $nf = 5;
else $nf = 10;

$ymax = $nf * pow(10, $exp);
?>

<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td style="text-align: center;">
    <?php if ($entry['has_bonus']) {?>
      <small>
        <?= h(number_format($entry['base_rainbow_power'], 0)) ?> base <?= h($t['scoreboards_rp_short']) ?>
        <br /> (Av. rank <?= h(nth(number_format(round($entry['avg_pos']),0), 0)) ?> = <?= h(number_format($entry['multiplier'],2)) ?>x multiplier)
      </small>
    <br />
    <?php } ?>
    <h2><?= h(number_format($entry['rainbow_power'], 0)) ?> <?= h($t['scoreboards_rp_short']) ?></h2>
    </td>
    <td class="total" style="text-align: center; padding-right: 100px; width: 400px;">
      <canvas id="<?php echo 'rainbow_'.$entry['user_id'];?>" width="400" height="140"></canvas>
    </td>

    <!-- Scoreboard position columns -->
    
    <!-- column 1 -->
    <td style="text-align: center; max-width: 32px; width: 32px; padding-right: 12px;">
      <small>
        <span style="color: #e57400; text-shadow: 1px 1px rgba(0,0,0, 0.2);">
          <img src="/images/scoreboards/arcade.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_arcade_table'] ?>" title="<?= $t['general_arcade_table'] ?>"/><br />
          <?php if($entry['arcade_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['arcade_pos']),0); ?>
        </span>
      </small>
    </td>

    <!-- column 2 -->
    <td style="text-align: center; max-width: 32px; width: 32px; padding-right: 12px;">
      <small>
        <span style="color: #211d87; text-shadow: 1px 1px rgba(0,0,0, 0.2);">
          <img src="/images/scoreboards/default.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_mainboard'] ?>" title="<?= $t['general_mainboard'] ?>" /><br />
          <?php if($entry['starboard_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['starboard_pos']),0); ?>
        </span>
        <br />
        <span style="color: #087114; text-shadow: 1px 1px rgba(0,0,0, 0.3);">
          <img src="/images/scoreboards/speedrun.svg" style="padding-top: 12px;" height="16" alt="<?= $t['general_speedrun_awards'] ?>" title="<?= $t['general_speedrun_awards'] ?>" /><br />
          <?php if($entry['speedrun_pos'] == 0) { echo "N/A"; } 
                else echo nth(number_format($entry['speedrun_pos']),0); ?>
        </span>
        <br />
        <span style="color: #8a2be2; text-shadow: 1px 1px rgba(0,0,0, 0.2); padding-bottom: 12px;">
          <img src="/images/icon_submissions.png" style="padding-top: 12px;"  height="16" alt="<?= $t['general_total_subs'] ?>" title="<?= $t['general_total_subs'] ?>" /><br />
          <?php if($entry['total_submissions_pos'] == 0) { echo "N/A"; } 
                else echo nth(number_format($entry['total_submissions_pos']),0); ?>
        </span>
      </small>
    </td>

    <!-- column 3 -->
    <td style="text-align: center; max-width: 32px; width: 32px; padding-right: 12px;">
      <small>
        <span style="color: #6eeae4; text-shadow: 1px 1px rgba(0,0,0, 0.3);">
          <img src="/images/scoreboards/trophy.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_trophy_table'] ?>" title="<?= $t['general_trophy_table'] ?>" /><br />
          <?php if($entry['trophy_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['trophy_pos']),0); ?>
        </span>
        <br />
        <span style="color: #ff0000; text-shadow: 1px 1px rgba(0,0,0, 0.2);">
          <img src="/images/scoreboards/solution.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_solution_hub'] ?>" title="<?= $t['general_solution_hub'] ?>" /><br />
          <?php if($entry['solution_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['solution_pos']),0); ?>
        </span>
        <br />
        <span style="color: #ffff00; text-shadow: 1px 1px rgba(0,0,0, 0.5); padding-bottom: 12px;">
          <img src="/images/icon_proof.png" style="padding-top: 12px;"  height="16" alt="<?= $t['general_proof_board'] ?>" title="<?= $t['general_proof_board'] ?>" /><br />
          <?php if($entry['proof_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['proof_pos']),0); ?>
        </span>
      </small>
    </td>

    <!-- column 4 -->
    <td style="text-align: center; max-width: 32px; width: 32px; padding-right: 12px;">
      <small>
        <span style="color: #1a6c96; text-shadow: 1px 1px rgba(0,0,0, 0.2);">
          <img src="/images/scoreboards/standard.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_medal_table'] ?>" title="<?= $t['general_medal_table'] ?>" /><br />
          <?php if($entry['medal_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['medal_pos']),0); ?>
        </span>
        <br />
        <span style="color: #871d85; text-shadow: 1px 1px rgba(0,0,0, 0.2);">
          <img src="/images/scoreboards/challenge.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_challenge_table'] ?>" title="<?= $t['general_challenge_table'] ?>" /><br />
          <?php if($entry['challenge_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['challenge_pos']),0); ?>
        </span>
        <br />
        <span style="color: #65f442; text-shadow: 1px 1px rgba(0,0,0, 0.5); padding-bottom: 12px;">
          <img src="/images/icon_vproof.png" style="padding-top: 12px;"  height="16" alt="<?= $t['general_vproof_board'] ?>" title="<?= $t['general_vproof_board'] ?>" /><br />
          <?php if($entry['video_proof_pos'] == 0) { echo "N/A"; } 
                else echo nth(number_format($entry['video_proof_pos']),0); ?>
        </span>
      </small>
    </td>

    <!-- column 5 -->
    <td style="text-align: center; max-width: 32px; width: 32px;">
      <small>
      <span style="color: #1fbe96; text-shadow: 1px 1px rgba(0,0,0, 0.5);">
        <img src="/images/scoreboards/collectible.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_collectors_cache'] ?>" title="<?= $t['general_collectors_cache'] ?>" /><br />
          <?php if($entry['collectible_pos'] == 0) { echo "N/A"; }
                else echo nth(number_format($entry['collectible_pos']),0); ?>
        </span>
      </small>
    </td>

    <!-- column 6 -->
    <td style="text-align: center; max-width: 32px; width: 32px; padding-right: 35px;">
      <small>
      <span style="color: #ceba05; text-shadow: 1px 1px rgba(0,0,0, 0.5);">
        <img src="/images/scoreboards/incremental.svg" style="padding-top: 12px;"  height="16" alt="<?= $t['general_experience_table'] ?>" title="<?= $t['general_experience_table'] ?>" /><br />
          <?php if($entry['incremental_pos'] == 0) { echo "N/A"; } 
                else echo nth(number_format($entry['incremental_pos']),0); ?>
        </span>
      </small>
    </td>

  </tr>
  <script>
    var user_id = <?= json_encode($entry['user_id'], JSON_HEX_TAG) ?>;
    var rainbow_id = 'rainbow_';
        rainbow_id += user_id;
    var rainbow_chart = document.getElementById(rainbow_id);

    var colors = [
      "#ff0000",
      "#e57400",
      "#ceba05",
      "#ffff00",
      "#65f442",
      "#1fbe96",
      "#087114",
      "#6eeae4",
      "#1a6c96",
      "#211d87",
      "#8a2be2",
      "#871d85",
    ];

    var values = [
      <?= json_encode($entry['solution_hub'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['arcade_board'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['experience_ranking'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['proof_board'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['video_proof_board'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['collectors_cache'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['speedrun_table'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['trophy_table'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['medal_table'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['starboard'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['total_submissions_board'], JSON_HEX_TAG) ?>,
      <?= json_encode($entry['challenge_board'], JSON_HEX_TAG) ?>,
    ];

    var labels = [
      "Solution hub score",
      "Arcade board score",
      "Experience table score",
      "Proof ranking score",
      "Video proof ranking score",
      "Collector's cache score",
      "Speedrun awards table score",
      "Trophy table score",
      "Medal table score",
      "Starboard score",
      "Submissions board score",
      "User challenge board score",
    ];

    // alternative representation using a stacked horizontal bar chart
    // var datasets = [];
    // for (var i = 0; i < labels.length; i++) {
    //   datasets[i] = {
    //     label: labels[i],
    //     data: [values[i]],
    //     backgroundColor: [colors[i]],
    //     hoverBackgroundColor: [colors[i]],
    //   };
    // }

    var datasets = [{
      backgroundColor: colors,
      hoverBackgroundColor: colors,
      data: values,
    }];

    var testChart = new Chart(rainbow_chart, {
      type: 'bar',
      data: { labels, datasets },
      options: {
        legend: { display: false },
        title: { display: false },
        tooltips: { mode: 'point' },
        scales: {
          xAxes: [{
            barPercentage: 1,
            ticks: { display: false },
          }],
          yAxes: [{
            ticks: { min: 0, max: <?= $ymax ?> },
          }],
        }
      }
    });
  </script>
<?php } ?>
