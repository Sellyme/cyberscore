<tr>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th></th>
  <th style="text-align: center;"><img src="/images/Star_Green16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Blue16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Blue16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Blue16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Blue16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_LightGreen16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Yellow16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Blue16.png"></th>
  <th style="text-align: center;"><img src="/images/Star_Black16.png"></th>
</tr>
</tr>
  <td style="text-align: center;">Rank</td>
  <td colspan='3' class='user'>Staff member</td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'teamwork_power']) ?>">Teamwork Power</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'total_tasks']) ?>">Total tasks completed</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'games_added']) ?>">Games added</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'record_investigated']) ?>">Records investigated</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'record_moved']) ?>">Records moved</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'record_deleted']) ?>">Records deleted</a> / <a href="<?= current_url_with_query(['manual_sort' => 'record_reinstated']) ?>">reinstated</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'record_reverted']) ?>">Records reverted</a></td>
  <td>Proofs <a href="<?= current_url_with_query(['manual_sort' => 'photo_proof_approved']) ?>">photo approved</a> + <a href="<?= current_url_with_query(['manual_sort' => 'video_proof_approved']) ?>">video approved</a> / <a href="<?= current_url_with_query(['manual_sort' => 'proof_refused']) ?>">refused</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'rankbutton_uploaded']) ?>">Rankbuttons uploaded</a></td>
  <td>Support requests <a href="<?= current_url_with_query(['manual_sort' => 'support_request_archived']) ?>">completed</a> / <a href="<?= current_url_with_query(['manual_sort' => 'support_request_deleted']) ?>">deleted</a></td>
  <td><a href="<?= current_url_with_query(['manual_sort' => 'news_article_written']) ?>">News articles written</a></td>
</tr>
<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="scoreboardCSR">
      <?= h(number_format($entry['teamwork_power'])) ?>
    </td>
    <td><?= h($entry['total_tasks']) ?></td>
    <td><?= h($entry['games_added']) ?></td>
    <td><?= h($entry['record_investigated']) ?></td>
    <td><?= h($entry['record_moved']) ?></td>
    <td><?= h($entry['record_deleted']) ?> / <?= h($entry['record_reinstated']) ?></td>
    <td><?= h($entry['record_reverted']) ?></td>
    <td><?= h($entry['photo_proof_approved']) ?> + <?= h($entry['video_proof_approved']) ?> / <?= h($entry['proof_refused']) ?></td>
    <td><?= h($entry['rankbutton_uploaded']) ?></td>
    <td><?= h($entry['support_request_archived']) ?> / <?= h($entry['support_request_deleted']) ?></td>
    <td><?= h($entry['news_article_written']) ?></td>
  </tr>
<?php } ?>
