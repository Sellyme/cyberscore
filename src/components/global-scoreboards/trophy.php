<tr>
  <th colspan="4"></th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'points']) ?>">
      <img src="<?= skin_image_url('trophy_points_trophy.png') ?>" width="40" height="40" alt="trophy points" />
      <?= $t['general_trophy_points'] ?>
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'platinum']) ?>">
      <img src="<?= skin_image_url('trophy_1.png') ?>" width="40" height="40" alt="platinum" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'gold']) ?>">
      <img src="<?= skin_image_url('trophy_2.png') ?>" width="40" height="40" alt="gold trophy" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'silver']) ?>">
      <img src="<?= skin_image_url('trophy_3.png') ?>" width="40" height="40" alt="silver trophy" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'bronze']) ?>">
      <img src="<?= skin_image_url('trophy_4.png') ?>" width="40" height="40" alt="bronze trophy" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => '4th']) ?>">
      <img src="<?= skin_image_url('trophy_5.png') ?>" width="40" height="40" alt="4th medal" />
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => '5th']) ?>">
      <img src="<?= skin_image_url('trophy_6.png') ?>" width="40" height="40" alt="5th medal" />
    </a>
  </th>
</tr>
<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="medals"><?= h(number_format($entry['trophy_points'])) ?></td>
    <td class="medals"><?= h(number_format($entry['trophy_platinum'])) ?></td>
    <td class="medals"><?= h(number_format($entry['trophy_gold'])) ?></td>
    <td class="medals"><?= h(number_format($entry['trophy_silver'])) ?></td>
    <td class="medals"><?= h(number_format($entry['trophy_bronze'])) ?></td>
    <td class="medals"><?= h(number_format($entry['trophy_fourth'])) ?></td>
    <td class="medals"><?= h(number_format($entry['trophy_fifth'])) ?></td>
  </tr>
<?php } ?>
