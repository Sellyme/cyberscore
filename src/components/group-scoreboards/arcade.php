<table class="scoreboard" id="scoreboard_classic">
  <tr>
    <th colspan="4"></th>
    <th class="medals"><img src="<?= skin_image_url('scoreboards/arcade.png') ?>" width="100" alt="<?= $t['general_ap'] ?>" title="<?= $t['general_ap'] ?>"/><br /><?= $t['general_ap'] ?></th>
  </tr>
  <?php foreach ($scoreboard['entries'] as $g) { ?>
    <tr<?= $g['tr_class'] ?>>
      <td class="pos"><?= $g['pos_content'] ?></td>
      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />
        <?php if (isset($g['num_subs'])) { ?>
          <small><?= h($g['num_subs'] == 1 ? $t['general_submission_singular'] : str_replace('[subs]', $g['num_subs'], $t['general_submission_plural'])) ?></small>
        <?php } ?>
      </td>
      <td class="medals">
        <?= h(number_format($g['score'])) ?> <?= h($t['general_ap']) ?>
      </td>
    </tr>
  <?php } ?>
</table>
