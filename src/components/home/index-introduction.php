<?php
$stats = database_fetch_one("SELECT * FROM stats", []);

$root = $config['app']['root'];
$files = glob("$root/public/uploads/homepics/small/*");
$homepic = $files[array_rand($files)];
$homepic = explode("/public/", $homepic)[1];

render_component_template('home/index-introduction', [
  'users' => $stats['users'],
  'games' => $stats['games'],
  'records' => $stats['records'],
  'homepic' => $homepic,
]);

/*
// Get random boxart instead of homepic
$boxarts = database_fetch_all("SELECT * FROM boxarts", []);
shuffle($boxarts);
$boxart = $boxarts[0];
$boxart['url'] = "/uploads/boxarts/{$boxart['sha256']}";

<a href="/games/{{ boxart.game_id }}" class="home-index-introduction-picture">
  <img src="{{ boxart.url }}" />
</a>
*/
