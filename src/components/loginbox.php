<?php

if ($current_user) {
  $unread_csmails = database_single_value("
    SELECT COUNT(1)
    FROM csmail
    WHERE to_id = ? AND to_status = 0
  ", 's', [$current_user['user_id']]);

  $unread_notifications = database_single_value("
    SELECT COUNT(1)
    FROM notifications
    WHERE note_seen = 'n' AND user_id = ?
  ", 's', [$current_user['user_id']]);

  $has_notifications = $unread_csmails > 0 || $unread_notifications > 0;

  $queued_records = database_single_value("
    SELECT COUNT(1)
    FROM queued_records
    WHERE user_id = ?
  ", 's', [$current_user['user_id']]);
} else {
  $unread_csmails = 0;
  $unread_notifications = 0;
  $has_notifications = false;
  $queued_records = 0;
}

render_component_template('loginbox', [
  'current_user' => $current_user,
  'unread_csmails' => $unread_csmails,
  'unread_notifications' => $unread_notifications,
  'has_notifications' => $has_notifications,
  'queued_records' => $queued_records,
  'is_impersonator' => Session::Fetch('impersonator_id') !== null,
]);
