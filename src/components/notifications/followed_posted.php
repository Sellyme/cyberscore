<div class="notification-item__icon">
  <i class="xxx fas fa-blog"></i>
</div>

<div class="notification-item__message">
  <strong><?= h(str_replace('[name]', $notification['sender_username'], $t['notifications_type_followed_posted'])) ?></strong>
  <br>
  <q><?= h(first_line($notification['news_headline'])) ?></q>
</div>
