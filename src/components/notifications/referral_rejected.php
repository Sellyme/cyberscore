<div class="notification-item__icon">
  <i class="xxx fas fa-users-slash"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_referral_rejected_3'] ?></strong>
  <br>
  <?php if ($user_id == $notification['referrer_id']) { ?>
    <?= h(str_replace('[user]', $notification['referred_username'], $t['notifications_type_referral_rejected_1'])) ?>
  <?php } else { ?>
    <?= h(str_replace('[user]', $notification['referrer_username'], $t['notifications_type_referral_rejected_2'])) ?>
  <?php } ?>
</div>
