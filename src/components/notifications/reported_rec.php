<div class="notification-item__icon">
  <i class="xxx fas fa-search"></i>
</div>

<div class="notification-item__message">
  <?php if ($notification['target_user_id']) { ?>
    <strong>
      <?= str_replace(['[user]', '[user 2]'], [$notification['sender_username'] ?? $t['notifications_type_reported_mod_2'], $notification['target_username']], $t['notifications_type_reported_mod']) ?>
    </strong>
    <br>
    <?= $notification['game_name'] ?> &gt;
    <?= $notification['group_name'] ?> &gt;
    <?= $notification['level_name'] ?>
  <?php } else { ?>
    <strong>
      <?= str_replace('[user]', $notification['sender_username'] ?? $t['notifications_type_reported_mod_2'], $t['notifications_type_reported_mod_3']) ?>
    </strong>
  <?php } ?>
</div>
