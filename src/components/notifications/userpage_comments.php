<div class="notification-item__icon">
  <i class="xxx far fa-comment"></i>
</div>

<div class="notification-item__message">
  <strong>
    <?= h(str_replace('[user]', $notification['sender_username'], $t['notifications_type_userpage_comment'])) ?>
  </strong>
</div>
