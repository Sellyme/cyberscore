<?php
$best_days = database_get_all(database_select("
  SELECT
    DATE_FORMAT(approval_date, '%Y-%m-%d') AS date,
    COUNT(*) AS num_approved
  FROM record_approvals
  GROUP BY date
  ORDER BY COUNT(*) DESC LIMIT 20
", '', []));

render_component_template('proof-stats/best_days', [
  'best_days' => $best_days,
]);
