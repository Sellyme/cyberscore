<?php

$months = database_get_all(database_select("
  SELECT
    DATE_FORMAT(approval_date, '%Y-%m') AS month,
    COUNT(*) AS num_approved
  FROM record_approvals
  GROUP BY month
  ORDER BY month
", '', []));

render_component_template('proof-stats/month_history', [
  'months' => $months,
]);
