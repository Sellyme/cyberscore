<?php

// TODO: why is this not just using current_user?
$moderator = UsersRepository::get($current_user['user_id']);

if (!$moderator) {
  exit;
}

$approved_users = database_get_all(database_select("
  SELECT COUNT(*) AS num, users.*
  FROM record_approvals
  JOIN records USING(record_id)
  JOIN users ON records.user_id = users.user_id
  WHERE record_approvals.user_id = ?
  GROUP BY users.user_id
  ORDER BY num DESC
", 's', [$moderator['user_id']]));

render_component_template('proof-stats/your_users', [
  'approved_users' => $approved_users,
]);
