<?php

$flairs = ChartIcon::all_selections($current_user);

render_component_template('settings/charticons', [
  'flairs' => $flairs,
]);
