<?php

$blank_country = $t->GetCountryName(1);
$countries = $t->GetCountryNames();

$has_avatar = Uploads::exists('user-avatar', $current_user['user_id'], 'variant');
$has_banner = Uploads::exists('user-banner', $current_user['user_id'], 'legacy');

render_component_template('settings/personal', [
  'blank_country' => $blank_country,
  'countries' => $countries,
  'has_avatar' => $has_avatar,
  'has_banner' => $has_banner,
]);
