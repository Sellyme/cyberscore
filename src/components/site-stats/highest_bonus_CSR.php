<?php
$highestbonuscsr = database_get_all(database_select("
  SELECT bonus_csr, game_id, user_id, scoreboard_pos
  FROM gsb_cache_csp
  ORDER BY bonus_csr DESC LIMIT 100
", '', []));

foreach ($highestbonuscsr as &$user) {
  $user['username'] = database_single_value("SELECT username FROM users WHERE user_id = ?", 's', [$user['user_id']]);
  $user['gamename'] = database_single_value("SELECT game_name FROM games WHERE game_id = ?", 's', [$user['game_id']]);
}
unset($user);

render_component_template('site-stats/highest_bonus_CSR', [
  'highestbonuscsr' => $highestbonuscsr,
]);
