<?php

$biggestsubdays = database_get_all(database_select("
  SELECT
    CONCAT(YEAR(original_date), '-', MONTH(original_date), '-', DAY(original_date)) AS date,
    COUNT(*) AS num_subs,
    user_id,
    username
  FROM records
  JOIN users USING (user_id)
  GROUP BY user_id, date
  ORDER BY num_subs DESC
  LIMIT 25
", '', []));

render_component_template('site-stats/highest_sub_days', [
  'biggestsubdays' => $biggestsubdays,
]);
