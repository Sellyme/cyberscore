<?php

render_component_template('site-stats/proven_golds', [
  'mostprovengolds' => database_get_all(database_select("
    SELECT COUNT(chart_pos) AS count, user_id, username
    FROM records
    JOIN users USING (user_id)
    WHERE rec_status = 3 AND chart_pos = 1 AND ranked = 1 AND chart_subs >= 3
    GROUP BY user_id
    ORDER BY count DESC
    LIMIT 100
  ", '', [])),
]);
