<?php

render_component_template('two-panel-multiselect', [
  'entries' => $entries,
  'name' => $name,
  'value' => $value,
  'label' => $label,
  'selected' => $selected,
]);
