<?php

require_once("includes/article.php");
$a = new Article();

$iamme = $user['user_id'] == $current_user['user_id'];
$following = database_find_by('user_blog_follows', ['user_id' => $current_user['user_id'] ?? null, 'follow_id' => $user['user_id']]);

$blogposts = database_get_all(database_select("
  SELECT news_id, news_date, article_type, username, user_id, edit_date, edit_user_id
  FROM news
  WHERE user_id = ? AND article_type = 2
  ORDER BY news_id DESC
", 's', [$user['user_id']]));

foreach ($blogposts as &$post) {
  $post['headline'] = $a->RetrieveHeadline($post['news_id']);
  $post['text'] = $a->RetrieveText($post['news_id']);

  if ($post['edit_user_id']) {
    $post['editor_username'] = database_single_value("SELECT username from users WHERE user_id = ?", 's', [$post['edit_user_id']]);
  }

  $comments = database_get_all(database_select("
    SELECT
      users.*,
      news_comments.*,
      'news' AS commentable_type,
      news_comments.news_id AS commentable_id,
      news_comments.news_comments_id AS comment_id
    FROM news_comments
    JOIN users USING(user_id)
    WHERE news_comments.news_id = ?
    ORDER BY news_comments.comment_date ASC
  ", 's', [$post['news_id']]));

  $post['comments'] = tree_sort($comments, 'comment_id', 'reply_id');
}
unset($post);


render_component_template('user-stats/blog', [
  'blog_name' => $a->BlogName($user['user_id']),
  'iamme' => $iamme,
  'following' => $following,
  'posts' => $blogposts,
]);
