<?php
$sort = $_GET['sort'] ?? "percentage";

$order = 'percentage DESC, scoreboard_pos ASC, cyberstars DESC';
switch ($sort) {
case 'position':        $order = 'scoreboard_pos ASC, percentage DESC'; break;
case 'num_subs':        $order = 'num_subs DESC'; break;
case 'num_approved':	  $order = 'num_approved DESC, num_approved_v DESC, num_subs DESC'; break;
case 'num_approved_v':	$order = 'num_approved_v DESC, num_approved DESC, num_subs DESC'; break;
case 'cyberstars':      $order = 'cyberstars DESC'; break;
case 'percentage':      $order = 'percentage DESC, scoreboard_pos ASC, cyberstars DESC'; break;
default:                $order = 'percentage DESC, scoreboard_pos ASC, cyberstars DESC'; break;
}

$user_games = database_get_all(database_select("
SELECT
    games.*,
    games.collectible_charts as eligible_charts,
    scoreboard_pos, num_subs, cyberstars, percentage, num_approved, num_approved_v
  FROM gsb_cache_collectible
  LEFT JOIN games USING (game_id)
  WHERE gsb_cache_collectible.user_id = ?
  ORDER BY $order
", 's', [$user['user_id']]));

$t->CacheGameNames();
foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$cache = database_find_by('sb_cache_collectible', ['user_id' => $user['user_id']]);

$bars = [];
$values = [];
foreach ($user_games as &$game) {
  $bars []= [
    ['class' => 'subs_bar_collectible', 'value' => 100 * $game['num_subs'] / $game['eligible_charts']],
  ];
  $values []= [
    ['width' => 44, 'value' => number_format($game['cyberstars'], 0)],
  ];
}
unset($game);

render_component_template('user-stats/submissions-table', [
  'user' => $user,
  'cache' => $cache,
  'user_games' => $user_games,

  'chart_status' => 'collectible',
  'default_color' => 'collectible',
  'scoreboard' => 'collectible',
  'scoreboard_label' => t('general_collectors_cache'),
  'columns' => [
    ['width' => 44, 'icon' => '/images/icon_collectible.png', 'alt' => t('general_cyberstars'), 'sort' => 'cyberstars'],
  ],
  'totals' => [
    number_format($cache['cyberstars'], 0),
  ],
  'total_bars' => [
    ['class' => 'subs_bar_collectible', 'value' => 100],
  ],
  'bars' => $bars,
  'values' => $values,
]);
