<?php
$sort = $_GET['sort'] ?? "percentage";

$order = 'scoreboard_pos ASC, vs_cxp DESC';
switch ($sort) {
case 'position':        $order = 'scoreboard_pos ASC, percentage DESC'; break;
case 'num_subs':        $order = 'num_subs DESC'; break;
case 'num_approved':	  $order = 'num_approved DESC, num_approved_v DESC, num_subs DESC'; break;
case 'num_approved_v':	$order = 'num_approved_v DESC, num_approved DESC, num_subs DESC'; break;
case 'vs_cxp':          $order = 'vs_cxp DESC'; break;
case 'cxp':             $order = 'cxp DESC'; break;
case 'percentage':      $order = 'percentage DESC, scoreboard_pos ASC, vs_cxp DESC'; break;
default:                $order = 'percentage DESC, scoreboard_pos ASC, vs_cxp DESC'; break;
}

$user_games = database_get_all(database_select("
  SELECT
    games.*,
    games.incremental_charts as eligible_charts,
    scoreboard_pos, num_subs, cxp, vs_cxp, num_approved, num_approved_v, percentage
  FROM gsb_cache_incremental
  LEFT JOIN games USING (game_id)
  WHERE gsb_cache_incremental.user_id = ?
  ORDER BY $order
", 's', [$user['user_id']]));

$t->CacheGameNames();
foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$cache = database_find_by('sb_cache_incremental', ['user_id' => $user['user_id']]);

$bars = [];
$values = [];
foreach ($user_games as &$game) {
  $bars []= [
    ['class' => 'subs_bar_incremental', 'value' => 100 * $game['num_subs'] / $game['eligible_charts']],
  ];
  $values []= [
    ['width' => 22, 'value' => number_format($game['vs_cxp'], 0)],
    ['width' => 22, 'value' => number_format($game['cxp'], 0)],
  ];
}
unset($game);

render_component_template('user-stats/submissions-table', [
  'user' => $user,
  'cache' => $cache,
  'user_games' => $user_games,

  'chart_status' => 'incremental',
  'default_color' => 'incremental',
  'scoreboard' => 'incremental',
  'scoreboard_label' => t('general_experience_table'),
  'columns' => [
    ['width' => 22, 'icon' => '/images/icon_incremental.png', 'alt' => t('general_vxp'), 'sort' => 'vs_cxp', 'label' => t('general_vxp')],
    ['width' => 22, 'icon' => '/images/icon_incremental.png', 'alt' => t('general_cxp'), 'sort' => 'cxp', 'label' => t('general_cxp')],
  ],
  'totals' => [
    number_format($cache['vs_cxp'], 0),
    number_format($cache['cxp'], 0),
  ],
  'total_bars' => [
    ['class' => 'subs_bar_incremental', 'value' => 100],
  ],
  'bars' => $bars,
  'values' => $values,
]);
