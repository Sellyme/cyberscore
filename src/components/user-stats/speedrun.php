<?php
$sort = $_GET['sort'] ?? "percentage";

$order = 'percentage DESC';
switch ($sort) {
case 'position':       $order = 'scoreboard_pos ASC, percentage DESC'; break;
case 'num_subs':       $order = 'num_subs DESC'; break;
case 'medal_points':   $order = 'medal_points DESC'; break;
case 'num_approved':   $order = 'num_approved DESC, num_approved_v DESC, num_subs DESC'; break;
case 'num_approved_v': $order = 'num_approved_v DESC, num_approved DESC, num_subs DESC'; break;
case 'percentage':     $order = 'percentage DESC, medal_points DESC'; break;
default:               $order = 'percentage DESC, medal_points DESC'; break;
}

$user_games = database_fetch_all("
  SELECT
    games.*,
    games.speedrun_charts AS eligible_charts,
    num_subs, scoreboard_pos, medal_points, percentage, num_approved, num_approved_v
  FROM gsb_cache_speedrun
  LEFT JOIN games USING (game_id)
  WHERE user_id = ?
  ORDER BY $order
", [$user['user_id']]);

$t->CacheGameNames();
foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$cache = database_find_by('sb_cache_speedrun', ['user_id' => $user['user_id']]);

$bars = [];
$values = [];
foreach ($user_games as &$game) {
  $bars []= [
    ['class' => 'subs_bar_speedrun', 'value' => 100 * $game['num_subs'] / $game['eligible_charts']],
  ];
  $values []= [
    ['width' => 44, 'value' => number_format($game['medal_points'], 0)],
  ];
}
unset($game);

render_component_template('user-stats/submissions-table', [
  'user' => $user,
  'cache' => $cache,
  'user_games' => $user_games,

  'chart_status' => 'speedrun',
  'default_color' => 'speedrun',
  'scoreboard' => 'speedrun',
  'scoreboard_label' => t('general_awards_table'),
  'columns' => [
    ['width' => 44, 'icon' => '/images/icon_speedrun.png', 'alt' => t('general_speedrun_points'), 'sort' => 'medal_points'],
  ],
  'totals' => [
    number_format($cache['speedrun_points'], 0) . " - " . $cache['speedrun_time'],
  ],
  'total_bars' => [
    ['class' => 'subs_bar_speedrun', 'value' => 100],
  ],
  'bars' => $bars,
  'values' => $values,
]);
