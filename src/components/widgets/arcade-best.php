<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'arcade', 'top');

render_component_template('widgets/submissions', [
  'id' => 'arcade-best',
  'title' => t('widget_title_arcade_best'),
  'scoreboard' => 'arcade',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
