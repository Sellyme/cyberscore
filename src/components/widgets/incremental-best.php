<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'incremental', 'top');

render_component_template('widgets/submissions', [
  'id' => 'incremental-best',
  'title' => t('widget_title_incremental_best'),
  'scoreboard' => 'incremental',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
