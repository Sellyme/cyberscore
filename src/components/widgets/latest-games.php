<?php
// Hardcoded game id check in WHERE clause is for proper display and handling of the date of game publication (feature implemented on October 15 2018)
// No games with an older game ID than 2402 will show up on the latest games applet (useful if we manually give older published games accurate publication dates in the database)
// Hardcoded game IDs in the OR clauses are used to exclude older, legitimate games in the workshop that may be published in the future (and thus receive a date), from the above parameter
$potentially_published = implode(',', [
  1735,
  1784,
  1785,
  1814,
  1822,
  1923,
  1924,
  2052,
  2102,
  2171,
  2346
]);

$entries = database_get_all(database_select("
  SELECT
    game_id,
    date_published
  FROM games
  WHERE games.site_id != 4 AND (games.game_id >= 2402 OR games.game_id IN ($potentially_published))
  ORDER BY
    games.date_published DESC,
    games.game_id DESC
  LIMIT 7
", '', []));

render_component_template('widgets/latest-games', [
  'entries' => $entries,
]);
