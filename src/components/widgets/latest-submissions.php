<?php
//Game ID 2000 is the development test game
$latest_subs = database_get_all(database_select("
  SELECT
    records.*,
    users.username,
    users.country_code,
    users.country_id,
    users.user_groups,
    levels.*,
    POW((records.csp) / 100,4) AS csr,
    chart_modifiers2.*
  FROM records
  JOIN users USING(user_id)
  LEFT JOIN levels USING(level_id)
  LEFT JOIN chart_modifiers2 USING(level_id)
  WHERE records.game_id != 2000
  ORDER BY records.last_update DESC
  LIMIT 5
", '', []));

foreach ($latest_subs as &$latest) {
  $latest['awards'] = chart_awards(...Modifiers::ChartModifiers($latest));
  $latest['colour'] = Modifiers::ChartFlagColouration($latest);
}
unset($latest);

render_component_template('widgets/latest-submissions', [
  'entries' => $latest_subs,
  'widget_user' => $widget_user,
]);
