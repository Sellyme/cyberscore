<?php
$scoreboard_entries = database_fetch_all("
  SELECT
    users.username,
    users.user_id,
    sb_cache.scoreboard_pos,
    sb_cache.total_csr + sb_cache.bonus_csr AS csr
  FROM sb_cache
  JOIN users USING(user_id)
  WHERE sb_cache.scoreboard_pos >= COALESCE(
    (SELECT GREATEST(1, scoreboard_pos - 3) FROM sb_cache WHERE user_id = ?),
    1
  )
  ORDER BY sb_cache.scoreboard_pos ASC
  LIMIT 7
", [$widget_user['user_id'] ?? null]);

render_component_template('widgets/main-scoreboard', [
  'scoreboard_entries' => $scoreboard_entries,
  'widget_user' => $widget_user,
]);
