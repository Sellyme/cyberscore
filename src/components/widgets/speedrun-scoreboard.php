<?php

$entries = database_fetch_all("
  SELECT
    users.user_id,
    users.username,
    sb_cache_speedrun.speedrun_points,
    sb_cache_speedrun.speedrun_time,
    sb_cache_speedrun.scoreboard_pos
  FROM sb_cache_speedrun
  JOIN users USING(user_id)
  WHERE sb_cache_speedrun.scoreboard_pos >= COALESCE(
    (SELECT GREATEST(1, scoreboard_pos - 3) FROM sb_cache_speedrun WHERE user_id = ?),
    1
  )
  ORDER BY
    sb_cache_speedrun.scoreboard_pos ASC,
    sb_cache_speedrun.speedrun_time DESC,
    sb_cache_speedrun.speedrun_points DESC
  LIMIT 7
", [$widget_user['user_id'] ?? null]);

render_component_template('widgets/speedrun-scoreboard', [
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
