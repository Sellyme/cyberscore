<!-- Note: Needs proper php stuff, right now it's just a static page. -Nyhilo -->
debug 3
<div class="tabbed-scoreboard-widget">
  <div id="scoreboard-tabs">
    <ul>
      <li><a href="#scoreboard-1"><i class="fa-solid fa-star"></i></a></li>
      <li><a href="#scoreboard-2"><i class="fa-solid fa-medal"></i></a></li>
      <li><a href="#scoreboard-3"><i class="fa-solid fa-gamepad"></i></a></li>
      <li><a href="#scoreboard-4"><i class="fa-solid fa-person-running"></i></a></li>
      <li><a href="#scoreboard-5"><i class="fa-solid fa-question"></i></a></li>
      <li><a href="#scoreboard-6"><i class="fa-solid fa-x"></i></a></li>
    </ul>
    <div id="scoreboard-1">
      <div class="scoreboard-container">
        <div class="scoreboard-aside">
          <i class="fa-solid fa-star"></i>
        </div>
        <div class="scoreboard-content">
          <div class="scoreboard-header"></div>
          <div class="scoreboard-body">
              Stars
          </div>
        </div>
      </div>
    </div>
    <div id="scoreboard-2">
      <div class="scoreboard-container">
        <div class="scoreboard-aside">
          <i class="fa-solid fa-medal"></i>
        </div>
        <div class="scoreboard-content">
          <div class="scoreboard-header"></div>
          <div class="scoreboard-body">
              Medals
          </div>
        </div>
      </div>
    </div>
    <div id="scoreboard-3">
      <div class="scoreboard-container">
        <div class="scoreboard-aside">
          <i class="fa-solid fa-gamepad"></i>
        </div>
        <div class="scoreboard-content">
          <div class="scoreboard-header"></div>
          <div class="scoreboard-body">
              Arcade
          </div>
        </div>
      </div>
    </div>
    <div id="scoreboard-4">
      <div class="scoreboard-container">
        <div class="scoreboard-aside">
          <i class="fa-solid fa-person-running"></i>
        </div>
        <div class="scoreboard-content">
          <div class="scoreboard-header"></div>
          <div class="scoreboard-body">
              Running
          </div>
        </div>
      </div>
    </div>
    <div id="scoreboard-5">
      <div class="scoreboard-container">
        <div class="scoreboard-aside">
          <i class="fa-solid fa-question"></i>
        </div>
        <div class="scoreboard-content">
          <div class="scoreboard-header"></div>
          <div class="scoreboard-body">
              Question
          </div>
        </div>
      </div>
    </div>
    <div id="scoreboard-6">
      <div class="scoreboard-container">
        <div class="scoreboard-aside">
          <i class="fa-solid fa-x"></i>
        </div>
        <div class="scoreboard-content">
          <div class="scoreboard-header"></div>
          <div class="scoreboard-body">
              Swords
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $("#scoreboard-tabs").tabs({
    active: 0
  });
</script>
