<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'all', 'top');

render_component_template('widgets/submissions', [
  'id' => 'ucsp-best',
  'title' => t('widget_title_ucsp_best'),
  'scoreboard' => 'starboard',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
