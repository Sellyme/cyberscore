<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'all', 'bottom');

render_component_template('widgets/submissions', [
  'id' => 'ucsp-worst',
  'title' => t('widget_title_ucsp_worst'),
  'scoreboard' => 'starboard',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
