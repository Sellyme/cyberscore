<?php

use Ramsey\Uuid\Uuid;

// Users can create their own dashboards. These are composed of widgets.
// This class also contains a bunch of helpers used in those widgets.

class Dashboard {
  public static $available_widgets = [
    'ucsp-best',
    'speedrun-best',
    'challenge-best',
    'standard-best',
    'arcade-best',
    'collectible-best',
    'solution-best',
    'incremental-best',
    'ucsp-worst',
    'favourite-games',
    'standard-scoreboard',
    'speedrun-scoreboard',
    'solution-scoreboard',
    'arcade-scoreboard',
    'challenge-scoreboard',
    'collectible-scoreboard',
    'incremental-scoreboard',
    'main-scoreboard'
  ];

  public static $default = [
    'widgets' => [
      ['widget' => 'ucsp-best', 'params' => []],
      ['widget' => 'speedrun-best', 'params' => []],
      ['widget' => 'challenge-best', 'params' => []],
      ['widget' => 'standard-best', 'params' => []],
      ['widget' => 'arcade-best', 'params' => []],
      ['widget' => 'collectible-best', 'params' => []],
      ['widget' => 'ucsp-worst', 'params' => []],
      ['widget' => 'solution-best', 'params' => []],
      ['widget' => 'incremental-best', 'params' => []],
    ],
  ];

  public static function Find($user) {
    $dashboard = database_find_by('user_dashboards', ['user_id' => $user['user_id']]);

    if ($dashboard) {
      $dashboard["config"] = json_decode($dashboard["config"], true);
    } else {
      $dashboard = [
        'user' => $user,
        'user_id' => $user['user_id'],
        'config' => self::$default,
      ];
    }

    // add a transient identifier to each widget
    foreach ($dashboard["config"]["widgets"] as &$widget) {
      $widget['uuid'] = Uuid::uuid4()->toString();
    }
    unset($widget);


    return $dashboard;
  }

  // Helper functions to some of our widgets
  public static function Submissions($user_id, $chart_type, $top_or_bottom) {
    if ($top_or_bottom == 'top') { $direction = 'DESC'; } else { $direction = 'ASC'; }

    if ($chart_type == 'all') {
      $filter = "";
    } else {
      $filter = "AND $chart_type";
    }

    return database_fetch_all("
      SELECT
        records.game_id,
        records.level_id,
        levels.group_id,
        levels.level_name,
        games.game_name,
        level_groups.group_name,
        records.ucsp
      FROM records
      JOIN levels USING (level_id)
      JOIN chart_modifiers2 USING (level_id)
      LEFT JOIN level_groups USING(group_id)
      JOIN games ON records.game_id = games.game_id
      WHERE records.user_id = ? $filter
      ORDER BY ucsp {$direction} LIMIT 10
    ", [$user_id]);
  }
}
