<?php

class RankbuttonGenerator {
  static $tables = [
    'challenge' => 'sb_cache_challenge',
    'proof' => 'sb_cache_proof',
    'vproof' => 'sb_cache_video_proof',
    'trophy' => 'sb_cache_trophy',
    'medal' => 'sb_cache_standard',
    'speedrun' => 'sb_cache_speedrun',
    'arcade' => 'sb_cache_arcade',
    'starboard' => 'sb_cache',
    'solution' => 'sb_cache_solution',
    'collectible' => 'sb_cache_collectible',
    'incremental' => 'sb_cache_incremental',
    'subs' => 'sb_cache_total_subs',
    'rainbow' => 'sb_cache_rainbow'
  ];

  public static function global_rankbutton($type, $user_id) {
    $table = self::$tables[$type] ?? null;
    if (!$table) {
      return null;
    }

    $rank = database_single_value("SELECT scoreboard_pos FROM $table WHERE user_id = ?", 's', [$user_id]);
    if (!$rank) {
      return null;
    }

    $im = imagecreatefromgif(self::global_template_path($type));
    imagepalettetotruecolor($im);
    $black = imagecolorallocatealpha($im, 0, 0, 0, 0);
    imagestring($im, 1, round(71 - imagefontwidth(1) * strlen($rank) / 2, 0), 17, $rank, $black);
    return $im;
  }

  public static function game_rankbutton($game_id, $user_id) {
    global $config;

    $rank = database_value("
      SELECT scoreboard_pos
      FROM gsb_cache_csp JOIN games USING (game_id)
      WHERE game_id = ? AND user_id = ? AND site_id = 1
    ", [$game_id, $user_id]);

    if (!$rank) {
      return null;
    }

    $im = imagecreatefromgif(self::game_template_path($game_id));
    imagepalettetotruecolor($im);
    $black = imagecolorallocatealpha($im, 0, 0, 0, 0);
    imagestring($im, 1, round(71 - imagefontwidth(1) * strlen($rank) / 2, 0), 17, $rank, $black);
    return $im;
  }

  public static function global_template_path($type) {
    global $config;
    $root = $config['app']['root'];

    return "$root/public/images/rankbuttons/$type.gif";
  }

  public static function game_template_path($game_id, $type = "game") {
    global $config;
    $root = $config['app']['root'];

    $rankbutton = RankbuttonsRepository::find_by(['game_id' => $game_id, 'active' => true]);
    if ($rankbutton) {
      $sha256 = $rankbutton['sha256'];
      return "$root/public/uploads/rankbuttons/games/$sha256.gif";
    } else{
      return "$root/public/images/rankbuttons/$type.gif";
    }
  }

  public static function game_template_url($game_id, $type = 'game') {
    global $config;
    $root = $config['app']['root'];

    $path = self::game_template_path($game_id, $type);
    return str_replace("$root/public", "", $path);
  }

  public static function global_template_url($type) {
    global $config;
    $root = $config['app']['root'];

    $path = self::global_template_path($type);
    return str_replace("$root/public", "", $path);
  }
}
