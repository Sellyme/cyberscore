<?php

Authorization::authorize('Guest');

render_with('accounts/new', [
  'page_title' => t('register_title'),
  'params' => [],
]);
