<?php

Authorization::authorize('GlobalMod');

$users = database_get_all(database_select("
  SELECT
    users.user_id,
    users.username,
    users.mod_notes,
    users.mod_notes_date,
    moderator.user_id as moderator_user_id,
    moderator.username as moderator_username
  FROM users
  LEFT JOIN users moderator ON users.mod_notes_id = moderator.user_id
  WHERE users.user_status = 0
  ORDER BY users.username ASC
", '', []));

render_with('admin/banned_users/index', [
  'page_title' => 'Banned users',
  'users' => $users,
]);
