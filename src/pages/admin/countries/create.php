<?php

Authorization::authorize('Admin');

$params = filter_post_params([
  Params::str('country_code'),
  Params::str('country_name'),
  Params::str('continent'),
]);

if (database_find_by('countries', ['country_code' => $params['country_code']])) {
  $cs->WriteNote(false, "Country code already in use");
  $cs->RedirectToPreviousPage();
}

database_insert('countries', [
  'country_code' => $params['country_code'],
  'country_name' => $params['country_name'],
  'continent' => $params['continent'],
]);

$cs->WriteNote(true, "Country added");
$cs->RedirectToPreviousPage();
