<?php

Authorization::authorize('Admin');

$country = database_find_by('countries', ['country_id' => $_GET['id']]);

$params = filter_post_params([
  Params::str('new_country_name'),
]);

if (!$country) {
  $cs->PageNotFound();
}

if ($params['new_country_name']) {
  $new_country = database_find_by('countries', ['country_name' => $_POST['new_country_name']]);

  if (!$new_country) {
    $cs->LeavePage('/admin/countries', "Can't find the relocation country");
  }

  database_update_by('users', [
    'country_id' => $new_country['country_id'],
    'country_code' => $new_country['country_code'],
  ], [
    'country_id' => $country['country_id'],
  ]);
}

database_delete_by('countries', ['country_id' => $country['country_id']]);
database_delete_by('translation_db', ['table_field' => 'country_name', 'table_id' => $country['country_id']]);

// TODO: remove "/flags/CC.png" if it exists

$cs->WriteNote(true, 'Country deleted');
$cs->RedirectToPreviousPage();
