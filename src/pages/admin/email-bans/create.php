<?php

Authorization::authorize('Dev');

$params = filter_post_params([
  Params::str('domain'),
]);

EmailBansRepository::create($params);
$cs->WriteNote(true, "Created email ban");

redirect_to_back();
