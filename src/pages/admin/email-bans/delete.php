<?php

Authorization::authorize('Dev');

$email_ban = EmailBansRepository::get($_GET['id']);

if (!$email_ban) {
  $cs->PageNotFound();
}

EmailBansRepository::delete($email_ban['email_ban_id']);

$cs->WriteNote(true, "The email ban has been deleted.");
redirect_to_back();
