<?php

Authorization::authorize('Guest');

$chart = database_find_by('levels', ['level_id' => $_GET['id']]);

if (!$chart) {
  http_response_code(404);
  render_json(['error' => 'Chart not found']);
  exit;
}

$t->CacheGame($chart['game_id']);
$t->CacheEntityNames($chart['game_id']);
$t->CacheCountryNames();

$game_name = $t->GetGameName($chart['game_id']);
$group_name = $t->GetGroupName($chart['group_id']);
$chart_name = $t->GetChartName($chart['level_id']);

$scoreboard = database_get_all(database_select("
  SELECT record_id, chart_pos, submission, linked_proof, username, user_id, rec_status
  FROM records
  JOIN users USING (user_id)
  WHERE level_id = ?
  ORDER BY chart_pos
", 's', [$chart['level_id']]));

foreach ($scoreboard as &$entry) {
  $proof_details = ProofManager::ProofDetails($entry);
  $entry['record_url'] = url_for(['record' => $entry]);
  $entry['proof_url'] = $proof_details['url'] ?? NULL;
  $entry['verified'] = $entry['rec_status'] == 3;
  $entry['user_url'] = url_for(['user' => $entry]);
  $entry['user_avatar'] = $config['app']['base_url'] . $cs->GetUserPic($entry);

  unset($entry['linked_proof']);
  unset($entry['rec_status']);
}

render_json([
  'game_id' => $chart['game_id'],
  'chart_id' => $chart['level_id'],
  'game_name' => $game_name,
  'group_name' => $group_name,
  'chart_name' => $chart_name,
  'scoreboard' => $scoreboard,
]);
