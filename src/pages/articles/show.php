<?php
$news = database_find_by('news', ['news_id' => intval($_GET['id'])]);

if ($news == null) {
  $cs->PageNotFound();
}

if ($news['article_type'] == 3) Authorization::authorize('Admin');
elseif($news['article_type'] == 4) Authorization::authorize('GlobalMod');
elseif($news['article_type'] == 6) Authorization::authorize('GameMod');
elseif($news['article_type'] == 7) Authorization::authorize('ProofMod');
elseif($news['article_type'] == 8) Authorization::authorize('Dev');
elseif($news['article_type'] == 9) Authorization::authorize('Designer');
elseif($news['article_type'] == 10) Authorization::authorize('Translator');
elseif($news['article_type'] == 11) Authorization::authorize('Newswriter');

$editor = database_find_by('users', ['user_id' => $news['edit_user_id']]);

$comments = database_get_all(database_select("
  SELECT
    users.*,
    news_comments.*,
    news_comments.news_comments_id AS comment_id,
    'news' AS commentable_type,
    news_comments.news_id AS commentable_id
  FROM news_comments JOIN users USING(user_id)
  WHERE news_comments.news_id = ?
  ORDER BY news_comments.comment_date ASC
", 'i', [$news['news_id']]));

$comments = tree_sort($comments, 'news_comments_id', 'reply_id');

$followed_check = database_find_by('user_blog_follows', ['user_id' => $current_user['user_id'], 'follow_id' => $news['user_id']]);

render_with('articles/show', [
  'page_title' => $t->GetNewsHeadline($news['news_id']),
  'news' => $news,
  'comments' => $comments,
  'followed_check' => $followed_check,
  'blog_name' => $a->BlogName($news['user_id']),
  'headline' => $a->RetrieveHeadline($news['news_id']),
]);
