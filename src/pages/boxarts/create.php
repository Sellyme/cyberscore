<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_POST['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

$game_platform_ids = pluck(database_get_all(database_select("SELECT platform_id FROM game_platforms WHERE game_id = ?", 's', [$game['game_id']])), 'platform_id');

$no_errors = true;
foreach ($_POST['platform_ids'] as $i => $platform_id) {
  if (in_array($platform_id, $game_platform_ids)) {
    if (is_uploaded_file($_FILES['boxarts']['tmp_name'][$i] ?? NULL)) {

      $image = imagecreatefrompng($_FILES['boxarts']['tmp_name'][$i]) ?: imagecreatefromjpeg($_FILES['boxarts']['tmp_name'][$i]);
      if ($image) {
        $sha256 = hash('sha256', file_get_contents($_FILES['boxarts']['tmp_name'][$i]));

        $root = $config['app']['root'];
        move_uploaded_file($_FILES['boxarts']['tmp_name'][$i], "$root/public/uploads/boxarts/$sha256");

        database_insert('boxarts', [
          'game_id' => $game['game_id'],
          'platform_id' => $platform_id,
          'remote' => false,
          'sha256' => $sha256,
          'created_at' => database_now(),
        ]);
      } else {
        $no_errors = false;
        $cs->WriteNote(false, "Boxart is not a PNG or a JPEG");
      }
    }
  }
}

if ($no_errors) {
  $cs->WriteNote(true, 'Boxarts uploaded');
}
redirect_to_back();
