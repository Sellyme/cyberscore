<?php

Authorization::authorize('GameMod');

$boxart = database_find_by('boxarts', ['boxart_id' => $_GET['id']]);

if (!$boxart) {
  $cs->PageNotFound();
}

database_delete_by('boxarts', ['boxart_id' => $boxart['boxart_id']]);

$cs->WriteNote(true, "Boxart deleted");
redirect_to_back();
