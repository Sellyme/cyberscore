<?php

$chart = database_find_by('levels', ['level_id' => $_GET['id']]);

// Hardcoded date in below query is intentional
$submissions = database_get_all(database_select("
  SELECT
    record_history.history_id,
    record_history.user_id,
    record_history.submission,
    record_history.submission2,
    record_history.update_date,
    record_history.update_type,
    record_history.comment,
    users.username
  FROM record_history
  JOIN records ON record_history.record_id = records.record_id
  JOIN users ON record_history.user_id = users.user_id
  WHERE records.level_id = ? AND record_history.update_date != '2018-06-10 00:00:00'
", 's', [$chart['level_id']]));

//Conditionally wrangle the conversation factor only if it is necessary.
$cf = $chart['chart_type2'] != 0 ? $chart['conversion_factor'] : FALSE;

render_json([
  'game_id' => $chart['game_id'],
  'chart_id' => $chart['level_id'],
  'chart_type' => $chart['chart_type'],
  'chart_type2' => $chart['chart_type2'],
  'conversion_factor' => $cf,
  'submissions' => $submissions,
]);
