<?php

$chart_ids = [
  1841,   // single standard     m:ss.d, with entities
  367128, // dual   speedrun     m:ss.d
  43308,  // dual   standard     small scores
  406328, // single solution     big suffix
  734428, // dual   incremental  big labels, suffixes and scores
  740428, // dual   incremental  big scores
  670546, // dual   incremental  big suffix
  643484, // single collectible  arcade
];

$charts = [];
foreach ($chart_ids as $chart_id) {
  $chart = database_fetch_one("
    SELECT levels.*, chart_modifiers2.*
    FROM levels
    JOIN chart_modifiers2 USING (level_id)
    WHERE level_id = ?
  ", [$chart_id]);

  if ($chart['name_pri'] == '') {
    $chart['name_pri'] = $chart['chart_type'] >= 3 ? 'Score' : 'Time';
  }

  $rank = Modifiers::ChartFlagColouration($chart);
  $awards = chart_awards(...Modifiers::ChartModifiers($chart));

  $entries = $cs->GetChart($chart['level_id']);

  foreach ($entries as &$entry) {
    $entry['csr'] = round(pow($entry['csp'] / 100, 4), 6);
    $entry['ugh_submission'] = [
      'submission1'  => $cs->GetConvertedSubmission($entry['submission'],  $entry['submission2'], $chart['conversion_factor'], 1, $chart['num_decimals']),
      'submission2'  => $cs->GetConvertedSubmission($entry['submission2'], $entry['submission'],  $chart['conversion_factor'], 2, $chart['num_decimals2']),

      'num_decimals' => $chart['num_decimals'],
      'chart_type'   => $chart['chart_type'],
      'score_prefix' => $chart['score_prefix'],
      'score_suffix' => $chart['score_suffix'],

      'num_decimals2' => $chart['num_decimals2'],
      'chart_type2'   => $chart['chart_type2'],
      'score_prefix2' => $chart['score_prefix2'],
      'score_suffix2' => $chart['score_suffix2'],
    ];

    $entry['ugh_submission']['distance1'] = $cs->SubmissionDistance($entry['ugh_submission']['submission1'], $entries[0]['ugh_submission']['submission1'], $chart['chart_type']);
    $entry['ugh_submission']['distance2'] = $cs->SubmissionDistance($entry['ugh_submission']['submission2'], $entries[0]['ugh_submission']['submission2'], $chart['chart_type2']);
  }
  unset($entry);

  $charts []= [
    'chart' => $chart,
    'rank' => $rank,
    'awards' => $awards,
    'entries' => $entries,
  ];
}

// Translation caching
$t->CacheCountryNames();
foreach ($chart_ids as $chart_id) {
  $t->CacheGame($chart['game_id']);
  $t->CacheEntityNames($chart['game_id']);
}

render_with('charts/index48', [
  'page_title' => 'Test page',
  'charts' => $charts,
]);
