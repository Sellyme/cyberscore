<?php

Authorization::authorize('Guest');

$chart = database_fetch_one("
  SELECT levels.*, chart_modifiers2.*
  FROM levels
  JOIN chart_modifiers2 USING (level_id)
  WHERE level_id = ?
", [$_GET['id']]);

if (!$chart) {
  $cs->PageNotFound('The requested chart does not exist.');
}

$game = database_find_by("games", ['game_id' => $chart['game_id']]);
$game['platforms'] = database_fetch_all("
  SELECT platforms.*
  FROM game_platforms
  LEFT JOIN platforms USING(platform_id)
  WHERE game_platforms.game_id = ? AND original = 1
", [$game['game_id']]);


$chart_id = $chart['level_id'];

$chart_group = database_find_by('level_groups', ['group_id' => $chart['group_id']]);
$chart_sorting = database_single_value("SELECT chart_sort FROM user_prefs WHERE user_id = ?", 's', [$user_id]);

[$total, $modifiers, $set] = Modifiers::ChartModifiers($chart);
$rank = Modifiers::ChartFlagColouration($chart);
$awards = chart_awards(...Modifiers::ChartModifiers($chart));

switch ($chart_sorting) {
case 0:
  $sort = "records.chart_pos ASC, records.rec_status = 3 AND records.proof_type = 'v' DESC, records.rec_status IN (3, 4) DESC, records.last_update ASC";
  break;
case 1:
  $sort = "records.last_update ASC, records.rec_status = 3 AND records.proof_type = 'v' DESC, records.rec_status IN (3, 4) DESC, records.chart_pos ASC";
  break;
case 2:
  $sort = "records.last_update DESC, records.rec_status = 3 AND records.proof_type = 'v' DESC, records.rec_status IN (3, 4) DESC, records.chart_pos ASC";
  break;
}

database_select("SET SESSION group_concat_max_len = 10000", '', []);

list($game_id, $group_id, $chart_info, $entities, $platforms) = $cs->GetChartInfo($chart['level_id']);

$t->CacheGame($game_id);
$t->CacheEntityNames($game_id);
$t->CacheCountryNames();

$game_name = $t->GetGameName($game_id);
$group_name = $t->GetGroupName($group_id);
$chart_name = $t->GetChartName($chart['level_id']);
$description = [
  'output' => t('chart_notice_no_recs'),
  'count' => 0,
];


//count number of records
$num_records = database_single_value("SELECT COUNT(*) FROM records WHERE level_id = ?", 's', [$chart['level_id']]);

if (($_GET['format'] ?? NULL) != 'json') {
  $user_friends = $cs->GetUserFriends($user_id);
  $get_chart = $cs->GetChart($chart['level_id'], $sort);
  $rules = $t->GetRuleText($chart_info['level_rules']);
  $proof_rules = $t->GetProofRuleText($chart_info['level_proof_rules']);

  $game_groups = database_get_all(database_select("
    SELECT levels.group_id, GROUP_CONCAT(levels.level_id ORDER BY levels.level_pos ASC SEPARATOR ',') AS chart_ids
    FROM levels LEFT JOIN level_groups USING(group_id)
    WHERE levels.game_id = ?
    GROUP BY levels.group_id
    ORDER BY levels.group_id != 0 DESC, level_groups.group_pos ASC, level_groups.group_name ASC
  ", 'i', [$game_id]));
  foreach ($game_groups as &$group) {
    $group['name'] = $t->GetGroupName($group['group_id']);
    $group['charts'] = [];
    foreach (explode(',', $group['chart_ids']) as $chart_id) {
      $group['charts'] []= [
        'chart_id' => $chart_id,
        'name' => $t->GetChartName($chart_id),
      ];
    }
  }
  unset($group);

  $scoreboards = [];

  if ($awards['csp'])             $scoreboards []= ['starboard',   'The Mainboard',                    'Game scoreboard'];
  if ($awards['medals'])          $scoreboards []= ['medal',       'Global medals scoreboard',         'Game medals scoreboard'];
  if ($awards['speedrun_points']) $scoreboards []= ['speedrun',    'Global speedrun scoreboard',       'Game speedrun scoreboard'];
  if ($awards['arcade_points'])   $scoreboards []= ['arcade',      'Global arcade scoreboard',         'Game arcade scoreboard'];
  if ($awards['brain_power'])     $scoreboards []= ['solution',    'Global solution scoreboard',       'Game solution scoreboard'];
  if ($awards['style_points'])    $scoreboards []= ['challenge',   'Global user challenge scoreboard', "Game user challenge scoreboard"];
  if ($awards['cyberstars'])      $scoreboards []= ['collectible', "Global collector's cache",         "Game collector's cache"];
  if ($awards['cxp'])             $scoreboards []= ['incremental', 'Global experience table',          "Game experience table"];

  // TODO: use this
  // extra_image_id | game_id | extra_name | extra_value | file
  $extra_images = database_get_all(database_select("
    SELECT * FROM extra_images WHERE game_id = ?
  ", 's', [$game_id]));

  foreach ($get_chart as &$record) {
    $record['fully_complete'] = in_array($game_id, ['968', '816', '969']) && $record['comment'] == "FC";
    $record['csr'] = csp2csr($record['csp']);
    $record['extras'] = [];
    if ($chart_info['name1'] != '' && $record['extra1'] !== NULL) {
      $record['extras'] []= ['name' => $chart_info['name1'], 'value' => $record['extra1']];
    }
    if ($chart_info['name2'] != '' && $record['extra2'] !== NULL) {
      $record['extras'] []= ['name' => $chart_info['name2'], 'value' => $record['extra2']];
    }
    if ($chart_info['name3'] != '' && $record['extra3'] !== NULL) {
      $record['extras'] []= ['name' => $chart_info['name3'], 'value' => $record['extra3']];
    }

    $record['entities'] = [];
    if ($chart_info['max_entities'] > 0) {
      // range(1, 0) returns [1,0] instead of [], so we need the preceding conditional.
      foreach (range(1, $chart_info['max_entities']) as $i) {
        $record['entities'] []= Entity::info($game_id, $record["entity_id$i"]);
      }
    }
    $record['entity_names'] = pluck(array_compact($record['entities']), 'name');

    $record['ugh'] = [
      'submission1' => $cs->GetConvertedSubmission($record['submission'],  $record['submission2'], $chart_info['conversion_factor'], 1, $chart_info['num_decimals']),
      'submission2' => $cs->GetConvertedSubmission($record['submission2'], $record['submission'],  $chart_info['conversion_factor'], 2, $chart_info['num_decimals2']),
      'num_decimals' => $chart_info['num_decimals'],
      'chart_type' => $chart_info['chart_type'],
      'score_prefix' => $chart_info['score_prefix'],
      'score_suffix' => $chart_info['score_suffix'],

      'num_decimals2' => $chart_info['num_decimals2'],
      'chart_type2' => $chart_info['chart_type2'],
      'score_prefix2' => $chart_info['score_prefix2'],
      'score_suffix2' => $chart_info['score_suffix2'],
    ];

    $record['ugh']['formatted_submission1'] = $cs->FormatSubmission($record['ugh']['submission1'], $chart_info['chart_type'],  $chart_info['num_decimals'],  $game_id, $chart_info['decimals']);
    $record['ugh']['formatted_submission2'] = $cs->FormatSubmission($record['ugh']['submission2'], $chart_info['chart_type2'], $chart_info['num_decimals2'], $game_id, $chart_info['decimals']);

    $record['ugh']['distance1'] = $cs->SubmissionDistance($record['ugh']['submission1'], $get_chart[0]['ugh']['submission1'], $chart_info['chart_type']);
    $record['ugh']['distance2'] = $cs->SubmissionDistance($record['ugh']['submission2'], $get_chart[0]['ugh']['submission2'], $chart_info['chart_type2']);
    // ($converted_submission > $leader_score ? '+' : '-') . $cs->FormatSubmission(abs($leader_score - $converted_submission), $chart_info['chart_type'], $chart_info['num_decimals'])

    // This is to declare description metadata for OpenGraph protocol
    if ($record['chart_pos'] == 1) {
      $description['username'] = $record['username'];
      $description['score'] = $chart_info['score_prefix'] . $record['ugh']['formatted_submission1'] . $chart_info['score_suffix'];
      if (isset($record['ugh']['formatted_submission2'])) $description['score'] = $chart_info['score_prefix'] . $record['ugh']['formatted_submission1'] . $chart_info['score_suffix'] . " / " . $chart_info['score_prefix2'] . $record['ugh']['formatted_submission2'] . $chart_info['score_suffix2'];

      if ($description['count'] > 0) {
        $description['count']++;
        $description['output'] = "The 1st place submission on this chart is " . $description['score'] . ", held by " . $description['count'] . " players.";
      } else {
        $description['count'] = 1;
        $description['output'] = "The 1st place submission on this chart is " . $description['score'] . ", held by " . $description['username'] . " since " . $record['last_update'] .".";
      }
    }
  }
  unset($record);

  $prev_chart_id = database_single_value("SELECT level_id FROM levels WHERE game_id = ? AND group_id = ? AND level_pos = ?", 'iii', [$game_id, $group_id, $chart['level_pos'] - 1]);
  $next_chart_id = database_single_value("SELECT level_id FROM levels WHERE game_id = ? AND group_id = ? AND level_pos = ?", 'iii', [$game_id, $group_id, $chart['level_pos'] + 1]);

  $body_of_work_type = null;
  foreach ($modifiers as &$modifier) {
    $modifier['label'] = Modifiers::csp_modifier_label($modifier['identifier']);
    $modifier['formatted_value'] = Modifiers::format_csp_modifier($modifier['value']);
    if ($modifier['label'] == "Full-game" || $modifier['label'] == "Part-game") {
      $body_of_work_type = $modifier['label'];
    }
  }
  unset($modifier);

  render_with("charts/show", [
    'chart' => $chart,
    'game' => $game,
    'group_id' => $group_id,
    'game_groups' => $game_groups,
    'chart_name' => $chart_name,
    'num_records' => $num_records,
    'chart_info' => $chart_info,
    'get_chart' => $get_chart,
    'chart_sorting' => $chart_sorting,
    'rank' => $rank,
    'next_chart_id' => $next_chart_id,
    'prev_chart_id' => $prev_chart_id,
    'awards' => $awards,
    'user_friends' => $user_friends,
    'rules' => $rules,
    'proof_rules' => $proof_rules,
    'scoreboards' => $scoreboards,
    'platforms' => $platforms,
    'entities' => $entities,
    'modifiers' => [
      'total' => $total,
      'list' => $modifiers,
      'set' => $set,
      'body_of_work_type' => $body_of_work_type,
    ],
    'metadata' => [
      'description' => $description['output'],
      'title' => $group_id > 0 ? $game_name . ' → ' . $group_name . ' → ' . $chart_name : $game_name . ' → ' . $chart_name,
    ],
  ]);
} else {
  $scoreboard = database_get_all(database_select("
    SELECT record_id, chart_pos, submission, linked_proof, username, user_id, rec_status
    FROM records
    JOIN users USING (user_id)
    WHERE level_id = ?
    ORDER BY chart_pos
  ", 's', [$chart['level_id']]));

  foreach ($scoreboard as &$entry) {
    $proof_details = ProofManager::ProofDetails($entry);
    $entry['record_url'] = url_for(['record' => $entry]);
    $entry['proof_url'] = $proof_details['url'] ?? NULL;
    $entry['verified'] = $entry['rec_status'] == 3;
    $entry['user_url'] = url_for(['user' => $entry]);
    $entry['user_avatar'] = $config['app']['base_url'] . $cs->GetUserPic($entry);

    unset($entry['linked_proof']);
    unset($entry['rec_status']);
  }

  render_json([
    'game_id' => $chart['game_id'],
    'chart_id' => $chart['level_id'],
    'game_name' => $game_name,
    'group_name' => $group_name,
    'chart_name' => $chart_name,
    'scoreboard' => $scoreboard,
  ]);
}
