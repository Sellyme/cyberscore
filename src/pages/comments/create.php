<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::str('comment'),
  Params::number('comment_id'),
  Params::str('commentable_type'),
  Params::number('commentable_id'),
]);

if (empty($params['comment'])) {
  $cs->WriteNote(false, $t['general_comments_empty']);
} else {
  $commentable = Commentable::get($params['commentable_type'], $params['commentable_id']);

  if ($params['comment_id'] ?? null) {
    $comment = Comment::find_by(
      $params['commentable_type'],
      ['comment_id' => $params['comment_id'], 'commentable_id' => $params['commentable_id']]
    );
  }

  $a->AddComment(
    $params['comment'],
    $commentable['commentable_type'],
    $commentable['commentable_id'],
    $comment['comment_id'] ?? NULL,
    $user_id
  );

  $cs->WriteNote(true, $t['general_comments_submitted']);
}

$cs->RedirectToPreviousPage();
