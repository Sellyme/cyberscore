<?php

$params = filter_post_params([
  Params::str('comment'),
  Params::number('comment_id'),
  Params::str('commentable_type'),
]);

$comment = Comment::get($params['commentable_type'], $params['comment_id']);

Authorization::authorize('GlobalMod', $comment['user_id']);

if (empty($params['comment'])) {
  $cs->WriteNote(false, $t['general_comments_empty']);
} else {
  Comment::update(
    $params['commentable_type'],
    $params['comment_id'],
    ['comment' => $params['comment']]
  );
  $cs->WriteNote(true, $t['general_comments_edit']);
}

$cs->RedirectToPreviousPage();
