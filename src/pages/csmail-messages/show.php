<?php

Authorization::authorize('User');

$csmail_id = intval($_GET['id'] ?? 0);

$message = database_get(database_select("
  SELECT *, from_user.username AS from_username, to_user.username AS to_username
  FROM csmail
  LEFT JOIN users from_user ON from_user.user_id = csmail.from_id
  LEFT JOIN users to_user ON to_user.user_id = to_id
  WHERE csmail_id = ?
", 's', [$csmail_id]));

if (!$message) {
  $cs->LeavePage('/csmails', $t['csmail_error_faulty_id']);
}

if ($message['from_id'] != $current_user['user_id'] && $message['to_id'] != $current_user['user_id']) {
  $cs->LeavePage('/csmails', $t['csmail_error_not_yours']);
}

if (($message['from_id'] == $current_user['user_id'] && $message['from_deleted'] == 1) || ($message['to_id'] == $current_user['user_id'] && $message['to_deleted'] == 1)) {
  $cs->LeavePage('/csmails', $t['csmail_error_deleted_mail']);
}

// Mark it as read
if ($message['to_status'] == 0 && $message['to_id'] == $current_user['user_id']) {
  database_update_by('csmail', ['to_status' => 1], ['csmail_id' => $csmail_id]);
}

// It's ok to read it so lets check status and update
switch ($message['to_status']) {
case 0:
  if ($message['to_id'] == $user_id) {
    $print_status = $t['csmail_status_read'];
  } else {
    //PERSON WHO SENT IT IS LOOKING AT IT, SHOW THEM IT HASN’T BEEN READ
    $print_status = $t['csmail_status_unread'];
  }
  break;
case 1:
  $print_status = $t['csmail_status_read'];
  break;
case 2:
  $print_status = $t['csmail_status_replied'];
  break;
case 3:
  $print_status = $t['csmail_status_forwarded'];
  break;
case 4:
  $print_status = $t['csmail_status_sent'];
  break;
case 5:
  $print_status = $t['csmail_status_replied'] . '<br /><span style="visibility:hidden"><strong>' . $t['csmail_status'] . '</strong></span> '. $t['csmail_status_forwarded'];
  break;
}

render_with("csmail-messages/show", [
  'page_title' => t('csmail_read_title'),
  'message' => $message,
  'print_status' => $print_status,
]);
