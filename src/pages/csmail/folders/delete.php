<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::number('id'),
]);

$folder = database_find_by('csmail_folders', [
  'csmail_folder_id' => $params['id'],
  'user_id' => $current_user['user_id']
]);

if ($folder == NULL) {
  $cs->LeavePage('/csmails', h("Folder does not exist or does not belong to you"));
}

//empty the box and then remove it
database_update_by('csmail', ['csmail_folder_id' => 0], ['csmail_folder_id' => $folder['csmail_folder_id']]);
database_delete_by('csmail_folders', ['csmail_folder_id' => $folder['csmail_folder_id']]);

$cs->RedirectToPreviousPage();
