<?php

$game_id = $_GET['game_id'];
$widget_id = $_GET['widget_id'];

$game = database_find_by('games', ['game_id' => $game_id]);

$game['game_name'] = $t->GetGameName($game['game_id']);
$game['boxart'] = $cs->GetBoxArts($game_id)[0];

display_partial('dashboard-widgets/game', [
  'game' => $game,
  'widget' => [
    'uuid' => $widget_id,
  ],
]);
