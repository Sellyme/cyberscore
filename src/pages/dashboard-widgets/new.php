<?php

use Ramsey\Uuid\Uuid;

$available_widgets = [
  'ucsp-best',
  'speedrun-best',
  'challenge-best',
  'standard-best',
  'arcade-best',
  'collectible-best',
  'solution-best',
  'incremental-best',
  'ucsp-worst',
  'favourite-games',
  'incremental-scoreboard',
];

$widget = $_GET['widget_id'];

display_partial('dashboard-widgets/new', [
  'widget' => [
    'widget' => $widget,
    'uuid' => Uuid::uuid4()->toString(),
  ],
]);
