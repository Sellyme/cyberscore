<?php

Authorization::authorize('User');

$dashboard = Dashboard::Find($current_user);

render_with('dashboard/edit', [
  'page_title' => "Editing your dashboard",
  'dashboard' => $dashboard,
  'available_widgets' => Dashboard::$available_widgets,
]);
