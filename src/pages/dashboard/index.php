<?php

Authorization::authorize('User');

$widget_user_id = intval($_GET['id'] ?? $current_user['user_id']);

$user = UsersRepository::get($widget_user_id);

$dashboard = Dashboard::Find($user);

// hydrate widgets for rendering
foreach ($dashboard["config"]["widgets"] as &$widget) {
  $widget['params']['widget_user_id'] ??= $user['user_id'];
  $widget['params']['widget_user'] ??= $user;
}
unset($widget);

render_with('dashboard/index', [
  'page_title' => $user['username'] . "'s Dashboard",
  'user' => $user,
  'dashboard' => $dashboard,
  'can_edit' => $widget_user_id == $current_user['user_id'],
]);
