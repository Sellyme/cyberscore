<?php

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::number('game_id'),
  Params::str('text'),
  Params::str('link'),
]);

DiscordServersRepository::create($params);
$cs->WriteNote(true, "Added discord server");

$cs->RedirectToPreviousPage();
