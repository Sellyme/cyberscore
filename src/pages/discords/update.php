<?php

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::number('server_id'),
  Params::str('text'),
  Params::str('link'),
]);

DiscordServersRepository::update($params['server_id'], $params);
$cs->WriteNote(true, "Edited discord server");

$cs->RedirectToPreviousPage();
