<?php

Authorization::authorize('User');

$other = UsersRepository::get($_POST['user_id']);

if (!$other || $other['user_id'] == $current_user['user_id']) {
  $cs->WriteNote(false, "You must follow someone other than yourself");
  $cs->RedirectToPreviousPage();
}

$params = ['user_id' => $current_user['user_id'], 'follow_id' => $other['user_id']];

if (!database_find_by('user_blog_follows', $params)) {
  database_insert('user_blog_follows', $params);
  Notification::BlogFollowed($other['user_id'], $current_user['user_id'])->DeliverToTarget();
}

$cs->WriteNote(true, "Followed {$other['username']}");
$cs->RedirectToPreviousPage();
