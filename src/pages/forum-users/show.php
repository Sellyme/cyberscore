<?php

$user = database_find_by('smf_accounts', ['smf_member_id' => $_GET['id']]);

if (!$user) {
  $cs->PageNotFound();
}

redirect_to("/users/{$user['user_id']}");
