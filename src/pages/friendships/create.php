<?php

Authorization::authorize('User');

$other = UsersRepository::get($_POST['user_id']);

if (!$other || $other['user_id'] == $current_user['user_id']) {
  $cs->WriteNote(false, "You must befriend someone other than yourself");
  $cs->RedirectToPreviousPage();
}

$params = ['user_id' => $current_user['user_id'], 'friend_id' => $other['user_id']];

if (!database_find_by('user_friends', $params)) {
  database_insert('user_friends', $params);
}

$cs->WriteNote(true, "Befriended {$other['username']}");
$cs->RedirectToPreviousPage();
