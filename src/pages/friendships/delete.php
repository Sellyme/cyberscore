<?php

Authorization::authorize('User');

$other = UsersRepository::get($_GET['id']);

if (!$other || $other['user_id'] == $current_user['user_id']) {
  $cs->WriteNote(false, "You must unfriend someone other than yourself");
  $cs->RedirectToPreviousPage();
}

$params = ['user_id' => $current_user['user_id'], 'friend_id' => $other['user_id']];
database_delete_by('user_friends', $params);

$cs->WriteNote(true, "Unfriended {$other['username']}");
$cs->RedirectToPreviousPage();
