<?php

Authorization::authorize('User');

$game = GamesRepository::get($_GET['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

$page_title = t('gameboard_create_topic_game', ['game' => $t->GetGameName($game['game_id'])]);

render_with('game-board-topics/new', [
  'page_title' => $page_title,
  'game' => $game,
]);
