<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->LeavePage('/games', 'The requested game does not exist.');
}

$chart_infos = database_get_all(database_select("
  SELECT
    COUNT(level_id) AS num_levels,
    MIN(level_id) AS level_id,
    players, dlc,
    chart_type, num_decimals, score_prefix, score_suffix,
    chart_type2, num_decimals2, score_prefix2, score_suffix2,
    conversion_factor, name_pri, name_sec, name1, name2, name3,
    SUM(levels.decimals = 0) AS d0,
    SUM(levels.decimals = 1) AS d1,
    SUM(levels.decimals = 2) AS d2,
    SUM(levels.decimals = 3) AS d3
  FROM levels WHERE game_id = ?
  GROUP BY
    players, dlc,
    chart_type, num_decimals, score_prefix, score_suffix,
    chart_type2, num_decimals2, score_prefix2, score_suffix2,
    conversion_factor, name_pri, name_sec, name1, name2, name3
  ORDER BY num_levels DESC, players ASC, dlc ASC, chart_type ASC, chart_type2 ASC
", 's', [$game['game_id']]));

foreach ($chart_infos as &$chart_info) {
  $chart_info['groups'] = database_get_all(database_select("
    SELECT
      IF(levels.group_id = 0, 'Separate charts', level_groups.group_name) AS group_name,
      SUM(IF(
        levels.players = ? AND levels.dlc = ? AND
        levels.chart_type = ? AND levels.num_decimals = ? AND
        levels.score_prefix = ? AND levels.score_suffix = ? AND
        levels.chart_type2 = ? AND levels.num_decimals2 = ? AND
        levels.score_prefix2 = ? AND levels.score_suffix2 = ? AND
        levels.conversion_factor = ? AND levels.name_pri = ? AND levels.name_sec = ? AND
        levels.name1 = ? AND levels.name2 = ? AND levels.name3 = ?,
      1, 0)) AS num_charts,
      COUNT(*) AS total_charts
    FROM levels
    LEFT JOIN level_groups USING(group_id)
    WHERE levels.game_id = ?
    GROUP BY levels.group_id
    HAVING num_charts > 0
    ORDER BY level_groups.group_pos ASC
  ", 'sssssssssssssssss', [
      $chart_info['players'], $chart_info['dlc'],
      $chart_info['chart_type'], $chart_info['num_decimals'],
      $chart_info['score_prefix'], $chart_info['score_suffix'],
      $chart_info['chart_type2'], $chart_info['num_decimals2'],
      $chart_info['score_prefix2'], $chart_info['score_suffix2'],
      $chart_info['conversion_factor'], $chart_info['name_pri'], $chart_info['name_sec'],
      $chart_info['name1'], $chart_info['name2'], $chart_info['name3'],
      $game['game_id']
  ]));
}
unset($chart_info);

if (isset($_GET['edit'])) {
  $chart = LevelsRepository::get($_GET['edit']);

  if (!$chart || $chart['game_id'] != $game['game_id']) {
    $cs->PageNotFound();
  }

  $chart_ids = pluck(database_get_all(database_select("
    SELECT level_id
    FROM levels
    WHERE game_id = ? AND
    players = ? AND dlc = ? AND
    chart_type = ? AND num_decimals = ? AND score_prefix = ? AND score_suffix = ? AND
    chart_type2 = ? AND num_decimals2 = ? AND score_prefix2 = ? AND score_suffix2 = ? AND
    conversion_factor = ? AND name_pri = ? AND name_sec = ? AND
    name1 = ? AND name2 = ? AND name3 = ?
  ", 'sssssssssssssssss', [
      $chart['game_id'],
      $chart['players'], $chart['dlc'],
      $chart['chart_type'], $chart['num_decimals'],
      $chart['score_prefix'], $chart['score_suffix'],
      $chart['chart_type2'], $chart['num_decimals2'],
      $chart['score_prefix2'], $chart['score_suffix2'],
      $chart['conversion_factor'], $chart['name_pri'], $chart['name_sec'],
      $chart['name1'], $chart['name2'], $chart['name3'],
  ])), 'level_id');
} else {
  $chart_ids = [];

  $chart = [
    'players' => 1,
    'dlc' => 0,
    'chart_type' => 1,
    'num_decimals' => -1,
    'score_prefix' => '',
    'score_suffix' => '',
    'chart_type2' => 0,
    'num_decimals2' => -1,
    'score_prefix2' => '',
    'score_suffix2' => '',
    'conversion_factor' => 0,
    'name_pri' => '',
    'name_sec' => '',
    'name1' => '',
    'name2' => '',
    'name3' => '',
  ];
}

$sample_chart = $chart;

$game_groups = database_get_all(database_select("
  SELECT group_id, group_name FROM level_groups
  WHERE game_id = ? ORDER BY group_pos ASC
", 's', [$game['game_id']]));

foreach ($game_groups as &$group) {
  $group['charts'] = database_get_all(database_select("
    SELECT level_id, level_name
    FROM levels
    WHERE group_id = ?
    ORDER BY level_pos ASC
  ", 's', [$group['group_id']]));
}
unset($group);

$other_charts = database_get_all(database_select("
  SELECT level_id, level_name
  FROM levels
  WHERE game_id = ? AND group_id = 0
  ORDER BY level_pos ASC
", 's', [$game['game_id']]));

render_with('game-chart-details/show', [
  'page_title' => 'Edit chart details',
  'game' => $game,
  'chart_infos' => $chart_infos,
  'other_charts' => $other_charts,
  'game_groups' => $game_groups,
  'chart_ids' => $chart_ids,
  'chart_types' => array_map(fn($i) => $t->ReadChartType($i), range(0, 5)),
  'sample_chart' => $sample_chart,
]);
