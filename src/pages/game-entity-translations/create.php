<?php

$params = filter_post_params([
  Params::str('translation'),
  Params::number('lang_id'),
  Params::number('game_id'),
]);

$game = GamesRepository::get($params['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

$name_mappings = array_map(
  fn($line) => array_map(fn($p) => trim($p), explode("->", trim($line))),
  explode("\n", $params['translation'])
);

foreach ($name_mappings as [$from, $to]) {
  $entity_id = database_single_value("
    SELECT game_entity_id
    FROM game_entities
    WHERE game_id = ? AND entity_name = ?
  ", 'ss', [$params['game_id'], $from]);

  if ($entity_id == null) {
    $cs->WriteNote(false, "An entity with the name '" . h($from) . "' was not found");
  } else {
    $t->ChangeEntityName($params['lang_id'], $entity_id, $to, $params['game_id']);
  }
}

$cs->WriteNote(true, $t->GetChanges());
redirect_to_back();
