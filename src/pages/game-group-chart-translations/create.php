<?php

// TODO: automatic "Separate charts" must go.
// We need to explicitly create a group_id for them.
if (str_starts_with($_POST['group_id'], 's')) {
  $game = GamesRepository::get(substr($_POST['group_id'], 1));
  $group = ['group_id' => 0, 'game_id' => $game['game_id']];
} else {
  $group = GroupsRepository::get($_POST['group_id']);
  $game = GamesRepository::get($group['game_id']);
}

if (!$group) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('translation'),
  Params::enum('mode', ['group', 'game']),
  Params::number('language_id'),
]);

$name_mappings = array_map(
  fn($line) => array_map(fn($p) => trim($p), explode("->", trim($line))),
  explode("\n", $params['translation'])
);

if ($params['mode'] == 'game') {
  $pkey = ['game_id' => $group['game_id']];
} else {
  $pkey = ['group_id' => $group['group_id'], 'game_id' => $group['game_id']];
}

foreach ($name_mappings as [$from, $to]) {
  $charts = LevelsRepository::where(array_merge($pkey, ['level_name' => $from]));

  foreach ($charts as $chart) {
    $t->ChangeChartName($params['language_id'], $chart['level_id'], $to, $group['game_id']);
  }
}

$cs->WriteNote(true, $t->GetChanges());
$cs->RedirectToPreviousPage();
