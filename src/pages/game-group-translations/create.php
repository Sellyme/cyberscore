<?php

$game = GamesRepository::get($_POST['game_id']);
if (!$game) {
  $cs->PageNotFound();
}

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::str('translation'),
  Params::number('lang_id'),
]);

$name_mappings = array_map(
  fn($line) => array_map(fn($p) => trim($p), explode("->", trim($line))),
  explode("\n", $params['translation'])
);

foreach ($name_mappings as [$from, $to]) {
  $groups = GroupsRepository::where(['game_id' => $game['game_id'], 'group_name' => $from]);

  if (empty($groups)) {
    $cs->WriteNote(false, "No group named '" . h($from) . "' found.");
  }

  foreach ($groups as $group) {
    $t->ChangeGroupName($params['lang_id'], $group['group_id'], $to, $game['game_id']);
  }
}

$cs->WriteNote(true, $t->GetChanges());
redirect_to_back();
