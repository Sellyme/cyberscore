<?php

// TODO: game-groups-groups is a stupid name, but game-groups/update was taken.
// I probably need to rename that action first and then rename this one.

$game = GamesRepository::get($_GET['id']);
if (!$game) {
  $cs->PageNotFound();
}

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::array_of_numbers('group_pos'),
  Params::array_of_numbers('group_ranked'),
  Params::array_of_strs('group_names'),
  Params::number('lang_id'),
]);

foreach ($params['group_pos'] as $group_id => $new_pos) {
  database_update_by('level_groups', ['group_pos' => $new_pos], ['group_id' => $group_id]);
}

foreach ($params['group_ranked'] as $group_id => $ranked) {
  database_update_by('level_groups', ['ranked' => $ranked], ['group_id' => $group_id]);
}

foreach ($params['group_names'] as $group_id => $group_name) {
  $t->ChangeGroupName($params['lang_id'], $group_id, $group_name, $game['game_id']);
}

$cs->WriteNote(true, $t->GetChanges());
redirect_to_back();
