<?php

Authorization::authorize(['GameMod', 'ProofMod']);

$game_id = intval($_GET['id']);

if (!$cs->GameExists($game_id)) {
  $cs->LeavePage('/games.php', 'The requested game does not exist.');
}

$results = database_get_all(database_select("
  SELECT CASE level_proof_rules WHEN '0' THEN '' WHEN NULL THEN '' ELSE level_proof_rules END AS rule_ids
  FROM levels
  WHERE game_id = ?
", 's', [$game_id]));

$used_rules = '';
foreach ($results as $row) { $used_rules .= "," . $row['rule_ids']; }
$used_rules = array_map(
  function($id) use($t) { return ['rule_id' => $id, 'text' => $t->GetProofRuleText($id)[0]]; },
  array_filter(array_unique(explode(',', $used_rules)), function($e) {return $e != '';})
);
sort($used_rules);

$game_charts = database_get_all(database_select("
  SELECT
    levels.level_id,
    levels.group_id,
    level_name,
    level_proof_rules,
    group_name,
    CASE level_groups.ranked WHEN 1 THEN 'standard' ELSE 'standard' END as group_ranked
  FROM levels
  LEFT JOIN level_groups USING (group_id)
  WHERE levels.game_id = ?
  ORDER BY level_groups.group_pos, levels.level_pos
", 'i', [$game_id]));

foreach ($game_charts as &$chart) {
  if ($chart['level_proof_rules'] != "") {
    $chart['proof_rules'] = explode(',', $chart['level_proof_rules']);
  } else {
    $chart['proof_rules'] = [];
  }
}
unset($chart);

$game_charts = group_by($game_charts, function($e) { return $e['group_id']; });

render_with('game-proof-rules/show', [
  'page_title' => 'Edit game proof guidelines',
  'game_id' => $game_id,
  'used_rules' => $used_rules,
  'game_charts' => $game_charts,
]);
