<?php

Authorization::authorize('GameMod');

$series = database_find_by('series_info', ['series_id' => $_GET['id']]);
if (!$series) {
  $cs->PageNotFound();
}

database_update_by('series_info', ['series_name' => $_POST['series_name']], ['series_id' => $series['series_id']]);
database_delete_by('series', ['series_id' => $series['series_id']]);
database_insert_all('series',
  array_map(
    fn($id) => ['game_id' => $id, 'series_id' => $series['series_id']],
    $_POST['game_ids']
  )
);

$cs->WriteNote(true, "Series edited");
redirect_to("/game-series?series_id={$series['series_id']}");
