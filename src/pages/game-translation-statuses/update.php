<?php

$params = filter_post_params([
  Params::enum('status', ['incomplete', 'forum_topic', 'complete']),
  Params::number('language_id'),
]);

$game = GamesRepository::get($_GET['id']);
if (!$game) {
  $cs->PageNotFound();
}

database_update_by('translation_games_progress', [
  'status' => $params['status'],
], [
  'game_id' => $game['game_id'],
  'language_id' => $params['language_id'],
]);
redirect_to_back();
