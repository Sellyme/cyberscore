<?php
Authorization::authorize('User');

// Header                            Tip text                                Script input names       Example text                          Required
$fields = [
  [$t['gamerequests_game_name'],     $t['gamerequests_tips_gamename'],       "Game_Name",             $t['gamerequests_example_gamename'],     true],
  [$t['gamerequests_game_exists'],   $t['gamerequests_tips_game_exists'],    "game_exists",           $t['gamerequests_example_game_exist'],   true],
  [$t['gamerequests_platforms'],     $t['gamerequests_tips_platforms'],      "Platforms",             $t['gamerequests_example_platforms'],    true],
  [$t['gamerequests_entities'],      $t['gamerequests_tips_entities'],       "Entities",              $t['gamerequests_example_entities'],     false],
  [$t['gamerequests_rules'],         $t['gamerequests_tips_rules'],          "Rules",                 $t['gamerequests_example_rules'],        false],
  [$t['gamerequests_regiondiff'],    $t['gamerequests_tips_regiondiff'],     "Region_Differences",    $t['gamerequests_example_regiondiff'],   false],
  [$t['gamerequests_platformdiff'],  $t['gamerequests_tips_platformdiff'],   "Platform_Differences",  $t['gamerequests_example_platformdiff'], false],
  [$t['gamerequests_patches'],       $t['gamerequests_tips_patches'],        "Patches",               $t['gamerequests_example_patches'],      false],
  [$t['gamerequests_boxart'],        $t['gamerequests_tips_boxart'],         "Boxarts",               $t['gamerequests_example_boxart'],       false],
  [$t['gamerequests_charts'],        $t['gamerequests_tips_charts'],         "Charts",                $t['gamerequests_example_charts'],       true],
  [$t['gamerequests_extra_info'],    $t['gamerequests_tips_extrainfo'],      "Extra_Information",     $t['gamerequests_example_extrainfo'],    false],
  [$t['gamerequests_chartcount'],    $t['gamerequests_tips_chartcount'],     "Chart_Count",           $t['gamerequests_example_chartcount'],   false],
  [$t['gamerequests_exampleproof'],  $t['gamerequests_tips_exampleproof'],   "Example_Proof",         $t['gamerequests_example_exampleproof'], false],
];

render_with('game_requests/new', [
  'page_title' => "Game Requests",
  'fields' => $fields,
]);
