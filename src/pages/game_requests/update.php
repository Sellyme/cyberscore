<?php

$game_request = database_find_by('game_requests', ['gamereq_id' => $_GET['id']]);

if (!$game_request) {
  $cs->LeavePage('/game_requests', 'Game request not found');
}

$params = filter_post_params([
  Params::enum('handled', ['deleted', 'complete', 'denied']),
  Params::number('game_id'),
  Params::str('game_name'),
  Params::str('boxart'),
  Params::str('entities'),
  Params::str('rules'),
  params::str('platforms'),
  Params::str('regiondiff'),
  Params::str('platformdiff'),
  Params::str('patches'),
  Params::str('extra_info'),
  Params::str('charts'),
  Params::str('chartcount'),
  Params::str('exampleproof'),
]);

Authorization::authorize('GameMod', $game_request['submitter_id']);
if ($params['handled'] !== NULL && !Authorization::has_access('GameMod')) {
  $cs->LeavePage('/game_requests', "You do not have permission do that.");
}

if ($params['handled'] == 'deleted') {
  database_delete_by('game_requests', ['gamereq_id' => $game_request['gamereq_id']]);
  $cs->WriteNote(true, $t['gamerequests_deleted']);
  $cs->LeavePage('/game_requests');
}

switch ($params['handled']) {
case 'denied':
  $params['handled'] = 2;
  $cs->WriteNote(true, $t['gamerequests_denied']);
  break;
case 'complete':
  $params['handled'] = 1;
  $cs->WriteNote(true, $t['gamerequests_complete']);
  break;
default:
  $params['handled'] = 0;
  $cs->WriteNote(true, $t['gamerequests_updated']);
}

database_update_by('game_requests', $params, ['gamereq_id' => $game_request['gamereq_id']]);

redirect_to('/game_requests');
