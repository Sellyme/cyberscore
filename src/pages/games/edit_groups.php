<?php

Authorization::authorize(['GameMod', 'ProofMod']);

$game_id = intval($_GET['game_id']);
$language_id = $_GET['lang_id'] ?? 1;

if (!$cs->GameExists($game_id)) {
  $cs->LeavePage('/games.php', 'The requested game does not exist.');
}

$groups = database_get_all(database_select("
  SELECT
    level_groups.group_id,
    level_groups.group_name,
    level_groups.group_pos,
    COUNT(levels.level_id) AS num_levels,
    level_groups.ranked
  FROM level_groups
  LEFT JOIN levels USING(group_id)
  WHERE level_groups.game_id = ?
  GROUP BY level_groups.group_id
  ORDER BY level_groups.group_pos ASC, level_groups.group_name ASC
", 's', [$game_id]));

foreach ($groups as &$group) {
  $group['translated_name'] = $t->RetrieveGroupName($language_id, $group['group_id']);
}
unset($group);

$groupless_charts = database_single_value("
  SELECT COUNT(level_id)
  FROM levels
  WHERE game_id = ? AND group_id = 0
", 's', [$game_id]);

$translation_goal = (
  1 +
  database_single_value("SELECT COUNT(*) FROM level_groups WHERE game_id = ?", 's', [$game_id]) +
  database_single_value("SELECT COUNT(*) FROM levels WHERE game_id = ?", 's', [$game_id])
);

$num_entities = database_single_value("SELECT COUNT(*) FROM game_entities WHERE game_id = ?", 's', [$game_id]);

$translation_status = database_get_all(database_select("
  SELECT
    MIN(translation_games.game_id) AS game_id,
    languages.language_id,
    languages.language_name,
    MIN(translation_games_progress.status) AS status,
    SUM(IF(translation_games.table_field = 'game_name', 1, 0)) AS num_game_names,
    SUM(IF(translation_games.table_field = 'group_name', 1, 0)) AS num_group_names,
    SUM(IF(translation_games.table_field = 'chart_name', 1, 0)) AS num_chart_names,
    SUM(IF(translation_games.table_field = 'entity_name', 1, 0)) AS num_entity_names
  FROM translation_games
  LEFT JOIN translation_games_progress USING(game_id, language_id)
  JOIN languages USING(language_id)
  WHERE translation_games.game_id = ?
  GROUP BY translation_games.language_id
  ORDER BY languages.language_name ASC
", 's', [$game_id]));

$languages = database_get_all(database_select("
  SELECT language_id, language_name FROM languages ORDER BY language_name ASC
", '', []));

$game_entities = database_single_value("SELECT COUNT(*) FROM game_entities WHERE game_id = ?", 's', [$game_id]);

render_with('games/edit_groups', [
  'page_title' => 'Edit Game',
  'languages' => $languages,
  'game_id' => $game_id,
  'language_id' => $language_id,
  'game_entities' => $game_entities,
  'game' => [
    'game_id' => $game_id,
    'game_name' => $t->RetrieveGameName($language_id, $game_id),
    'lang_id' => $language_id,
  ],
  'groups' => $groups,
  'groupless_charts' => $groupless_charts,
  'translation_status' => $translation_status,
  'translation_goal' => $translation_goal,
  'num_entities' => $num_entities,
]);
