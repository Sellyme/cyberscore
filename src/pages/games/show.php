<?php

Authorization::authorize('Guest');

$game_id = intval($_GET['id']);

$game = database_find_by("games", ['game_id' => $game_id]);
if (!$game) {
  $cs->PageNotFound('The requested game does not exist.');
}

$game['platforms'] = database_fetch_all("
  SELECT platforms.*
  FROM game_platforms
  LEFT JOIN platforms USING(platform_id)
  WHERE game_platforms.game_id = ? AND original = 1
", [$game['game_id']]);

global $t;
$t->CacheGame($game_id);

$game_groups = database_get_all(database_select("
  SELECT
    levels.game_id,
    levels.group_id,
    SUM(case levels.dlc when 0 then 1 else 0 end) as num_non_dlc_levels,
    COUNT(levels.level_id) AS num_levels,
    SUM(levels.num_subs) AS total_subs,
    level_groups.ranked,
    level_groups.group_name
  FROM levels
  LEFT JOIN level_groups USING(group_id)
  WHERE levels.game_id = ?
  GROUP BY levels.game_id, levels.group_id
  ORDER BY levels.group_id != 0 DESC, level_groups.group_pos ASC
", 'i', [$game_id]));

foreach ($game_groups as &$group) {
  $group['colour'] = Modifiers::GroupColouration($group['ranked']);
  $group['name'] = $t->GetGroupName($group['group_id']);
}
unset($group);

$group_levels = database_get_all(database_select("
  SELECT
    levels.*,
    MIN(records.record_id) AS record_id,
    MIN(records.chart_pos) AS chart_pos,
    MIN(records.platinum) AS platinum,
    MIN(records.rec_status) AS rec_status,
    MIN(records.submission) AS rec_submission,
    MIN(records.submission2) AS rec_submission2,
    MIN(best.submission) AS best_submission,
    MIN(best.submission2) AS best_submission2,
    chart_modifiers2.*
  FROM levels
  LEFT JOIN records ON records.level_id = levels.level_id AND records.user_id = ?
  LEFT JOIN records best ON best.level_id = levels.level_id AND best.chart_pos = 1
  LEFT JOIN chart_modifiers2 ON chart_modifiers2.level_id = levels.level_id
  WHERE levels.game_id = ?
  GROUP BY levels.level_id
  ORDER BY levels.level_pos ASC, levels.level_name ASC
", 'ii', [$current_user['user_id'] ?? 0, $game_id]));

foreach ($group_levels as &$level) {
  [$total, $modifiers, $set] = Modifiers::ChartModifiers($level);
  $level['chart_id'] = $level['level_id'];
  $level['awards'] = chart_awards($total, $modifiers, $set);
  $level['chart_name'] = $t->GetChartName($level['level_id']);
  $level['colour'] = Modifiers::ChartFlagColouration($level);
}
unset($level);

$group_levels = group_by($group_levels, fn ($e) => $e['group_id']);

$game_name = $t->GetGameName($game_id);

if (($_GET['format'] ?? NULL) != 'json') {
  $submitted_any_records = database_single_value("
    SELECT COUNT(*)
    FROM records JOIN levels USING(level_id)
    WHERE levels.game_id = ? AND records.user_id = ?
  ", 'ii', [$game_id, $current_user['user_id'] ?? 0]) > 0;

  render_with("games/show", [
    'page_title' => $game_name,
    'game_name' => $game_name,
    'game' => $game,
    'submitted_any_records' => $submitted_any_records,
    'game_groups' => $game_groups,
    'group_levels' => $group_levels,
  ]);
} else {
  $groups = [];
  foreach ($game_groups as $group) {
    $charts = [];
    foreach ($group_levels[$group['group_id']] as $level) {
      $charts []= [
        'chart_id' => $level['level_id'],
        'chart_name' => $level['chart_name'],
        'english_chart_name' => $level['level_name'],
        'chart_type' => $level['chart_type'],
        'chart_type2' => $level['chart_type2'],
        'prefix' => $level['score_prefix'],
        'suffix' => $level['score_suffix'],
        'conversion_factor' => $level['conversion_factor'],
        'num_decimals' => $level['num_decimals'],
        'num_decimals2' => $level['num_decimals2'],
        'record' => [
          'record_id' => $level['record_id'],
          'submission' => $level['rec_submission'],
          'submission2' => $level['rec_submission2'],
          'rec_status' => $level['rec_status'],
        ],
        'chart_url' => [
          'html' => url_for(['chart' => $level]),
          'json' => url_for(['chart' => $level]) . ".json",
        ],
      ];
    }
    unset($level);

    $groups []= [
      'group_id' => $group['group_id'],
      'group_name' => $t->GetGroupName($group['group_id']),
      'english_group_name' => $group['group_name'],
      'group_pos' => $group['group_id'],
      'colour' => $group['colour'],
      'charts' => $charts,
      'group_url' => [
        'html' => url_for_group($group),
      ]
    ];
  }

  $boxart = $cs->GetBoxArts($game_id)[0] ?? null;

  render_json([
    'game_id' => $game_id,
    'game_name' => $game_name,
    'auto_proofer_module' => $game['auto_proofer_module'],
    'savefile_module' => $game['savefile_module'],
    'game_boxart' => $boxart['url'] ?? null,
    'chart_groups' => $groups,
    'platforms' => $game['platforms'],
  ]);
}
