<?php

Authorization::authorize('GameMod');

$requests = database_get_all(database_select("
  SELECT *
  FROM game_requests
  WHERE handled != 0
  ORDER BY gamereq_id DESC
", '', []));

render_with('handled-game-requests/index', [
  'page_title' => "Handled game requests",
  'requests' => $requests,
]);
