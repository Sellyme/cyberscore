<?php

Authorization::authorize('GameMod');

$id = $_GET['id'];

$params = filter_post_params([
  Params::number('handled'),
]);

if ($params['handled'] != 0) {
  $cs->LeavePage("/handled-game-requests", "Wrong parameters");
}

database_select("
  UPDATE game_requests
  SET handled = ?
  WHERE gamereq_id = ?
", 'ss', [$params['handled'], $id]);

$cs->WriteNote(true, $t['gamerequests_returned']);
$cs->LeavePage('/game_requests');
