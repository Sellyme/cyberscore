<?php

Authorization::authorize('User');

$small_files = $cs->GetHomepics(true);
$big_files = $cs->GetHomepics(false);

render_with('homepics/index', [
  'page_title' => 'View homepics',
  'small_files' => $small_files,
  'big_files' => $big_files,
]);
