<?php

$impersonator_id = Session::Fetch('impersonator_id');

if ($impersonator_id == null) {
  $cs->Exit();
}

Session::Clear('impersonator_id');
Session::Store('user_id', $impersonator_id);
redirect_to_back();
