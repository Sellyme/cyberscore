<?php

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->PageNotFound();
}

function format_xml($string) {
  return str_replace(array(' &amp; ', ' &ndash; ', '&rsquo;'), array(' & ', ' - ', '’'), html_entity_decode($string));
}

$submissions = database_get_all(database_select("
  SELECT
    users.username,
    levels.*, IFNULL(level_groups.group_name, 'Seperate charts') AS group_name,
    records.*
  FROM records
  JOIN levels USING (level_id)
  LEFT JOIN level_groups USING (group_id)
  JOIN users USING (user_id)
  WHERE levels.game_id = ?
  ORDER BY records.last_update DESC LIMIT 10
", 's', [$game['game_id']]));

$game_name = $game['game_name'];

header('Content-type: text/xml');
?>
<rss version="2.0">
  <channel>
    <title>Cyberscore 4.0 &#x2013; Latest 10 submissions for <?= h($game['game_name']) ?></title>
    <description></description>
    <link>http://www.cyberscore.me.uk</link>
    <copyright>This RSS feed is copyright to Cyberscore.</copyright>
    <?php foreach ($submissions as $submission) { ?>
      <item>
        <title><?= h($submission['username']) ?> set a record of <?= h($cs->FormatSubmissions($submission)) ?> on <?= h($submission['group_name']) ?> &#x02192; <?= h($submission['level_name']) ?>.</title>
        <description>Position: <?= h($submission['chart_pos']) ?>  |  Date: <?= h(date("jS F Y",strtotime($submission['last_update']))) ?></description>
        <link>http://cyberscore.me.uk/chart/<?= h($submission['level_id']) ?></link>
      </item>
    <?php } ?>
  </channel>
</rss>
