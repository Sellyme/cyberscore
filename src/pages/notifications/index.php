<?php
Authorization::authorize('User');

$type = $_GET['type'] ?? null;
$state = $_GET['state'] ?? null;

$available_types = ['community', 'competition', 'proof', 'staff'];
if (!in_array($type, $available_types)) {
  $type = 'any';
}

$available_states = ['unread', 'read', 'any'];
if (!in_array($state, $available_states)) {
  $state = 'unread';
}

function notifications_url($options) {
  global $type, $state;
  $query = array_merge(['type' => $type, 'state' => $state], $options);

  if ($query['type'] == 'any') unset($query['type']);
  if ($query['state'] == 'unread') unset($query['state']);

  $query = http_build_query($query);

  return "/notifications?$query";
}

function notifications_url_current($options) {
  global $type, $state;
  $query = array_merge(['type' => $type, 'state' => $state], $options);

  return $query['type'] == $type && $query['state'] == $state;
}

$current_user_is_staff = Authorization::has_access(['Admin', 'GlobalMod', 'GameMod', 'ProofMod', 'Translator', 'Designer']);

$count_all = database_single_value(
  "SELECT COUNT(*) FROM notifications WHERE user_id = ?",
  'i',
  [$current_user['user_id']]
);

$count_unread = database_single_value(
  "SELECT COUNT(*) FROM notifications WHERE user_id = ? AND note_seen = 'n'",
  'i',
  [$current_user['user_id']]
);

$filters = [
  [
    'title' => t('notifications_filter_state'),
    'options' => [
      ['title' => t('csmail_status_unread')." ($count_unread)", 'options' => ['state' => 'unread']],
      ['title' => t('games_first_letter_all')." ($count_all)", 'options' => ['state' => 'any']],
    ],
  ],
  [
    'title' => t('notifications_filter_category'),
    'options' => [
      ['title' => t('games_first_letter_all'), 'options' => ['type' => 'any']],
      ['title' => t('notifications_community'), 'options' => ['type' => 'community']],
      ['title' => t('notifications_competition'), 'options' => ['type' => 'competition']],
      ['title' => t('notifications_role'), 'options' => ['type' => 'staff'], 'if' => $current_user_is_staff],
      ['title' => t('notifications_proof'), 'options' => ['type' => 'proof']],
    ],
  ],
];

foreach ($filters as &$filter) {
  foreach ($filter['options'] as &$option) {
    $option['selected'] = notifications_url_current($option['options']);
    $option['url'] = notifications_url($option['options']);
    $option['visible'] = ($option['if'] ?? true) !== false;
  }
  unset($option);
}
unset($filter);


$where = ['notifications.user_id' => $current_user['user_id']];

if ($type != 'any') {
  $where['notifications.type'] = NotificationsRepository::$categories[$type];
}

if ($state != 'any') {
  $where['notifications.note_seen'] = $state == 'unread' ? 'n' : 'y';
}

[$where_sql, $where_binds] = database_where_sql_bind($where);

$notifications = database_fetch_all("
  SELECT
    notifications.*,
    senders.username AS sender_username,
    targets.username AS target_username,
    userpage.username AS userpage_username,
    referrals.referrer_id AS referrer_user_id,
    referrals.user_id AS referred_user_id,
    referrer.username AS referrer_username,
    referred.username AS referred_username,
    levels.level_name,
    levels.group_id,
    games.game_name,
    level_groups.group_name,
    game_requests.game_name AS game_request_name,
    news.headline AS news_headline
  FROM notifications
  LEFT JOIN users senders ON notifications.sender_user_id = senders.user_id
  LEFT JOIN users targets ON notifications.target_user_id = targets.user_id
  LEFT JOIN users userpage ON notifications.userpage_id = userpage.user_id
  LEFT JOIN levels ON notifications.chart_id = levels.level_id
  LEFT JOIN level_groups ON levels.group_id = level_groups.group_id
  LEFT JOIN games ON notifications.game_id = games.game_id
  LEFT JOIN game_requests ON notifications.game_request_id = game_requests.gamereq_id
  LEFT JOIN records ON notifications.record_id = records.record_id
  LEFT JOIN support ON notifications.support_id = support.support_id
  LEFT JOIN referrals ON notifications.referral_id = referrals.referral_id
  LEFT JOIN users referrer ON referrals.referrer_id = referrer.user_id
  LEFT JOIN users referred ON referrals.user_id = referred.user_id
  LEFT JOIN news ON notifications.news_id = news.news_id
  WHERE $where_sql
  ORDER BY notifications.notification_id DESC
", $where_binds);

foreach ($notifications as &$notification) {
  $notification['note_time_seconds'] = strtotime($notification['note_time']);
}
unset($notification);

render_with('notifications/index', [
  'page_title' => t('loginbox_notifications'),
  'filters' => $filters,
  'notifications' => $notifications,
  'now' => time(),
]);
