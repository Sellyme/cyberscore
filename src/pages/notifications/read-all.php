<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::array_of_numbers('notification_id'),
]);

$params['user_id'] = $current_user['user_id'];

database_update_by('notifications', ['note_seen' => 'y'], $params);

$cs->RedirectToPreviousPage();
