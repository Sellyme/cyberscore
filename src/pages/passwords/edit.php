<?php

Authorization::authorize('Admin');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

render_with('passwords/edit', [
  'page_title' => 'Password change center',
  'user' => $user,
]);
