<?php

$games = database_get_all(database_select("
  SELECT
    predictions_games.game_id,
    predictions_games.game_name,
    predictions_users.user_id IS NOT NULL AND predictions_users.user_status != 'barred' AS enrolled,
    user_status = 'barred' AS barred
  FROM predictions_games
  LEFT JOIN predictions_users ON (predictions_games.game_id = predictions_users.game_id AND predictions_users.user_id = ?)
  ORDER BY enrolled
", 's', [$current_user['user_id']]));

$enrolled_games = array_filter($games, function($e){return $e['enrolled'];});
$not_enrolled_games = array_filter($games, function($e){return !$e['enrolled'];});

render_with('predictions/index', [
  'page_title' => $t['predictions_hub'],
  'games' => $games,
  'enrolled_games' => $enrolled_games,
  'not_enrolled_games' => $not_enrolled_games,
]);
