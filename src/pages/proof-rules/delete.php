<?php

Authorization::authorize(['GameMod', 'ProofMod']);

$rule_id = intval($_GET['id']);

if (!ProofRulesRepository::exists($rule_id)) {
  $cs->PageNotFound();
}

$result = ProofRulesRepository::delete($rule_id);

$cs->WriteNote(true, "The proof guideline has been deleted.");

header("Location: /proof-rules");
