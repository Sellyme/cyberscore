<?php

Authorization::authorize(['GameMod', 'ProofMod']);

render_with('proof-rules/new', [
  'page_title' => 'Proof Guidelines editor',
]);
