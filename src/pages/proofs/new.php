<?php

$game = GamesRepository::get($_GET['game_id']);
$hide_proven = $_GET['proven'] ?? 0;
$type = $_GET['type'] ?? '';

if (!$game) {
  $cs->PageNotFound();
}

$all_levels = database_get_all(database_select("
  SELECT
    records.ranked,
    records.submission,
    records.submission2,
    records.linked_proof,
    levels.level_id,
    levels.group_id,
    levels.chart_type,
    levels.chart_type2,
    levels.decimals,
    levels.num_decimals,
    levels.score_prefix,
    levels.score_suffix,
    levels.score_prefix2,
    levels.score_suffix2
  FROM records
  JOIN levels USING(level_id)
  WHERE records.user_id = ? AND records.game_id = ? AND records.rec_status <= ?
  ORDER BY levels.group_id, levels.level_pos
", 'sss', [$current_user['user_id'], $game['game_id'], $hide_proven]));

$t->CacheGame($game['game_id']);
$game_name = $t->GetGameName($game['game_id']);

foreach ($all_levels as &$level) {
  $level['group_name'] = $t->GetGroupName($level['group_id']);
}
unset($level);


render_with('proofs/new', [
  'page_title' => t('multiproof_title'),
  'game' => $game,
  'type' => $type,
  'hide_proven' => $hide_proven,
  'game_name' => $game_name,
  'all_levels' => $all_levels,
]);
