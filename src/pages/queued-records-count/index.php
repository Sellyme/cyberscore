<?php

Authorization::authorize('User');

$queued_records = database_single_value("
  SELECT COUNT(1)
  FROM queued_records
  WHERE user_id = ?
", 's', [$current_user['user_id']]);

render_json([
  'queued_records_count' => $queued_records,
]);
