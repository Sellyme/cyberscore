<?php

Authorization::authorize('User');

$proof_files = [];
foreach ($_FILES['files']['name'] as $index => $file) {
  $file = [
    'name' => $_FILES['files']['name'][$index],
    'type' => $_FILES['files']['type'][$index],
    'size' => $_FILES['files']['size'][$index],
    'error' => $_FILES['files']['error'][$index],
    'tmp_name' => $_FILES['files']['tmp_name'][$index],
  ];

  $proof_files []= ProofManager::StoreUploadedProof($file);
}

foreach ($proof_files as $result) {
  if ($result['status'] != 'success') {
    $cs->WriteNote(false, "Error uploading proof file");
    exit;
  }
}

$queued_records = [];
foreach ($_POST['records'] as $params) {
  $chart = LevelsRepository::get($params['chart_id']);

  if ($chart === null) {
    $cs->WriteNote(false, 'Something went wrong: submitting to an unknown chart');
    break;
  }

  if (isset($params['file_index']) && count($proof_files) <= $params['file_index']) {
    $cs->WriteNote(false, 'Something went wrong: probably tried to upload too many files at once');
    break;
  }

  $existing_record = RecordsRepository::find_by([
    'level_id' => $chart_id,
    'user_id' => $current_user['user_id'],
  ]);

  if (RecordManager::IsUnderInvestigation($existing_record)) {
    $cs->LeavePage('/', 'Something went wrong: record under investigation');
  }

  if (RecordManager::IsValidScore($chart, $params)) {
    $queued_records []= [
      'chart_id'    => $chart['level_id'],
      'user_id'     => $current_user['user_id'],
      'entity_id'   => $params['entity_id1'] ?? null,
      'entity_id2'  => $params['entity_id2'] ?? null,
      'entity_id2'  => $params['entity_id3'] ?? null,
      'entity_id2'  => $params['entity_id4'] ?? null,
      'entity_id2'  => $params['entity_id5'] ?? null,
      'entity_id2'  => $params['entity_id6'] ?? null,
      'platform_id' => $params['platform_id'] ?? null,
      'game_patch'  => $params['game_patch'] ?? null,
      'submission'  => $params['submission'],
      'submission2' => $params['submission2'],
      'extra1'      => $params['extra1'] ?? null,
      'extra2'      => $params['extra2'] ?? null,
      'extra3'      => $params['extra3'] ?? null,
      'comment'     => $params['comment'] ?? null,
      'created_at'  => database_now(),
      // TODO: link proof $proof_files[$params['file_index']]['link']
      // ProofManager::LinkProof($record_id, $proof_files[$params['file_index']]['link']);
    ];
  }
}

$new_records = 0;
$updated_records = 0;
$unchanged_records = 0;

if (count($queued_records) > 100) {
  database_insert_all('queued_records', $queued_records);
  $cs->FlashSuccess("queued " . count($queued_records) . " records. These will all be submitted in a few minutes.");
} else {
  foreach ($queued_records as $queued_record) {
    [$result, $change, $record_id] = $r->SubmitQueuedRecord($queued_record);

    if ($result == 'no_entity')           $cs->WriteNote(false, 'No entity was selected');
    else if ($result == 'error_entity')   $cs->WriteNote(false, 'There was an error with the entity');
    else if ($result == 'error_platform') $cs->WriteNote(false, 'There was an error with the platform');
    else {
      if ($change == 'add')            $new_records++;
      else if ($change == 'update')    $updated_records++;
      else if ($change == 'change')    $updated_records++;
      else if ($change == 'no change') $unchanged_recordss++;
    }
  }

  $cs->FlashSuccess("$new_records new records, $updated_records updated records.");
}

render_json($cs->notes);
