<?php
require_once("src/lib/record_moderation_actions.php");

$required_roles = [
  'ip' => 'GlobalMod', // Require image proof
  'iv' => 'GlobalMod', // Require video proof
  'or' => 'GlobalMod', // Reset record status
  'od' => 'GlobalMod', // Delete record
  'ot' => 'GlobalMod', // Termine record
  'ov' => 'GlobalMod', // Revert record
  're' => 'GlobalMod', // Reinstate record
  'ap' => 'ProofMod', // Approve as Image Proof
  'av' => 'ProofMod', // Approve as Video Proof
  'ae' => 'ProofMod', // Approve (Eyewitness) => NO LONGER IN USE.
  'ar' => 'ProofMod', // Approve as Video Proof (Replay File)
  'as' => 'ProofMod', // Approve as Video Proof (Live Stream)
  'rp' => 'ProofMod', // Refuse Proof
  'de' => 'ProofMod', // Delete Proof
  'ls' => 'ProofMod', // Lock Proof
  'le' => 'ProofMod', // Unlock Proof
  'null' => 'User',
];

$actions_taken = [];

foreach ($_POST['actions'] as $record_id => $what) {
  Authorization::authorize($required_roles[$what]);
}

foreach ($_POST['actions'] as $record_id => $what) {

  // We need to make sure we're fetching from the correct repository for the action being taken based on where the record being altered currently is.
  if ($what == "re") { $record = DeletedRecordsRepository::get($record_id); }
  else { $record = RecordsRepository::get($record_id); }

  $rec_status = $record['rec_status'];

  $actions_taken[$what] = ($actions_taken[$what] ?? 0) + 1;
  switch ($what) {
  case "ip":
  case "iv":
    RecordModerationActions::InvestigateRecord($current_user, $record_id, substr($what, 1, 1), $_POST['mail_note'][$record_id] ?? "");
    break;
  case "or":
    RecordModerationActions::ResetRecord($current_user, $record_id);
    break;
  case "od":
    RecordModerationActions::DeleteRecord($current_user, $record_id);
    break;
  case "ot": // Delete the record without backup
    RecordModerationActions::TerminateRecord($current_user, $record_id);
    break;
  case "ov":
    RecordModerationActions::RevertRecord($current_user, $record_id, $_POST['history_ids'][$record_id] ?? 0);
    break;
  case "ap":
  case "av":
  case "ae":
  case "as":
  case "ar":
    RecordModerationActions::ApproveRecord($current_user, $record_id, substr($what, 1, 1));
    break;
  case "re":
    RecordModerationActions::ReinstateRecord($current_user, $record_id);
    break;
  case "rp":
    RecordModerationActions::RefuseProofRecord($current_user, $record_id, $_POST['mail_note'][$record_id] ?? "");
    break;
  case "de":
    RecordModerationActions::DeleteProofRecord($current_user, $record_id);
    break;
  case "ls":
    RecordModerationActions::LockProofRecord($current_user, $record_id);
    break;
  case "le":
    RecordModerationActions::UnlockProofRecord($current_user, $record_id);
    break;
  }
}

foreach ($actions_taken as $key => $count) {
  switch ($key) {
  case 'od': $cs->WriteNote(true, "$count record(s) deleted"); break;
  case 'ap': $cs->WriteNote(true, "$count record(s) approved with picture proof"); break;
  case 'av': $cs->WriteNote(true, "$count record(s) approved with video proof"); break;
  case 'ae': $cs->WriteNote(true, "$count record(s) approved with eyewitness proof"); break;
  case 'as': $cs->WriteNote(true, "$count record(s) approved with livestreaming proof"); break;
  case 'ar': $cs->WriteNote(true, "$count record(s) approved with replay proof"); break;
  case 'or': $cs->WriteNote(true, "$count record(s) had their status reset"); break;
  case 'ov': $cs->WriteNote(true, "$count record(s) have been reverted"); break;
  case 'ot': $cs->WriteNote(true, "$count record(s) have been terminated"); break;
  case 'rp': $cs->WriteNote(true, "$count record(s) had their proofs refused"); break;
  case 're': $cs->WriteNote(true, "$count record(s) were reinstated"); break;
  }
}

// if we're coming from the records page and we just deleted it, go to the chart page instead.
if (isset($_POST['chart_id']) && (($actions_taken['od'] ?? 0) + ($actions_taken['ot'] ?? 0)) > 0) {
  redirect_to("/chart/{$_POST['chart_id']}");
} else {
  redirect_to_back();
}
