<?php

Authorization::authorize('ProofMod');

$record = RecordsRepository::get($_GET['id']);

$deleted_record = database_find_by('records_del', ['record_id' => $_GET['id']]);

if (!$record && !$deleted_record) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('record_note'),
]);

if (isset($params['record_note'])) {
  $params['record_note_date'] = database_now();
  $params['record_note_mod_id'] = $current_user['user_id'];
}

if ($record) {
  database_update_by('records', $params, ['record_id' => $record['record_id']]);
} else {
  database_update_by('records_del', $params, ['record_id' => $deleted_record['record_id']]);
}

$cs->WriteNote(true, "Record note has been added/updated");
$cs->RedirectToPreviousPage();
