<?php
Authorization::authorize('GlobalMod');

// Filter handling. 
// These three are set in separate variables because they are not optional. If no value is passed, we use a default.
$earliest  = date("Y-m-d H:i:s",strtotime($_GET['earliest'] ?? '2012-05-31 11:37:00'));
$latest    = date("Y-m-d H:i:s",strtotime($_GET['latest'] ?? 'Tomorrow'));
$offset    = intval($_GET['offset'] ?? 0);

// These optional filters are handled in an adapatable WHERE clause for the SQL query. 
// Any combination of filters works (including multiple game IDs).
$extra_where = '';
$submitter = $_GET['submitter'] ?? null;
$moderator = $_GET['moderator'] ?? null;
$filter_game = $_GET['game'] ?? null;
if ($submitter) { $extra_where .= " AND submitter.username LIKE '$submitter'"; }
if ($moderator) { $extra_where .= " AND deleter.username LIKE '$moderator'"; }
if ($filter_game) { $extra_where .= " AND records_del.game_id IN ($filter_game)"; }

//This query is separate from the larger data-pull in order to enable pagination
$num_records = database_single_value("
  SELECT COUNT(*)
  FROM records_del
  LEFT JOIN users AS submitter ON records_del.user_id = submitter.user_id
  LEFT JOIN records_del_info ON records_del.record_id = records_del_info.record_id
  LEFT JOIN users AS deleter ON records_del_info.deleter_id = deleter.user_id
  WHERE delete_date BETWEEN ? AND ? $extra_where
", 'ss', [$earliest, $latest]);

//Fetch the actual deleted records that match our filters
$deleted_recs = database_get_all(database_select("
  SELECT
    records_del.*,
    records_del_info.deleter_id,
    records_del_info.delete_date,
    submitter.username AS submitter,
    deleter.username AS deleter,
    reporter.username AS reporter,
    games.game_name
  FROM records_del
  LEFT JOIN games USING (game_id)
  LEFT JOIN users AS submitter ON records_del.user_id = submitter.user_id
  LEFT JOIN users AS reporter ON records_del.rec_reporter = reporter.user_id
  LEFT JOIN records_del_info ON records_del.record_id = records_del_info.record_id
  LEFT JOIN users AS deleter ON records_del_info.deleter_id = deleter.user_id
  WHERE delete_date BETWEEN ? AND ?
  $extra_where
  GROUP BY records_del.record_id
  ORDER by records_del_info.delete_date DESC, submitter.username
  LIMIT 100
  OFFSET $offset
", 'ss', [$earliest, $latest]));

//We want to also fetch information about the moderator who removed the record
foreach ($deleted_recs as &$record) {
  $record['investigator'] = database_get(database_select("
    SELECT users.*
    FROM staff_tasks
    JOIN users ON staff_tasks.user_id = users.user_id
    WHERE task_id = ?
    AND ((tasks_type = 'record_investigated' AND result = 'Record deleted') OR tasks_type = 'record_deleted')
    ORDER BY entry_id DESC
    LIMIT 1
  ", 's', [$record['record_id']]));

  $record['record_note_mod'] = UsersRepository::get($record['record_note_mod_id']);
  
  //TODO: I'm pretty sure the following two queries fetching group data aren't required to be separate queries. 
  //      The issue is that we are querying two different database tables adaptively, neither of which are the primary table being looked at in the initial query
  //      It might be resolvable but this is purely backend optimization and the front-end is fully functional.
  //      I have already managed to minify these queries to get the game information into the primary query.
  //Note: if you manage to optimise these queries, be aware that the information here is stored in a multi-dimensional array and 
  //      is utilised that way in front-end, will need changing as well

//Fetch additional information about the chart that the deleted record used to exist on, in case we need to reference that information or reinstate the record.
  $record['chart'] = database_get(database_select("
    SELECT
      levels.level_name,
      level_groups.group_id, level_groups.group_name
    FROM levels
    LEFT JOIN level_groups ON levels.group_id = level_groups.group_id
    WHERE levels.level_id = ?
  ", 's', [$record['level_id']]));

//It's possible that the record came from a chart that was deleted. Where possible, let's fetch information about that too.
  $record['deleted_chart'] = database_get(database_select("
    SELECT
      levels_del.level_name,
      level_groups.group_id, level_groups.group_name
    FROM levels_del
    LEFT JOIN level_groups ON levels_del.group_id = level_groups.group_id
    WHERE levels_del.level_id = ?
  ", 's', [$record['level_id']]));

}
unset($record);

//Render the page
render_with('records/deleted', [
  'page_title' => 'Deleted records overview',
  'earliest' => $earliest,
  'latest' => $latest,
  'submitter' => $submitter,
  'moderator' => $moderator,
  'game' => $filter_game,
  'offset' => $offset,
  'num_records' => $num_records,
  'deleted_records' => $deleted_recs,
]);
