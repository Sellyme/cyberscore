<?php

Authorization::authorize('User');

$game = GamesRepository::get($_GET['game_id']);

if (!$game) {
  $cs->LeavePage("/", "Game not found");
}

$game_id = $game['game_id'];

$game_groups = database_get_all(database_select("
  SELECT *
  FROM level_groups
  WHERE game_id = ?
  ORDER BY group_pos ASC
", 's', [$game['game_id']]));

foreach ($game_groups as &$group) {
  $group['levels'] = database_get_all(database_select("
    SELECT levels.level_id, levels.level_name, records.record_id
    FROM levels
    LEFT JOIN records ON levels.level_id = records.level_id AND records.user_id = ?
    WHERE levels.group_id = ?
    ORDER BY levels.level_pos ASC, levels.level_name ASC
  ", 'ss', [$current_user['user_id'], $group['group_id']]));
}
unset($group);

$separate_levels = database_get_all(database_select("
  SELECT levels.level_id, levels.level_name, records.record_id
  FROM levels
  LEFT JOIN records ON levels.level_id = records.level_id AND records.user_id = ?
  WHERE levels.game_id = ? AND levels.group_id = 0
  ORDER BY levels.level_pos ASC, levels.level_name ASC
", 'ss', [$current_user['user_id'], $game['game_id']]));

render_with("records/multiple/new", [
  'page_title' => 'Edit Records',
  'game' => $game,
  'game_groups' => $game_groups,
  'separate_levels' => $separate_levels,
]);
