<?php

Authorization::authorize('Guest');

$user = UsersRepository::get($_GET['id'] ?? $current_user['user_id']);
if (!$user) {
  $cs->PageNotFound();
}

$games = database_get_all(database_select("
  SELECT games.game_name, games.game_id
  FROM records
  JOIN games USING(game_id)
  WHERE user_id = ?
  GROUP BY games.game_id
  ORDER BY games.game_name ASC
", 's', [$user['user_id']]));

render_with('records/new', [
  'page_title' => t('loginbox_viewrecords'),
  'user' => $user,
  'games' => $games,
]);
