<?php

Authorization::authorize('User');

function ReportRecord($record_id, $reason, $owner_id, $reporter_id) {
  database_update_by('records', [
    'rec_status' => 1,
    'rec_reporter' => $reporter_id,
    'report_reason' => $reason,
    'proof_deadline' => database_now(),
  ], [
    'record_id' => $record_id,
  ]);
}

$params = filter_post_params([
  Params::number('record_id'),
  Params::str('reason'),
]);

$record = RecordsRepository::get($params['record_id']);
$record_owner_id = $record['user_id'];

// Make sure that the user has reports remaining
if ($current_user['recreport'] == 0) {
  $cs->WriteNote(false, 'Record report failed. You have no remaining reports for today.');
  redirect_to_back();
}

// Make sure the status of the record is 0 (normal status)
if ($current_user['recreport'] > 0) {
  if (empty($params['reason'])) {
    $cs->WriteNote(false, 'Record report failed. Please enter a reason in the field.');
    redirect_to_back();
  } else {
    ReportRecord($record['record_id'], $params['reason'], $record_owner_id, $current_user['user_id']);

    if (Authorization::has_access(['GlobalMod', 'Admin', 'ProofMod', 'GameMod'])) {
      $cs->WriteNote(true, 'Your # of available records has not diminished because you are a moderator');
    } else {
      database_select("UPDATE users SET recreport = recreport - 1 WHERE user_id = ?", 's', [$current_user['user_id']]);

      $current_user['recreport']--;
    }

    Notification::RecordReported($record['record_id'], $current_user['user_id'])->DeliverToGroups(['GlobalMod', 'Admin']);

    $cs->WriteNote(true, 'Record reported successfully');
  }
}

redirect_to_back();
