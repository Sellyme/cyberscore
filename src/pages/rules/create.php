<?php

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::str('rule_text'),
]);

if (RulesRepository::create($params)) {
  $new_id = database_single_value("SELECT rule_id FROM rules ORDER BY rule_id DESC LIMIT 1", '', []);
  $cs->WriteNote(true, "Rule ID #".$new_id." has been added.");
} else {
  $cs->WriteNote(false, "The rule's text matches an existing rule's text.");
}

header("Location: /rules/new");
