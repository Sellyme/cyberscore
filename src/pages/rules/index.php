<?php

Authorization::authorize('GameMod');

$rule_filter = $_GET['rule_filter'] ?? '';

$sample_rules = database_get_all(database_select("
  SELECT rule_id, rule_text
  FROM rules
  WHERE rule_id IN (35, 188, 419, 2400)
  ORDER BY rule_id
", '', []));

$rules = database_get_all(database_select("
  SELECT *
  FROM rules
  WHERE rule_text LIKE ?
  ORDER by rule_id
", 's', ["%$rule_filter%"]));

render_with("rules/index", [
  'page_title' => 'Rule editor',
  'rules' => $rules,
  'sample_rules' => $sample_rules,
  'rule_filter' => $rule_filter,
]);
