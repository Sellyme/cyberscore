<?php

Authorization::authorize('Guest');

$which_board = $_GET['board'] ?? 'country_csr';

switch ($which_board) {
case 'country_medals':
  $scoreboard = $cs->GetCountryMedalTable();
  $text = "Country Medal Table";
  break;
case 'country_csp':
  $scoreboard = $cs->GetCountryCSPBoard();
  $text = "Country Total Scoreboard (CSP)";
  break;
case 'continent_csr':
  $scoreboard = $cs->GetContinentCSRBoard();
  $text = "Continent Mainboard (CSR)";
  break;
case 'continent_medals':
  $scoreboard = $cs->GetContinentMedalTable();
  $text = "Continent Medal Table";
  break;
case 'continent_csp':
  $scoreboard = $cs->GetContinentCSPBoard();
  $text = "Continent CSP Board";
  break;
default:
  $scoreboard = $cs->GetCountryCSRBoard();
  $text = "The Country Mainboard (CSR)";
  break;
}

$t->CacheCountryNames();

foreach ($scoreboard as &$entry) {
  if (isset($entry['country_id'])) {
    $entry['label'] = $t->GetCountryName($entry['country_id']);
  } else {
    $entry['label'] = $t->GetContinentName($entry['continent']);
  }
}
unset($entry);

render_with('scoreboards/country', [
  'page_title' => 'Country Scoreboards',
  'board' => $which_board,
  'text' => $text,
  'scoreboard' => $scoreboard,
]);
