<?php

Authorization::authorize('Guest');

$game = GamesRepository::get($_GET['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

$game['platforms'] = database_get_all(database_select("
  SELECT *
  FROM platforms
  JOIN game_platforms USING (platform_id)
  WHERE game_id = ? AND game_platforms.original = 1
  ORDER BY platform_name
", 's', [$game['game_id']]));

$sb_sort = $_GET['sort'] ?? 'sb_pos';
$board = $_GET['board'];

$game_name = $t->GetGameName($game['game_id']);

$t->CacheCountryNames();

if ($current_user) {
  // Board display settings
  $medal_display = $current_user['medal_display'];
  $speedrun_display = $current_user['speedrun_display'];

  switch ($speedrun_display) {
  case "speedrunpoints":
    $speedrunsort = "gsb_cache_speedrun.medal_points DESC, gsb_cache_speedrun.platinum DESC, gsb_cache_speedrun.gold DESC, gsb_cache_speedrun.silver DESC, gsb_cache_speedrun.bronze DESC";
    break;
  case "plats":
    $speedrunsort = "gsb_cache_speedrun.platinum DESC, gsb_cache_speedrun.gold DESC, gsb_cache_speedrun.silver DESC, gsb_cache_speedrun.bronze DESC";
    break;
  case "golds":
    $speedrunsort = "gsb_cache_speedrun.gold DESC, gsb_cache_speedrun.silver DESC, gsb_cache_speedrun.bronze DESC";
    break;
  }

  switch($medal_display) {
  case "plats":       $medalsort = "gsb_cache_medals.platinum DESC, gsb_cache_medals.gold DESC, gsb_cache_medals.silver DESC, gsb_cache_medals.bronze DESC"; break;
  case "golds":       $medalsort = "gsb_cache_medals.gold DESC, gsb_cache_medals.silver DESC, gsb_cache_medals.bronze DESC"; break;
  }
} else {
  $medalsort = "platinum DESC, gold DESC, silver DESC, bronze DESC";
  $speedrunsort = "platinum DESC, gold DESC, silver DESC, bronze DESC";
}

$scoreboards = [
  ["scoreboard" => "csp",                     "label" => "general_scoreboard",        "icon" => "CSStar24x25White",         "entries" => $cs->GetGameScoreboard($game['game_id'])],
  ["scoreboard" => "medal",                   "label" => "general_medal_table",       "icon" => "icon_ranked20x25White",    "entries" => $cs->GetGameMedalTable($game['game_id'])],
  ["scoreboard" => "arcade",                  "label" => "general_arcade_table",      "icon" => "icon_arcadeWhite17x25",    "entries" => $cs->GetGameArcadeboard($game['game_id'])],
  ["scoreboard" => "speedrun",                "label" => "general_speedrun_awards",   "icon" => "icon_speedrun20x25White",  "entries" => $cs->GetGameAwardsTable($game['game_id'])],
  ["scoreboard" => "solution",                "label" => "general_solution_hub",      "icon" => "SolutionWhite25x17",       "entries" => $cs->GetGameSolutionboard($game['game_id'])],
  ["scoreboard" => "challenge",               "label" => "general_challenge_table",   "icon" => "icon_challenge25x25White", "entries" => $cs->GetGameChallengeBoard($game['game_id'])],
  ["scoreboard" => "collectible",             "label" => "general_collectors_cache",  "icon" => "icon_collectible_white",   "entries" => $cs->GetGameCollectorsCache($game['game_id'])],
  ["scoreboard" => "incremental",             "label" => "general_incremental",       "icon" => "icon_incremental_white",   "entries" => $cs->GetGameScoreboardIncremental($game['game_id'])],
  ["scoreboard" => "trophy",                  "label" => "profilemodules_trophy_case","icon" => "icon_trophy_white",        "entries" => $cs->GetGameTrophyCase($game['game_id'])], 
  ["scoreboard" => "tsc",                     "label" => "groupstats_tsc",            "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedScoreboardTSC($game['game_id'])],
  ["scoreboard" => "mkpp",                    "label" => "groupstats_mkpp",           "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedScoreboardMKPP($game['game_id'])], 
  ["scoreboard" => "theelite",                "label" => "groupstats_theelite",       "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedScoreboardTE($game['game_id'])], 
  ["scoreboard" => "vgr",                     "label" => "groupstats_vgr",            "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedMedalTableVGR($game['game_id'])], 
];

$selected_scoreboard = NULL;
foreach ($scoreboards as $scoreboard) {
  if ($scoreboard['scoreboard'] == $board) {
    $selected_scoreboard = $scoreboard;
  }
}

if ($board == 'starboard') {
  $board = 'csp';
}

if ($selected_scoreboard == NULL) {
  $board = "csp";
  $selected_scoreboard = $scoreboards[0];
}

render_with('scoreboards/game', [
  'page_title' => $game_name,
  'board' => $board,
  'scoreboards' => $scoreboards,
  'selected_scoreboard' => $selected_scoreboard,
  'game' => $game,
  'time_to_next' => Rebuilders::game()->time_to_next(),
]);
