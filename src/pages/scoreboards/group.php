<?php

Authorization::authorize('Guest');

$game = GamesRepository::get($_GET['game_id']);
if (!$game) {
  $cs->PageNotFound();
}

$game['platforms'] = database_fetch_all("
  SELECT platforms.*
  FROM game_platforms
  LEFT JOIN platforms USING(platform_id)
  WHERE game_platforms.game_id = ? AND original = 1
", [$game['game_id']]);


$board = $_GET['type'] ?? "csp";

// The board was renamed, keeping this to avoid breaking old links
if ($board == 'supremacy_table') { $board = 'leaders'; }

// Get the groups, can be single or multiple group_ids seperated by a , or an array
// TODO: Not sure why we have so many options.
if (isset($_GET['group_ids'])) {
  if (is_array($_GET['group_ids'])) {
    $group_ids = $_GET['group_ids'];
  } else {
    $group_ids = explode(",", $_GET['group_ids']);
  }
  $group_ids = array_map(fn($id) => intval($id), $group_ids);
} else if (isset($_GET['group_id'])) {
  $group_ids = [intval($_GET['group_id'])];
} else {
  // whole game
  $group_ids = pluck(database_get_all(database_select("SELECT DISTINCT group_id FROM levels where game_id = ?", 's', [$game['game_id']])), 'group_id');
}

$group_filter = implode(", ", $group_ids);
$groups = database_get_all(database_select("
  SELECT levels.group_id, (levels.group_id IN($group_filter)) AS selected
  FROM levels LEFT JOIN level_groups USING(group_id) WHERE levels.game_id = ?
  GROUP BY levels.group_id ORDER BY level_groups.group_pos ASC
", 's', [$game['game_id']]));

foreach ($groups as &$group) {
  $group['name'] = $t->GetGroupName($group['group_id']);
}
unset($group);

$scoreboards = [
  ["scoreboard" => "csp",                     "label" => "general_scoreboard",                    "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupScoreboard($game['game_id'], $group_ids)],
  ["scoreboard" => "medal",                   "label" => "general_medal_table",                   "icon" => "icon_ranked20x25White",    "entries" => $cs->GetGroupMedalTable($game['game_id'], $group_ids)],
  ["scoreboard" => "arcade",                  "label" => "general_arcade_table",                  "icon" => "icon_arcadeWhite17x25",    "entries" => $cs->GetGroupArcadeboard($game['game_id'], $group_ids)],
  ["scoreboard" => "speedrun",                "label" => "general_speedrun_awards",               "icon" => "icon_speedrun20x25White",  "entries" => $cs->GetGroupSpeedrunTable($game['game_id'], $group_ids)],
  ["scoreboard" => "solution",                "label" => "general_solution_hub",                  "icon" => "SolutionWhite25x17",       "entries" => $cs->GetGroupSolutionboard($game['game_id'], $group_ids)],
  ["scoreboard" => "challenge",               "label" => "general_challenge_table",               "icon" => "icon_challenge25x25White", "entries" => $cs->GetGroupChallengeboard($game['game_id'], $group_ids)],
  ["scoreboard" => "collectible",             "label" => "general_collectors_cache",              "icon" => "icon_collectible_white",   "entries" => $cs->GetGroupCollectorsCache($game['game_id'], $group_ids)],
  ["scoreboard" => "incremental",             "label" => "general_incremental",                   "icon" => "icon_incremental_white",   "entries" => $cs->GetGroupExperienceBoard($game['game_id'], $group_ids)],
  ["scoreboard" => "leaders",                 "label" => "groupstats_leaderboard",                "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupLeadersTable($game['game_id'], $group_ids)],
  ["scoreboard" => "totalizer_highest_score", "label" => "groupstats_totalizer_highest_score",    "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupTotalizer($game['game_id'], $group_ids, 4)],
  ["scoreboard" => "totalizer_lowest_score",  "label" => "groupstats_totalizer_lowest_score",     "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupTotalizer($game['game_id'], $group_ids, 3)],
  ["scoreboard" => "totalizer_fastest_time",  "label" => "groupstats_totalizer_fastest_time",     "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupTotalizer($game['game_id'], $group_ids, 1)],
  ["scoreboard" => "totalizer_longest_time",  "label" => "groupstats_totalizer_longest_time",     "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupTotalizer($game['game_id'], $group_ids, 2)],
  ["scoreboard" => "unmodified_csp",          "label" => "groupstats_unmodified_csp",             "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupTotalizer($game['game_id'], $group_ids, 2)], //lol
  ["scoreboard" => "tsc",                     "label" => "groupstats_tsc",                        "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupUnrankedScoreboardTSC($game['game_id'], $group_ids)],
  ["scoreboard" => "mkpp",                    "label" => "groupstats_mkpp",                       "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupUnrankedScoreboardMKPP($game['game_id'], $group_ids)],
  ["scoreboard" => "theelite",                "label" => "groupstats_theelite",                   "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupUnrankedScoreboardTE($game['game_id'], $group_ids)],
  ["scoreboard" => "vgr",                     "label" => "groupstats_vgr",                        "icon" => "CSStar24x25White",         "entries" => $cs->GetGroupUnrankedMedalTableVGR($game['game_id'], $group_ids)],
];

$selected_scoreboard = NULL;
foreach ($scoreboards as $scoreboard) {
  if ($scoreboard['scoreboard'] == $board) {
    $selected_scoreboard = $scoreboard;
  }
}

if ($selected_scoreboard == NULL) {
  $board = "csp";
  $selected_scoreboard = $scoreboards[0];
}

$t->CacheGame($game['game_id']);
$t->CacheCountryNames();

$game_name = $t->GetGameName($game['game_id']);

render_with("scoreboards/group", [
  'page_title' => t('groupstats_title'),
  'board' => $board,
  'scoreboards' => $scoreboards,
  'selected_scoreboard' => $selected_scoreboard,
  'group_filter' => $group_filter,
  'groups' => $groups,
  'game' => $game,
  'game_name' => $game_name,
]);
