<?php

Authorization::authorize('GlobalMod');

$support = database_find_by('support', ['support_id' => $_GET['id']]);

if (!$support) {
  Flash::AddError("Support request not found");
  $cs->LeavePage('/support_requests.php');
}

database_insert('staff_tasks', [
  'user_id' => $current_user['user_id'],
  'tasks_type' => 'support_request_deleted',
  'task_id' => $support['support_id'],
  'result' => 'Support request deleted',
]);

database_update_by('staff_tasks', ['result' => 'Support request deleted'], [
  'tasks_type' => 'support_request_archived',
  'task_id' => $support['support_id'],
]);

database_delete_by('support', ['support_id' => $support['support_id']]);

Flash::AddSuccess("Support request deleted");
$cs->LeavePage("/support_requests.php");
