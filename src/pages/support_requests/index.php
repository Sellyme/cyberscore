<?php

Authorization::authorize('GlobalMod');

$requests = database_get_all(database_select("
  SELECT
    users.username,
    support.user_id,
    support.support_id,
    support.message,
    support.message_date,
    users2.username AS mod_name,
    support.mod_date,
    support.mod_comment
  FROM support
  LEFT JOIN users USING(user_id)
  LEFT JOIN users AS users2 ON users2.user_id = support.mod_id
  WHERE support.status = 0
  ORDER BY support.message_date DESC
", '', []));


if (isset($_GET['mail'])) {
  $searched_email = $_GET['mail'];
  $searched_user = UsersRepository::find_by(['email' => $searched_email]);
} else {
  $searched_email = null;
  $searched_user = null;
}

render_with('support_requests/index', [
  'page_title' => 'Support requests',
  'requests' => $requests,
  'searched_email' => $searched_email,
  'searched_user' => $searched_user,
]);
