<?php

Authorization::authorize('GlobalMod');

foreach ($_POST['action'] as $suggestion_id => $action) {
  switch ($action) {
  case 'activate':
    $t->ActivateTranslationDB($suggestion_id);
    break;
  case 'delete':
    $t->DeleteTranslationDB($suggestion_id);
    break;
  }
}

$cs->RedirectToPreviousPage();
