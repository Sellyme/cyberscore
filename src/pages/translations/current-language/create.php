<?php

Authorization::authorize('User');

database_update_by(
  'user_prefs',
  ['translate_lang' => $_POST['translate_lang']],
  ['user_id' => $current_user['user_id']]
);

$cs->RedirectToPreviousPage();
