<?php
Authorization::authorize('Translator');

$languages = database_get_all(database_select("
  SELECT * FROM languages
  ORDER BY available ASC, sub_language DESC, language_name ASC
", '', []));

foreach ($languages as &$language) {
  $language['country_count'] = $t->CountCountryNames($language['language_id']);
  $language['news_headline_count'] = $t->CountNewsHeadlines($language['language_id']);
  $language['news_text_count'] = $t->CountNewsTexts($language['language_id']);
  $language['proof_rule_count'] = $t->CountProofRuleTexts($language['language_id']);
  $language['rule_count'] = $t->CountRuleTexts($language['language_id']);
  $language['game_progress_count'] = $t->CountGameProgress($language['language_id']);
  $language['translation_page_count'] = $t->CountTranslationPage($language['language_id']);
}
unset($language);

render_with('translations/index', [
  'page_title' => 'Translation hub',
  'languages' => $languages,
]);
