<?php

Authorization::authorize('Translator');

$suggestion = database_find_by('translation_db', ['id' => $_POST['suggestion_id']]);

if (!$suggestion) {
  $cs->LeavePage('/translations/news', "News suggestion not found");
}

