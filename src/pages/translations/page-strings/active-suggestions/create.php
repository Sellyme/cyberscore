<?php

Authorization::authorize('Translator');

$suggestion = database_find_by('translation_pages', ['id' => $_POST['suggestion_id']]);

if (!$suggestion) {
  $cs->LeavePage('/translations/page-strings', "Page string suggestion not found");
}

database_update_by('translation_pages', ['is_active' => 'n'], [
  'language_id' => $suggestion['language_id'],
  'key' => $suggestion['key'],
  'group' => $suggestion['group'],
]);

database_update_by('translation_pages', ['is_active' => 'y'], ['id' => $suggestion['id']]);

$cs->RedirectToPreviousPage();
