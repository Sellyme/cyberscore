<?php

Authorization::authorize('GlobalMod');

$params = filter_post_params([
  Params::str("group"),
  Params::str("key"),
  Params::str("text"),
]);

$t->AddTranslationPage(
  1,
  $params['group'],
  $params['key'],
  $params['text'],
  $current_user['user_id']
);

Notification::PageStringAdded($params['key'], $current_user['user_id'])->DeliverToGroups(['Translator']);

$cs->WriteNote(true, 'Page string added');
$cs->RedirectToPreviousPage();
