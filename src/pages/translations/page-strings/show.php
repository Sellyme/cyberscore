<?php

Authorization::authorize('Translator');

$translate_lang_id = $t->GetTranslateLang();

$key = $_GET['id'];

$group = database_single_value("SELECT `group` FROM translation_pages WHERE `key` = ?", 's', [$key]);

$english = database_find_by('translation_pages', ['key' => $key, 'language_id' => 1]);
$translation = $t->RetrievePageTranslation($translate_lang_id, $key);

$existing_suggestions = $t->RetrievePageSuggestions($translate_lang_id, $key);

foreach ($existing_suggestions as &$suggestion) {
  [$upvoters, $downvoters] = $t->RetrievePageVotes($suggestion['id']);
  $has_upvoted = array_search($current_user['username'], $upvoters) !== false;
  $has_downvoted = array_search($current_user['username'], $downvoters) !== false;

  $suggestion['upvoters'] = $upvoters;
  $suggestion['downvoters'] = $downvoters;
  $suggestion['current_user_vote'] = $has_upvoted ? 'up' : ($has_downvoted ? 'down' : NULL);

  $suggestion['current_user_can_edit'] = ($current_user['username'] == $suggestion['username'] || Authorization::has_access('GlobalMod'));
}
unset($suggestion);

$languages = database_get_all(database_select("
  SELECT language_id
  FROM languages
  WHERE sub_language = 'n'
  ORDER BY language_id ASC
", '', []));

foreach ($languages as &$language) {
  $language['existing_suggestions'] = $t->RetrievePageSuggestions($language['language_id'], $key);
  $language['name'] = $t->GetLangName($language['language_id']);
  $language['page_translation'] = $t->RetrievePageTranslation($language['language_id'], $key);
}
unset($language);

render_with('translations/page-strings/show', [
  'page_title' => 'Translation info',
  'key' => $key,
  'group' => $group,
  'english' => $english,
  'translation' => $translation,
  'language_name' => $t->GetLangName($translate_lang_id),
  'existing_suggestions' => $existing_suggestions,
  'languages' => $languages,
  'translate_lang_id' => $translate_lang_id,
]);
