<?php

Authorization::authorize('Translator');

$params = filter_post_params([
  Params::number('language_id'),
  Params::str('group'),
  Params::str('key'),
  Params::str('text'),
  Params::str('comment'),
  Params::enum('is_active', ['y', 'n']),
]);

$t->AddTranslationPage(
  $params['language_id'],
  $params['group'],
  $params['key'],
  $params['text'],
  $current_user['user_id'],
  $params['comment'],
  $params['is_active']
);

$cs->RedirectToPreviousPage();
