<?php

Authorization::authorize('Translator');

$params = filter_post_params([
  Params::str('group'),
  Params::str('key'),
  Params::str('text'),
  Params::checkbox('deactivate', true, false),
]);

$suggestion = database_find_by('translation_pages', ['key' => $_GET['id'], 'language_id' => 1]);

if (!$suggestion) {
  $cs->LeavePage('/translations/page-strings', "Page string not found");
}

if (isset($params['deactivate'])) {
  database_update_by('translation_pages', ['is_active' => 'n'], ['key' => $suggestion['key']]);
}

database_update_by(
  'translation_pages',
  ['group' => $params['group'], 'key' => $params['key']],
  ['key' => $suggestion['key']]
);

database_update_by(
  'translation_pages',
  ['translation' => $params['text'], 'is_active' => 'y'],
  ['id' => $suggestion['id']]
);

$cs->LeavePage("/translations/page-strings/{$params['key']}");
