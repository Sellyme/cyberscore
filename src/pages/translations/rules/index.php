<?php

Authorization::authorize('Translator');

$translate_lang_id = $t->GetTranslateLang();

$page = intval($_GET['p'] ?? 0);
$page_size = 50;

$start = $page * $page_size;
$total = database_single_value(
  "SELECT COUNT(1) FROM rules", '', []
);

$rules = database_get_all(database_select("
  SELECT rule_id, rule_text
  FROM rules
  ORDER BY rule_id ASC
  LIMIT $page_size
  OFFSET $start
", '', []));

foreach ($rules as &$rule) {
  $rule['translation'] = $t->GetRuleTextLang($rule['rule_id'], $translate_lang_id);

  $rule['suggested_translation'] =
    $t->GetSuggestedRuleTextLang($rule['rule_id'], $translate_lang_id) ??
    $rule['translation'];
}
unset($rule);

render_with('translations/rules/index', [
  'page_title' => 'Translation',
  'language_name' => $t->GetLangName($translate_lang_id),
  'rules' => $rules,
  'current_page' => $page,
  'total_pages' => $total / $page_size,
  'translate_lang_id' => $translate_lang_id,
]);
