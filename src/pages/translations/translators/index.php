<?php

Authorization::authorize('GlobalMod');

$users = database_get_all(database_select("
  SELECT users.*, GROUP_CONCAT(translator_info.language_code ORDER BY translator_info.language_code ASC SEPARATOR ',') AS language_codes
  FROM users
  LEFT JOIN translator_info USING(user_id)
  WHERE users.user_groups & 1
  GROUP BY users.user_id
  ORDER BY users.username ASC
", '', []));

$languages = database_get_all(database_select("
  SELECT language_name, language_code
  FROM languages
  WHERE sub_language = 'n' AND language_id != 1
  ORDER BY language_code ASC
", '', []));

render_with('translations/translators/index', [
  'page_title' => 'Add translator',
  'users' => $users,
  'languages' => $languages,
]);
