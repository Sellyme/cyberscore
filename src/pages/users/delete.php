<?php

Authorization::authorize('GlobalMod');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

$record_count = database_single_value("SELECT COUNT(1) FROM records WHERE user_id = ?", 's', [$user['user_id']]);

if ($record_count > 0) {
  $cs->WriteNote(false, "Can't delete this user: they still have submitted records");
  $cs->RedirectToPreviousPage();
}

// TODO: missing a bunch more
database_delete_by('csmail', ['from_id' => $user['user_id']]);
database_delete_by('csmail', ['to_id' => $user['user_id']]);
database_delete_by('csmail_folder', ['user_id' => $user['user_id']]);
database_delete_by('proofmailer', ['user_id' => $user['user_id']]);
database_delete_by('sb_history', ['user_id' => $user['user_id']]);
database_delete_by('user_friends', ['user_id' => $user['user_id']]);
database_delete_by('user_friends', ['friend_id' => $user['user_id']]);
database_delete_by('user_profile_boxes', ['user_id' => $user['user_id']]);
database_delete_by('user_prefs', ['user_id' => $user['user_id']]);
database_delete_by('user_info', ['user_id' => $user['user_id']]);
database_delete_by('users', ['user_id' => $user['user_id']]);

$cs->WriteNote(true, "The user {$user['username']} has been deleted.");
redirect_to("/users");
