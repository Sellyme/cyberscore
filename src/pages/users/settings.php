<?php

global $current_user;

Authorization::authorize('UnverifiedUser');

$sidebar = [
  'personal'     => t('profile_header_personal'),
  'contact'      => t('profile_header_contact'),
  'password'     => t('profile_header_password'),
  'display'      => ('Display settings'),
  'site'         => t('profile_header_prefs'),
  'notifications'=> ('Notifications'),
  'charticons'   => ('Chart icons'),
  'referrals'    => ('Referrals'),
  'forum'        => t('profile_header_forum'),
];

$src = validate_module(['Settings' => $sidebar], 'settings', $_GET['src']);

render_with('users/settings', [
  'page_title' => t('profile_stats_title', ['username' => $current_user['username']]),
  'sidebar' => $sidebar,
  'src' => $src,
]);
