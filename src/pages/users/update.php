<?php

Authorization::authorize('GlobalMod');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('username'),
  Params::str('email'),
  Params::str('mod_notes'),
  Params::checkbox('auth_verified', 'y', 'n'),
  Params::checkbox('user_status', 0, 2), // what's 1?
  Params::str('profile'),
]);

if (isset($params['username'])) {
  $other_user = database_find_by('users', ['username' => $params['username']]);
  if ($other_user && $other_user['user_id'] != $user['user_id']) {
    $cs->WriteNote(false, "Can't change username: it's already taken");
    redirect_to("/users/{$user['user_id']}/edit");
  }
}

if (isset($params['email'])) {
  $other_user = database_find_by('users', ['email' => $params['email']]);
  if ($other_user && $other_user['user_id'] != $user['user_id']) {
    $cs->WriteNote(false, "Can't change email: it's already taken");
    redirect_to("/users/{$user['user_id']}/edit");
  }
}

if (isset($params['mod_notes']) && $params['mod_notes'] != $user['mod_notes']) {
  $params['mod_notes_date'] = database_now();
  $params['mod_notes_id'] = $current_user['user_id'];
}

database_update_by('users', $params, ['user_id' => $user['user_id']]);

$cs->RedirectToPreviousPage();
