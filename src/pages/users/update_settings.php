<?php

Authorization::authorize('UnverifiedUser');

if ($_POST['verify'] ?? NULL) {
  User::SendConfirmationEmail($current_user['user_id']);

  $cs->WriteNote(true, 'Verification email sent. Check your inbox and follow the instructions on the message.');
  redirect_to_back();
}

$user_settings_params = filter_post_params([
  // Public information
  Params::str('forename'),
  Params::str('surname'),
  Params::str('gender'),
  Params::str('dob'),
  Params::str('profile'),
  Params::number('country_id'),
  // Display settings
  Params::str('timezone'),
  // Contact details
  Params::str('im_aol'),
  Params::str('im_icq'),
  Params::str('im_yahoo'),
  Params::str('im_skype'),
  Params::str('website'),
  // Site preferences
  Params::checkbox('email_ygb', 'y', 'n'),
  Params::checkbox('email_pm', 'y', 'n'),
  Params::checkbox('lastseen_hidden', 'y', 'n'),
]);

if ($user_settings_params['dob'] == '') {
  $user_settings_params['dob'] = NULL;
}

$user_info_params = filter_post_params([
  // Contact details
  Params::str('twitter'),
  Params::str('discord_tag'),
  Params::str('youtube'),
  Params::str('twitch'),
  Params::str('steam_id'),
  Params::str('3ds_fc'),
  Params::str('wiiu_fc'),
  Params::str('switch_fc'),
  Params::str('pokemon_go_fc'),
  Params::str('psn_gamertag'),
  Params::str('xbl_gamertag'),
  Params::str('backloggery'),
  // Site preferences
  Params::number('featured_article_id'),
  Params::str('blog_name'),
]);

$user_prefs_params = filter_post_params([
  Params::str('medal_display'),
  Params::str('speedrun_display'),
  Params::str('trophy_display'),
  Params::str('highlight_me'),
  Params::str('highlight_friends'),
  Params::str('show_full_names'),
  Params::number('chart_limit'),
  Params::number('chart_sort'),
  Params::str('date_format'),
  // Site preferences
  Params::str('applet'),
  Params::checkbox('index_forum_tags', 1, 0),
  Params::checkbox('show_entity_icons', 'yes', 'no'),
  Params::checkbox('userpage_comments', 'yes', 'no'),
  Params::number('version'),
  // Chart icons
  Params::str('preferred_chart_icon'),
]);

$notification_params = filter_post_params([
  Params::checkbox('ygb', 'yes', 'no'),
  Params::checkbox('rec_approved', 'yes', 'no'),
  Params::checkbox('proof_refused', 'yes', 'no'),
  Params::checkbox('greq_comment', 'yes', 'no'),
  Params::checkbox('new_grequest', 'yes', 'no'),
  Params::checkbox('supp', 'yes', 'no'),
  Params::checkbox('pstring_edited', 'yes', 'no'),
  Params::checkbox('pstring_added', 'yes', 'no'),
  Params::checkbox('reported_rec', 'yes', 'no'),
  Params::checkbox('new_game', 'yes', 'no'),
  Params::checkbox('article_comment', 'yes', 'no'),
  Params::checkbox('referral_confirmed', 'yes', 'no'),
  Params::checkbox('referral_rejected', 'yes', 'no'),
  Params::checkbox('reply', 'yes', 'no'),
  Params::checkbox('userpage_comments', 'yes', 'no'),
  Params::checkbox('blog_follow', 'yes', 'no'),
  Params::checkbox('blog_unfollow', 'yes', 'no'),
  Params::checkbox('followed_posted', 'yes', 'no'),
]);

$password_params = filter_post_params([
  Params::str('current_password'),
  Params::str('password'),
  Params::str('password_confirmation'),
]);

$referral_params = filter_post_params([
  Params::str('referrer'),
]);

$forum_params = filter_post_params([
  Params::str('forum_username'),
  Params::str('forum_password'),
]);

database_update_by(
  'users',
  $user_settings_params,
  ["user_id" => $current_user['user_id']]
);

$country_code = database_single_value("
  SELECT countries.country_code FROM users JOIN countries USING (country_id) WHERE user_id = ?
", 'i', [$current_user['user_id']]);

database_update_by(
  'users',
  ['country_code' => $country_code],
  ["user_id" => $current_user['user_id']]
);

database_update_by(
  'user_info',
  $user_info_params,
  ["user_id" => $current_user['user_id']]
);

database_update_by(
  'notification_prefs',
  $notification_params,
  ["user_id" => $current_user['user_id']]
);

// validate user preferences
{
  if (isset($user_prefs_params['preferred_chart_icon'])) {
    $good = false;
    foreach (ChartIcon::all_selections($current_user) as [$label, $selections]) {
      $selections = array_filter($selections, fn($s) => $s['available']);
      if (in_array($user_prefs_params['preferred_chart_icon'], pluck($selections, 'id'))) {
        $good = true;
      }
    }

    if (!$good) {
      $cs->WriteNote(false, "You haven't unlocked that chart icon yet!");
      redirect_to_back();
    }
  }

  database_update_by(
    'user_prefs',
    $user_prefs_params,
    ["user_id" => $current_user['user_id']]
  );
}

if ($_POST['delete-avatar'] ?? NULL) {
  Uploads::remove('user-avatar', $current_user['user_id']);
}

if ($_FILES['avatar']) {
  Uploads::save('user-avatar', $current_user['user_id'], $_FILES['avatar']);
}

if ($_POST['delete-banner'] ?? NULL) {
  Uploads::remove('user-banner', $current_user['user_id']);
}

if ($_FILES['banner']) {
  Uploads::save('user-banner', $current_user['user_id'], $_FILES['banner']);
}

if (!empty($password_params['current_password'])) {
  $current_password = $password_params['current_password'];
  $password = $password_params['password'];
  $password_confirmation = $password_params['password_confirmation'];

  list($good_password, $error) = Authentication::validate_password(
    $password,
    $password_confirmation
  );

  if (!Authentication::password_verify($current_password, $current_user)) {
    $cs->WriteNote(false, 'Current password incorrect!');
  } else if (!$good_password) {
    $cs->WriteNote(false, $error);
  } else {
    Authentication::store_password($password, $current_user);

    $cs->WriteNote(true, 'Password changed');
  }
}

if (!empty($referral_params['referrer'])) {
  $referrer = $referral_params['referrer'];
  $referrer = database_get(database_select(
    'SELECT user_id, username FROM users WHERE user_id = ? OR username = ?',
    'is',
    [intval($referrer), $referrer]
  ));

  if ($referrer !== NULL) {
    referral_create($referral_params['referrer'], $current_user['user_id']);
    $cs->WriteNote(true, 'You have successfully set ' . h($referrer['username']) . ' (#' . $referrer['user_id'] . ') as your referral.');
  } else {
    $cs->WriteNote(false, 'You did not input a valid user ID or username. Please verify the user ID and try again.');
  }
}

if (!empty($forum_params['forum_username'])) {
  include($config['forum']['path'] . "/SSI.php");

  $has_linked_account = database_single_value('SELECT 1 FROM smf_accounts WHERE user_id = ?', 'i', [$current_user['user_id']]);
  $smf_member_id = database_single_value('SELECT id_member FROM smf_members WHERE member_name = ?', 's', [$forum_params['forum_username']]);
  $right_password = ssi_checkPassword($forum_params['forum_username'], $forum_params['forum_password'], true);

  if ($has_linked_account) {
    $cs->WriteNote(false, 'You already have a linked forum account');
  } else if ($smf_member_id === NULL) {
    $cs->WriteNote(false, 'Forum account not found');
  } else if (!$right_password) {
    sleep(3);
    $cs->WriteNote(false, 'Forum password was incorrect');
  } else {
    database_insert('smf_accounts', [
      'user_id' => $current_uset['user_id'],
      'smf_member_id' => $smf_member_id,
    ]);
    $cs->WriteNote(true, 'Forum account synched up');
  }
}

// Reload current user
$current_user = database_get(database_select("
  SELECT *
  FROM users
  LEFT JOIN user_prefs USING(user_id)
  LEFT JOIN user_info USING(user_id)
  WHERE users.user_id = ? AND users.user_status > 0
", 'i', [$current_user['user_id']]));

redirect_to_back();
