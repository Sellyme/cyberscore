<?php

require_once('includes/base_repository.php');

class CSMailMessagesRepository extends BaseRepository {
  protected static $table_name = 'csmail';
  protected static $primary_key = 'csmail_id';
}
