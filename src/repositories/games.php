<?php

require_once('includes/base_repository.php');

class GamesRepository extends BaseRepository {
  protected static $table_name = 'games';
  protected static $primary_key = 'game_id';
}
