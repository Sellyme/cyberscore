<?php

require_once('includes/base_repository.php');

class LevelsRepository extends BaseRepository {
  protected static $table_name = 'levels';
  protected static $primary_key = 'level_id';

  protected static $relations = [
    'group' => ['GroupsRepository', 'group_id'],
  ];
}
