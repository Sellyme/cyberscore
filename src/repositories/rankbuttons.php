<?php

require_once('includes/base_repository.php');

class RankbuttonsRepository extends BaseRepository {
  protected static $table_name = 'rankbuttons';
  protected static $primary_key = 'rankbutton_id';
}
