<?php

require_once('includes/base_repository.php');

class RecordsRepository extends BaseRepository {
  protected static $table_name = 'records';
  protected static $primary_key = 'record_id';
}

class DeletedRecordsRepository extends BaseRepository {
  protected static $table_name = 'records_del';
  protected static $primary_key = 'record_id';
}
