<?php

require_once('includes/base_repository.php');

class ReferralsRepository extends BaseRepository {
  protected static $table_name = 'referrals';
  protected static $primary_key = 'referral_id';

  public static function count_confirmed($user_id) {
    $table = static::$table_name;

    return database_single_value("
      SELECT COUNT(*)
      FROM $table
      WHERE referrer_id = ?
      AND confirmed = 1
      AND deleted_at IS NULL
    ", 'i', [$user_id]);
  }

  public static function delete($id) {
    return static::update($id, ['deleted_at' => database_now()]);
  }

  public static function find_by($params, $includes = []) {
    return parent::find_by(array_merge(['deleted_at' => NULL], $params), $includes);
  }
}
