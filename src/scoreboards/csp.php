<?php

require_once("src/scoreboards/filters.php");

class CSPScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_csp
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_fetch_all("
      SELECT
        users.*,
        SUM(total_csp) AS total_csp,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_csp
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY total_csp DESC
      LIMIT $limit
      OFFSET $offset
    ", []);
  }
}
