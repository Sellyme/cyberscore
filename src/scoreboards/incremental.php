<?php

require_once("src/scoreboards/filters.php");

class IncrementalScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_incremental
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      case 'vxp': return 'vs_cxp DESC, cxp DESC';
      case 'cxp': return 'cxp DESC, vs_cxp DESC';
      default: return 'vs_cxp DESC, cxp DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(gsb_cache_incremental.cxp) AS cxp,
        SUM(gsb_cache_incremental.vs_cxp) AS vs_cxp,
        sb_cache_incremental.base_level
      FROM gsb_cache_incremental
      JOIN users USING (user_id)
      LEFT JOIN sb_cache_incremental USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
