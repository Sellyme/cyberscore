<?php

require_once("src/scoreboards/filters.php");

class ProofScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_proof
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      case 'percentage': return 'SUM(total) / SUM(num_subs) DESC, total DESC';
      default: return 'total DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(num_subs) AS num_subs,
        SUM(total) AS total,
        SUM(medal) AS medal,
        SUM(speedrun) AS speedrun,
        SUM(solution) AS solution,
        SUM(unranked) AS unranked,
        SUM(collectible) AS collectible,
        SUM(incremental) AS incremental,
        SUM(challenge) AS challenge,
        SUM(arcade) AS arcade
      FROM gsb_cache_proof
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
