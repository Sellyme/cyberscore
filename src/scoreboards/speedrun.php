<?php

require_once("src/scoreboards/filters.php");

class SpeedrunScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_speedrun
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      default:         return 'medal_points DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(gsb_cache_speedrun.medal_points) AS medal_points,
        sb_cache_speedrun.*
      FROM gsb_cache_speedrun
      JOIN users USING (user_id)
      JOIN sb_cache_speedrun USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY gsb_cache_speedrun.user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
