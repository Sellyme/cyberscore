<?php

require_once("src/scoreboards/filters.php");

class SubmissionsScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_submissions
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $sorting = 'total DESC';
    $filters = ScoreboardFilters::sql_filter($this);

    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(num_subs) AS num_subs,
        SUM(total) AS total,
        SUM(medal) AS medal,
        SUM(speedrun) AS speedrun,
        SUM(solution) AS solution,
        SUM(unranked) AS unranked,
        SUM(collectible) AS collectible,
        SUM(incremental) AS incremental,
        SUM(challenge) AS challenge,
        SUM(arcade) AS arcade
      FROM gsb_cache_submissions
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
