<?php

require_once("src/scoreboards/filters.php");

class TrophyScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_trophy
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      case 'platinum': return 'trophy_platinum DESC, trophy_points DESC';
      case 'gold':     return 'trophy_gold DESC, trophy_points DESC';
      case 'silver':   return 'trophy_silver DESC, trophy_points DESC';
      case 'bronze':   return 'trophy_bronze DESC, trophy_points DESC';
      case '4th':      return 'trophy_fourth DESC, trophy_points DESC';
      case '5th':      return 'trophy_fifth DESC, trophy_points DESC';
      default:         return 'trophy_points DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(gsb_cache_trophy.trophy_points) AS trophy_points,
        SUM(gsb_cache_trophy.platinum) AS trophy_platinum,
        SUM(gsb_cache_trophy.gold) AS trophy_gold,
        SUM(gsb_cache_trophy.silver) AS trophy_silver,
        SUM(gsb_cache_trophy.bronze) AS trophy_bronze,
        SUM(gsb_cache_trophy.fourth) AS trophy_fourth,
        SUM(gsb_cache_trophy.fifth) AS trophy_fifth
      FROM gsb_cache_trophy
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
