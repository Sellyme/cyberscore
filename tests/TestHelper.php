<?php

require_once("includes/config.php");
$config = load_config(__DIR__ . '/../config.ini');

require_once("includes/common.php");
require_once("includes/charts.php");
require_once("includes/modifiers.php");
require_once("includes/article.php");
require_once("includes/cs_class_x.php");
require_once("includes/translation.php");
require_once("includes/authorization.php");
require_once("includes/awards.php");
require_once("includes/helpers.php");
require_once("includes/session.php");
require_once("includes/uploads.php");
require_once("includes/user.php");
require_once("src/lib/tabbed_modules.php");
require_once("src/lib/rankbutton_generator.php");

require_once("src/repositories/rankbuttons.php");
require_once("src/repositories/api_tokens.php");

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../src/templates');
$twig = new \Twig\Environment($loader, [
  'cache' => __DIR__ . '/../cache',
  'debug' => true,
]);

require_once("src/lib/twig_functions.php");

$t = new TranslationManager();
$t->ResetUser();
$cs = new CSClass();
$current_user = null;

function render_test($path) {
  ob_start();
  require($path);

  $contents = ob_get_contents();
  ob_end_clean();
  return $contents;
}
