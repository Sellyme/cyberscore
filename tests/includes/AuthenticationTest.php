<?php

require_once("includes/helpers/database.php");
require_once("includes/authentication.php");

global $config;
global $db;
$config = parse_ini_file(__DIR__ . '/../../config.ini', true);
$config['app']['debug'] ??= false;
database_open();

test("authenticate()", function() {
  expect(Authentication::authenticate("wiki", "poop"))->toBeNull();
});
