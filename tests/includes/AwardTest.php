<?php

require_once("includes/awards.php");

test('Award::LevelFromExperience', function () {
  expect(Award::LevelFromExperience(0))->toBe(1);
  expect(Award::LevelFromExperience(1))->toBe(1);
  expect(Award::LevelFromExperience(50))->toBe(2);
  expect(Award::LevelFromExperience(100))->toBe(3);
});

test('Award::ExperienceFromLevel', function () {
  expect(Award::ExperienceFromLevel(1))->toBe(0);
  expect(Award::ExperienceFromLevel(2))->toBe(25.0);
  expect(Award::ExperienceFromLevel(3))->toBe(63.0);
  expect(Award::ExperienceFromLevel(9))->toBe(15259.0);
});

test('Award::ValidatePlatinum', function () {
  expect(Award::ValidatePlatinum(2, 1))->toBe(false);
  expect(Award::ValidatePlatinum(3, 1))->toBe(false);
  expect(Award::ValidatePlatinum(4, 1))->toBe(false);
  expect(Award::ValidatePlatinum(4, 1))->toBe(false);
  expect(Award::ValidatePlatinum(5, 1))->toBe(true);
  expect(Award::ValidatePlatinum(5, 2))->toBe(false);
  expect(Award::ValidatePlatinum(19, 2))->toBe(false);
  expect(Award::ValidatePlatinum(20, 2))->toBe(true);
});
