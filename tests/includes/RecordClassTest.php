<?php

require_once("includes/record_class.php");

test('RecordManager::BuildGameIncrementalCache', function () {
  $expected = database_filter_by('gsb_cache_incremental', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = RecordManager::FetchRecordsWithModifiers(2006);

  $actual = RecordManager::BuildGameIncrementalCache(2006, $records);

  expect($expected)->toBe($actual);
});

test('RecordManager::BuildGameArcadeCache', function () {
  $expected = database_filter_by('gsb_cache_arcade', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = RecordManager::FetchRecordsWithModifiers(2006);

  $actual = RecordManager::BuildGameArcadeCache(2006, $records);

  expect($expected)->toBe($actual);
});

test('RecordManager::BuildGameCollectibleCache', function () {
  $expected = database_filter_by('gsb_cache_collectible', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = RecordManager::FetchRecordsWithModifiers(2006);

  $actual = RecordManager::BuildGameCollectibleCache(2006, $records);

  expect($expected)->toBe($actual);
});

test('RecordManager::BuildGameSpeedrunCache', function () {
  $expected = database_filter_by('gsb_cache_speedrun', ['game_id' => 2961]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = RecordManager::FetchRecordsWithModifiers(2961);

  $actual = RecordManager::BuildGameSpeedrunCache(2961, $records);

  expect($expected)->toBe($actual);
});

test('RecordManager::BuildGameSolutionCache', function () {
  $expected = database_filter_by('gsb_cache_solution', ['game_id' => 629]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = RecordManager::FetchRecordsWithModifiers(629);

  $actual = RecordManager::BuildGameSolutionCache(629, $records);

  expect($expected)->toBe($actual);
});

test('RecordManager::BuildGameProofCache', function () {
  $expected = database_filter_by('gsb_cache_proof', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = RecordManager::FetchRecordsWithModifiers(2006);

  $actual = RecordManager::BuildGameProofCache(2006, $records);

  expect($expected)->toBe($actual);
});
