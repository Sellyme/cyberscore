<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/games/index', function () {
  global $_SESSION;
  $_SESSION = [];

  ob_start();
  require("src/pages/games/index.php");

  $contents = ob_get_contents();
  ob_end_clean();

  $crawler = new Crawler($contents);

  expect($crawler->filter('main #allgames tr')->count())
    ->toBe(1 + database_value("select count(1) from games where site_id != 4", []));
});
