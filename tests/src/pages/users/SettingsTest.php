<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/users/settings/password', function () {
  global $_SESSION;
  $_SESSION = [];

  global $_GET;
  $_GET = ['src' => 'password'];

  load_current_user_from_user_id(33991);

  $contents = render_test("src/pages/users/settings.php");
  $crawler = new Crawler($contents);

  expect($crawler->filter('main .user-settings-form-security')->count())->toBe(2);
});
