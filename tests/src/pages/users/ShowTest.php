<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/users/show/trophy', function () {
  global $_SESSION;
  $_SESSION = [];

  global $_GET;
  $_GET = ['id' => '33991', 'src' => 'trophy'];

  load_current_user_from_user_id(33991);

  $contents = render_test("src/pages/users/show.php");
  $crawler = new Crawler($contents);

  expect($crawler->filter('main .user-show-contents tr')->count())->toBeGreaterThan(4);
});

